# ------------------------------------------------------------------------------
#  labelentry.tcl
#  This file is part of Unifix BWidget Toolkit
#  $Id: labelentry.tcl,v 1.6.2.1 2011/02/14 16:56:09 oehhar Exp $
# ------------------------------------------------------------------------------
#  Index of commands:
#     - LabelEntry::create
#     - LabelEntry::configure
#     - LabelEntry::cget
#     - LabelEntry::bind
# ------------------------------------------------------------------------------
package provide labelentry 1.0

package require nx
package require widget
package require entry
package require labelframe



namespace eval NXBwidget {

nx::Class create LabelEntry -superclass NXBwidget::Widget {
    
    #+++
    :public method initVars args {
	#initialize the classvariables of the superclasses + mixinclasses and the ones of self
	next
	
	set :_inst_Entry [NXBwidget::Entry new]
	set :_inst_LabelFrame [NXBwidget::LabelFrame new]
    }
    
    #+++
    :public method init {} {
    	:initVars;  #+++
	
	#+++
	:define LabelEntry labelentry Entry LabelFrame

	:bwinclude LabelEntry LabelFrame .labf \
	    remove {-relief -borderwidth -focus} \
	    rename {-text -label} \
	    prefix {label -justify -width -anchor -height -font -textvariable}

	:bwinclude LabelEntry Entry .e \
	    remove {-fg -bg} \
	    rename {-foreground -entryfg -background -entrybg}

	:addmap LabelEntry "" :cmd {-background {}}
	
	:syncoptions LabelEntry Entry .e {-text {}}
	:syncoptions LabelEntry LabelFrame .labf {-label -text -underline {}}

	::bind BwLabelEntry <FocusIn> [list focus %W.labf]
	::bind BwLabelEntry <Destroy> [list [self] _destroy %W]
	#+++
    }

}

}




# ------------------------------------------------------------------------------
#  Command LabelEntry::create
# ------------------------------------------------------------------------------
NXBwidget::LabelEntry public method create { path args } {
    :storePath $path; #+++
    
    array set maps [list LabelEntry {} :cmd {} .labf {} .e {}]
    array set maps [:parseArgs LabelEntry $args]; #+++

    #+++
    if {[:theme]} {
        eval [list ttk::frame $path] $maps(:cmd) -class LabelEntry \
            -takefocus 0
    }  else  {
        eval [list frame $path] $maps(:cmd) -class LabelEntry \
            -relief flat -bd 0 -highlightthickness 0 -takefocus 0
    }
    :initFromODB LabelEntry $path $maps(LabelEntry); #+++
	
    set :_inst_LabelFrame [NXBwidget::LabelFrame new]; #+++
    #+++
    set labf  [eval [list ${:_inst_LabelFrame} create $path.labf] $maps(.labf) \
                   [list -relief flat -borderwidth 0 -focus $path.e]]
    set subf  [${:_inst_LabelFrame} getframe $labf]; #+++
    
    set :_inst_Entry [NXBwidget::Entry new]; #+++
    set entry [eval [list ${:_inst_Entry} create $path.e] $maps(.e)]

    pack $entry -in $subf -fill both -expand yes
    pack $labf  -fill both -expand yes

    bindtags $path [list $path BwLabelEntry [winfo toplevel $path] all]

    next "LabelEntry $path"; #+++
    
    #+++
    #proc ::$path { cmd args } \
    #	"return \[LabelEntry::_path_command [list $path] \$cmd \$args\]"
    
    proc ::$path { cmd args } \
    	"return \[[self] _path_command [list $path] \$cmd \$args\]"
    #+++
    
    return $path
}


# ------------------------------------------------------------------------------
#  Command LabelEntry::configure
# ------------------------------------------------------------------------------
NXBwidget::LabelEntry public method configure { path args } {
    return [next "$path $args"]; #+++
}


# ------------------------------------------------------------------------------
#  Command LabelEntry::cget
# ------------------------------------------------------------------------------
#+++ NXBwidget::LabelEntry public method cget { path option } {
#    return [Widget::cget $path $option]
#}


# ------------------------------------------------------------------------------
#  Command LabelEntry::bind
# ------------------------------------------------------------------------------
NXBwidget::LabelEntry public method bind { path args } {
    return [eval [list ::bind $path.e] $args]
}


#------------------------------------------------------------------------------
#  Command LabelEntry::_path_command
#------------------------------------------------------------------------------
NXBwidget::LabelEntry public method _path_command { path cmd larg } {
    if { [string equal $cmd "configure"] ||
         [string equal $cmd "cget"] ||
         [string equal $cmd "bind"] } {
        #+++ return [eval [list LabelEntry::$cmd $path] $larg]
	set rttmp [eval [list [self] $cmd $path] $larg]
	puts "\n-->LabelEntry::_path_command - IF-BLOCK\n  \
	     rttmp: $rttmp\n"
	return [eval [list [self] $cmd $path] $larg]; #+++
    } else {
	set rttmp [eval [list $path.e:cmd $cmd] $larg]
	puts "\n-->LabelEntry::_path_command - ELSE-BLOCK\n  \
	     rttmp: $rttmp\n"
        return [eval [list $path.e:cmd $cmd] $larg]
    }
}


NXBwidget::LabelEntry public method _destroy { path } {
    :destroy $path; #+++
}


#+++ (added by me)
NXBwidget::LabelEntry public method storePath {path} {
    set :wpath $path
}


#+++ (added by me)
NXBwidget::LabelEntry public method retrievePath {} {
    return ${:wpath}
}
