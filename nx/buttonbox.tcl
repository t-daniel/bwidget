# ----------------------------------------------------------------------------
#  buttonbox.tcl
#  This file is part of Unifix BWidget Toolkit
# ----------------------------------------------------------------------------
#  Index of commands:
#     - ButtonBox::create
#     - ButtonBox::configure
#     - ButtonBox::cget
#     - ButtonBox::add
#     - ButtonBox::itemconfigure
#     - ButtonBox::itemcget
#     - ButtonBox::setfocus
#     - ButtonBox::invoke
#     - ButtonBox::index
#     - ButtonBox::_destroy
# ----------------------------------------------------------------------------
package provide buttonbox 1.0

package require nx
package require widget
package require button



namespace eval NXBwidget {

nx::Class create ButtonBox -superclass NXBwidget::Widget {
    
    #+++
    :public method initVars args {
    	next
	
	set :_inst_Button [NXBwidget::Button new]
    }
    
    
    #+++
    :public method init {} {
	:initVars;  #+++
	
	#+++
	:define ButtonBox buttonbox Button
	
	:declare ButtonBox {
	    {-background  TkResource ""	    0 frame}
	    {-orient      Enum	 horizontal 1 {horizontal vertical}}
	    {-state	      Enum	 "normal"   0 {normal disabled}}
	    {-homogeneous Boolean	 1	    1}
	    {-spacing     Int	 10	    0 "%d >= 0"}
	    {-padx	      TkResource ""	    0 button}
	    {-pady	      TkResource ""	    0 button}
	    {-default     Int	 -1	    0 "%d >= -1"}
	    {-bg	      Synonym	 -background}
	}

	:addmap ButtonBox "" :cmd {-background {}}

	bind ButtonBox <Destroy> [list [self] _destroy %W]
	#+++
    }

}

}




# ----------------------------------------------------------------------------
#  Command ButtonBox::create
# ----------------------------------------------------------------------------
NXBwidget::ButtonBox public method create { path args } {
    :storePath $path
    :widget_init ButtonBox $path $args

    variable $path
    upvar 0  $path data
    #upvar 0 :pmpath data; #+++

    #+++
    eval [list frame $path] [:subcget $path :cmd] \
	[list -class ButtonBox -takefocus 0 -highlightthickness 0]
    # For 8.4+ we don't want to inherit the padding
    catch {$path configure -padx 0 -pady 0}

    set data(max)      0
    set data(nbuttons) 0
    set data(buttons)  [list]
    set data(default)  [:getoption $path -default]; #+++

    return [next "ButtonBox $path"]
}


# ----------------------------------------------------------------------------
#  Command ButtonBox::configure
# ----------------------------------------------------------------------------
NXBwidget::ButtonBox public method configure { path args } {
    variable $path
    upvar 0  $path data
    #upvar 0 :pmpath data; #+++

    set res [:widget_configure $path $args]; #+++

    if { [:hasChanged $path -default val] } {
        if { $data(default) != -1 && $val != -1 } {
            set but $path.b$data(default)
            if { [winfo exists $but] } {
                $but configure -default normal
            }
            set but $path.b$val
            if { [winfo exists $but] } {
                $but configure -default active
            }
            set data(default) $val
        } else {
            :setoption $path -default $data(default); #+++
        }
    }

    #+++
    if {[:hasChanged $path -state val]} {
	foreach i $data(buttons) {
	    $path.b$i configure -state $val
	}
    }

    return $res
}


# ----------------------------------------------------------------------------
#  Command ButtonBox::cget
# ----------------------------------------------------------------------------
#+++ NXBwidget::ButtonBox public method cget { path option } {
#    return [Widget::cget $path $option]
#}


# ----------------------------------------------------------------------------
#  Command ButtonBox::add
# ----------------------------------------------------------------------------
NXBwidget::ButtonBox public method add { path args } {
    return [eval [linsert $args 0 [self] insert $path end]]; #+++
}


NXBwidget::ButtonBox public method insert { path idx args } {
    variable $path
    upvar 0  $path data
    #upvar 0 :pmpath data; #+++

    set but     $path.b$data(nbuttons)
    set spacing [:getoption $path -spacing]; #+++

    ## Save the current spacing setting for this button.  Buttons
    ## appended to the end of the box have their spacing applied
    ## to their left while all other have their spacing applied
    ## to their right.
    if {$idx == "end"} {
	set data(spacing,$data(nbuttons)) [list left $spacing]
	lappend data(buttons) $data(nbuttons)
    } else {
	set data(spacing,$data(nbuttons)) [list right $spacing]
        set data(buttons) [linsert $data(buttons) $idx $data(nbuttons)]
    }

    if { $data(nbuttons) == $data(default) } {
        set style active
    } elseif { $data(default) == -1 } {
        set style disabled
    } else {
        set style normal
    }

    array set flags $args
    set tags ""
    if { [info exists flags(-tags)] } {
	set tags $flags(-tags)
	unset flags(-tags)
	set args [array get flags]
    }

    #+++
    set __btn [NXBwidget::Button new]
    
    if { ${:_theme}} {
	puts "\n-->ButtonBox::insert - create 1\n  args: $args\n  but: $but\n"
        eval [list $__btn create $but] \
            $args [list -default $style]
	} else {
	puts "\n-->ButtonBox::insert - create 2\n  \
	     args: $args\n  but: $but\n"
	#Dieser Zweig wird auch in der urspruenglichen Version ausgeführt.
        eval [list $__btn create $but \
    	      -background [:getoption $path -background]\
	          -padx       [:getoption $path -padx] \
	          -pady       [:getoption $path -pady]] \
            $args [list -default $style]
	}

    # ericm@scriptics.com:  set up tags, just like the menu items
    foreach tag $tags {
	lappend data(tags,$tag) $but
	if { ![info exists data(tagstate,$tag)] } {
	    set data(tagstate,$tag) 0
	}
    }
    set data(buttontags,$but) $tags
    # ericm@scriptics.com

    :_redraw $path; #+++

    incr data(nbuttons)

    return $but
}


NXBwidget::ButtonBox public method delete { path idx } {
    variable $path
    upvar 0  $path data
    #upvar 0 :pmpath data; #+++

    set i [lindex $data(buttons) $idx]
    set data(buttons) [lreplace $data(buttons) $idx $idx]
    destroy $path.b$i
}


# ButtonBox::setbuttonstate --
#
#	Set the state of a given button tag.  If this makes any buttons
#       enable-able (ie, all of their tags are TRUE), enable them.
#
# Arguments:
#	path        the button box widget name
#	tag         the tag to modify
#	state       the new state of $tag (0 or 1)
#
# Results:
#	None.

NXBwidget::ButtonBox public method setbuttonstate {path tag state} {
    variable $path
    upvar 0  $path data
    #upvar 0 :pmpath data; #+++
    
    # First see if this is a real tag
    if { [info exists data(tagstate,$tag)] } {
	set data(tagstate,$tag) $state
	foreach but $data(tags,$tag) {
	    set expression "1"
	    foreach buttontag $data(buttontags,$but) {
		append expression " && $data(tagstate,$buttontag)"
	    }
	    if { [expr $expression] } {
		set state normal
	    } else {
		set state disabled
	    }
	    $but configure -state $state
	}
    }
    return
}

# ButtonBox::getbuttonstate --
#
#	Retrieve the state of a given button tag.
#
# Arguments:
#	path        the button box widget name
#	tag         the tag to modify
#
# Results:
#	None.

NXBwidget::ButtonBox public method getbuttonstate {path tag} {
    variable $path
    upvar 0  $path data
    #upvar 0 :pmpath data; #+++
    
    # First see if this is a real tag
    if { [info exists data(tagstate,$tag)] } {
	return $data(tagstate,$tag)
    } else {
	error "unknown tag $tag"
    }
}

# ----------------------------------------------------------------------------
#  Command ButtonBox::itemconfigure
# ----------------------------------------------------------------------------
NXBwidget::ButtonBox public method itemconfigure { path index args } {
    if { [set idx [lsearch $args -default]] != -1 } {
        set args [lreplace $args $idx [expr {$idx+1}]]
    }
    #+++
    set __btn [NXBwidget::Button new]
    return [eval [list $__btn configure $path.b[:index $path $index]] $args]; #+++
}


# ----------------------------------------------------------------------------
#  Command ButtonBox::itemcget
# ----------------------------------------------------------------------------
NXBwidget::ButtonBox public method itemcget { path index option } {
    #+++
    set __btn [NXBwidget::Button new]
    return [$__btn cget $path.b[:index $path $index] $option]; #+++
}


# ----------------------------------------------------------------------------
#  Command ButtonBox::setfocus
# ----------------------------------------------------------------------------
NXBwidget::ButtonBox public method setfocus { path index } {
    set but $path.b[:index $path $index]; #+++
    if { [winfo exists $but] } {
        focus $but
    }
}


# ----------------------------------------------------------------------------
#  Command ButtonBox::invoke
# ----------------------------------------------------------------------------
NXBwidget::ButtonBox public method invoke { path index } {
    set but $path.b[:index $path $index]; #+++
    if { [winfo exists $but] } {
	set __btn [NXBwidget::Button new]
	
        $__btn invoke $but; #+++
    }
}


# ----------------------------------------------------------------------------
#  Command ButtonBox::index
# ----------------------------------------------------------------------------
NXBwidget::ButtonBox public method index { path index } {
    variable $path
    upvar 0  $path data
    #upvar 0 :pmpath data; #+++

    set n [expr {$data(nbuttons) - 1}]

    if {[string equal $index "default"]} {
        set res [:getoption $path -default]
    } elseif {$index == "end" || $index == "last"} {
	set res $n
    } elseif {![string is integer -strict $index]} {
	## It's not an integer.  Search the text of each button
	## in the box and return the index that matches.
	foreach i $data(buttons) {
	    set w $path.b$i
	    lappend text  [$w cget -text]
	    lappend names [$w cget -name]
	}
	set res [lsearch -exact [concat $names $text] $index]
    } else {
        set res $index
	if {$index > $n} { set res $n }
    }
    return $res
}


# ButtonBox::gettags --
#
#	Return a list of all the tags on all the buttons in a buttonbox.
#
# Arguments:
#	path      the buttonbox to query.
#
# Results:
#	taglist   a list of tags on the buttons in the buttonbox

NXBwidget::ButtonBox public method gettags {path} {
    #+++ upvar ::ButtonBox::$path data
    upvar 0 :ButtonBox_$path data; #+++???
    
    set taglist {}
    foreach tag [array names data "tags,*"] {
	lappend taglist [string range $tag 5 end]
    }
    return $taglist
}


# ----------------------------------------------------------------------------
#  Command ButtonBox::_redraw
# ----------------------------------------------------------------------------
NXBwidget::ButtonBox public method _redraw { path } {
    variable $path
    upvar 0  $path data
    #upvar 0 :pmpath data; #+++
    
    :getVariable $path buttons; #+++

    # For tk >= 8.4, -uniform gridding option is used.
    # Otherwise, there is the constraint, that button size may not change after
    # creation.
    set uniformAvailable [expr {0 <= [package vcompare [info patchlevel] 8.4.0]}]

    ## We re-grid the buttons from left-to-right.  As we go through
    ## each button, we check its spacing and which direction the
    ## spacing applies to.  Once spacing has been applied to an index,
    ## it is not changed.  This means spacing takes precedence from
    ## left-to-right.

    set idx  0
    set idxs [list]
    foreach i $data(buttons) {
	set dir     [lindex $data(spacing,$i) 0]
	set spacing [lindex $data(spacing,$i) 1]
        set but $path.b$i
	#+++
        if {[string equal [:getoption $path -orient] "horizontal"]} {
            grid $but -column $idx -row 0 -sticky nsew
            if { [:getoption $path -homogeneous] } {
                if {$uniformAvailable} {
                    grid columnconfigure $path $idx -uniform koen -weight 1
                } else {
                    set req [winfo reqwidth $but]
                    if { $req > $data(max) } {
                        grid columnconfigure $path [expr {2*$i}] -minsize $req
                        set data(max) $req
                    }
                    grid columnconfigure $path $idx -weight 1
                }
            } else {
                grid columnconfigure $path $idx -weight 0
            }

	    set col [expr {$idx - 1}]
	    if {[string equal $dir "right"]} { set col [expr {$idx + 1}] }
	    if {$col > 0 && [lsearch $idxs $col] < 0} {
		lappend idxs $col
		grid columnconfigure $path $col -minsize $spacing
	    }
        } else {
            grid $but -column 0 -row $idx -sticky nsew
            grid rowconfigure $path $idx -weight 0

	    set row [expr {$idx - 1}]
	    if {[string equal $dir "right"]} { set row [expr {$idx + 1}] }
	    if {$row > 0 && [lsearch $idxs $row] < 0} {
		lappend idxs $row
		grid rowconfigure $path $row -minsize $spacing
	    }
        }
        incr idx 2
    }

    if {!$uniformAvailable} {
        # Now that the maximum size has been calculated, go back through
        # and correctly set the size for homogeneous horizontal buttons.
	
	#+++
        if { [string equal [:getoption $path -orient] "horizontal"] && [:getoption $path -homogeneous] } {
            set idx 0
            foreach i $data(buttons) {
                grid columnconfigure $path $idx -minsize $data(max)
                incr idx 2
            }
        }
    }
}


# ----------------------------------------------------------------------------
#  Command ButtonBox::_destroy
# ----------------------------------------------------------------------------
NXBwidget::ButtonBox public method _destroy { path } {
    variable $path
    upvar 0  $path data
    #upvar 0 :pmpath data; #+++
    
    :destroy $path; #+++
    unset data
}


#+++ (added by me)
NXBwidget::ButtonBox public method storePath {path} {
    set :wpath $path
}


#+++ (added by me)
NXBwidget::ButtonBox public method retrievePath {} {
    return ${:wpath}
}
