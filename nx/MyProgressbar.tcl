#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide myprogressbar 1.0

package require nx
package require bwidget
package require progressbar



nx::Class create Demo {
    variable status
    #set prgindic 1
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


Demo public method create { } {
    #Demo::createMainwindow
    #set frame   [${Demo::mainwin} getframe]
    
    
    set topf  [frame .topf]
    pack $topf -pady 2 -fill x
    
    
    #create the ProgressBar
    set prg   [NXBwidget::ProgressBar new]
    $prg create $topf.prg -width 100 -height 10 -background white \
	    -variable DemoGlob::prgindic -maximum 10
    pack [$prg retrievePath] -side top -pady 20m
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    update idletasks
    
    
    #update the PorgressBar
    :_update_progbar $prg
}


Demo public method _update_progbar {prg} {
    if {${DemoGlob::prgindic} < 11} {
    	incr DemoGlob::prgindic
        puts "a: ${DemoGlob::prgindic}"
        update
        after 1000 [self] _update_progbar $prg
    }
}


Demo public method init args {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm geometry . 300x200
    wm title . "ProgressBar demo"
    
    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

namespace eval DemoGlob {
    set prgindic 1
}

#Demo::main
set dm [Demo new]
wm geom . [wm geom .]
