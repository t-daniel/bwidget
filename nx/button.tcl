# ----------------------------------------------------------------------------
#  button.tcl
#  This file is part of Unifix BWidget Toolkit
# ----------------------------------------------------------------------------
#  Index of commands:
#   Public commands
#     - Button::create
#     - Button::configure
#     - Button::cget
#     - Button::invoke
#   Private commands (event bindings)
#     - Button::_destroy
#     - Button::_enter
#     - Button::_leave
#     - Button::_press
#     - Button::_release
#     - Button::_repeat
# ----------------------------------------------------------------------------
package provide button 1.0

package require nx
package require widget
package require dynamichelp



namespace eval NXBwidget {

nx::Class create Button -superclass NXBwidget::Widget -mixin NXBwidget::DynamicHelp {

    #+++
    :public method initVars args {
	#initialize the classvariables of the superclasses + mixinclasses and the ones of self
	next
	:dynhelp_init; #+++
	
	set :_current ""
	set :_pressed ""
    }
    
    
    #+++
    :public method init {} {
    	:initVars;  #+++
	
	:define Button button DynamicHelp; #+++
	
	set remove [list -command -relief -text -textvariable -underline -state]
	if {[info tclversion] > 8.3} {
	    lappend remove -repeatdelay -repeatinterval
	}
	
	:tkinclude Button button :cmd remove $remove; #+++
	
	#+++
	:declare Button {
	    {-name            String "" 0}
	    {-text            String "" 0}
	    {-textvariable    String "" 0}
	    {-underline       Int    -1 0 "%d >= -1"}
	    {-armcommand      String "" 0}
	    {-disarmcommand   String "" 0}
	    {-command         String "" 0}
	    {-state           TkResource "" 0 button}
	    {-repeatdelay     Int    0  0 "%d >= 0"}
	    {-repeatinterval  Int    0  0 "%d >= 0"}
	    {-relief          Enum   raised  0 {raised sunken flat ridge solid groove link}}
	}
	
	:dyn_include Button balloon; #+++ (DynamicHelp::include)
	
	:syncoptions Button "" :cmd {-text {} -underline {}}; #+++
	
	#+++
	bind BwButton <Enter>           "[self] _enter %W"
	bind BwButton <Leave>           "[self] _leave %W"
	bind BwButton <ButtonPress-1>   "[self] _press %W"
	bind BwButton <ButtonRelease-1> "[self] _release %W"
	bind BwButton <Key-space>       "[self] invoke %W; break"
	bind BwButton <Return>          "[self] invoke %W; break"
	bind BwButton <<Invoke>> 	"[self] invoke %W; break"
	bind BwButton <Destroy>         "[self] destroy %W"
    }
    
    
    #+++ Widget::define Button button DynamicHelp

    #+++ set remove [list -command -relief -text -textvariable -underline -state]
    #if {[info tclversion] > 8.3} {
    #	lappend remove -repeatdelay -repeatinterval
    #}
    
    #+++ Widget::tkinclude Button button :cmd remove $remove

    #+++ Widget::declare Button {
    #    {-name            String "" 0}
    #    {-text            String "" 0}
    #    {-textvariable    String "" 0}
    #    {-underline       Int    -1 0 "%d >= -1"}
    #    {-armcommand      String "" 0}
    #    {-disarmcommand   String "" 0}
    #    {-command         String "" 0}
    #    {-state           TkResource "" 0 button}
    #    {-repeatdelay     Int    0  0 "%d >= 0"}
    #    {-repeatinterval  Int    0  0 "%d >= 0"}
    #    {-relief          Enum   raised  0 {raised sunken flat ridge solid groove link}}
    #}

    #+++ DynamicHelp::include Button balloon

    #+++ Widget::syncoptions Button "" :cmd {-text {} -underline {}}

    #+++ variable _current ""
    #+++ variable _pressed ""

    #+++
    #bind BwButton <Enter>           {Button::_enter %W}
    #bind BwButton <Leave>           {Button::_leave %W}
    #bind BwButton <ButtonPress-1>   {Button::_press %W}
    #bind BwButton <ButtonRelease-1> {Button::_release %W}
    #bind BwButton <Key-space>       {Button::invoke %W; break}
    #bind BwButton <Return>          {Button::invoke %W; break}
    #bind BwButton <<Invoke>> 	    {Button::invoke %W; break}
    #bind BwButton <Destroy>         {Widget::destroy %W}
}; #END - class

}; #END - namespace



# ----------------------------------------------------------------------------
#  Command Button::create
# ----------------------------------------------------------------------------
NXBwidget::Button public method create { path args } {
    :storePath $path; #+++ added by me
    
    puts "\n-->Button::create\n  path: $path\n  args: $args\n"
    
    array set maps [list Button {} :cmd {}]
    array set maps [:parseArgs Button $args]; #+++
    #+++
    if {${:_theme}} {
        eval [concat [list ttk::button $path] $maps(:cmd)]
    } else {
        eval [concat [list button $path] $maps(:cmd)]
    }
    #+++ Widget::initFromODB Button $path $maps(Button)
    puts "\n-->Button::create\n  maps(Button): $maps(Button)\n"
    :initFromODB Button $path $maps(Button); #+++

    # Do some extra configuration on the button
    set var [:getMegawidgetOption $path -textvariable]; #+++
    set st [:getMegawidgetOption $path -state]; #+++
    puts "\n-->Button::create\n  var: '$var'\n  st: '$st'\n"
    if {  ![string length $var] } {
        #+++ set desc [BWidget::getname [Widget::getMegawidgetOption $path -name]]
	set desc [:getname [:getMegawidgetOption $path -name]]; #+++
	puts "\n-->Button::create\n  getMega: [:getMegawidgetOption $path -name]\n"
	puts "\n-->Button::create\n  desc: '$desc'\n"
	#+++ The BUG ist in der Methode getname (in Bwidget). Die gibt eine leere Zeichenkette zurück, was
	#+++ nicht korrekt ist.
        if { [llength $desc] } {
            set text  [lindex $desc 0]
            set under [lindex $desc 1]
            :widget_configure $path [list -text $text]; #+++
            :widget_configure $path [list -underline $under]; #+++
        } else {
            set text  [:getMegawidgetOption $path -text]; #+++
            set under [:getMegawidgetOption $path -underline]; #+++
        }
    } else {
        set under -1
        set text  ""
        :widget_configure $path [list -underline $under]; #+++
    }

    $path configure -text $text -underline $under \
        -textvariable $var -state $st
    # Map relief flat on Toolbutton for ttk
    set relief [:getMegawidgetOption $path -relief]; #+++
    #+++
    if {${:_theme}} {
        if { [string equal $relief "link"] } {
            $path configure -style Toolbutton
        }
    } else {
        if { [string equal $relief "link"] } {
            set relief "flat"
        }
        $path configure -relief $relief
    }
    bindtags $path [list $path BwButton [winfo toplevel $path] all]

    set accel1 [string tolower [string index $text $under]]
    set accel2 [string toupper $accel1]
    if { $accel1 != "" } {
        bind [winfo toplevel $path] <Alt-$accel1> [list [self] invoke $path]; #+++
        bind [winfo toplevel $path] <Alt-$accel2> [list [self] invoke $path]; #+++
    }

    #+++ DynamicHelp::sethelp $path $path 1
    :sethelp $path $path 1; #+++

    return [next "Button $path"]; #+++
}


# ----------------------------------------------------------------------------
#  Command Button::configure
# ----------------------------------------------------------------------------
NXBwidget::Button public method configure { path args } {
    set oldunder [$path:cmd cget -underline]
    if { $oldunder != -1 } {
        set oldaccel1 [string tolower [string index [$path:cmd cget -text] $oldunder]]
        set oldaccel2 [string toupper $oldaccel1]
    } else {
        set oldaccel1 ""
        set oldaccel2 ""
    }
    set res [:widget_configure $path $args]; #+++

    # Extract all the modified bits we're interested in
    #+++
    foreach {cr cs cv cn ct cu} [:hasChangedX $path \
        -relief -state -textvariable -name -text -underline] break
    if { $cr || $cs } {
        set relief [:cget $path -relief]; #+++
        set state  [:cget $path -state]; #+++
	#+++
        if { ${:_theme}} {
			if { [string equal $relief "link"] } {
				$path:cmd configure -style Toolbutton
			} else {
				$path:cmd configure -style ""
			}
		} else {
			if { [string equal $relief "link"] } {
				if { [string equal $state "active"] } {
					set relief "raised"
				} else {
					set relief "flat"
				}
			}
			$path:cmd configure -relief $relief
		}
		$path:cmd configure -state $state
    }

    if { $cv || $cn || $ct || $cu } {
        set var		[:cget $path -textvariable]; #+++
        set text	[:cget $path -text]; #+++
        set under	[:cget $path -underline]; #+++
        if {  ![string length $var] } {
            set desc [:getname [:cget $path -name]]; #+++
            if { [llength $desc] } {
                set text  [lindex $desc 0]
                set under [lindex $desc 1]
            }
        } else {
            set under -1
            set text  ""
        }
        set top [winfo toplevel $path]
        if { $oldaccel1 != "" } {
            bind $top <Alt-$oldaccel1> {}
            bind $top <Alt-$oldaccel2> {}
        }
        set accel1 [string tolower [string index $text $under]]
        set accel2 [string toupper $accel1]
        if { $accel1 != "" } {
            bind $top <Alt-$accel1> [list [self] invoke $path]; #+++
            bind $top <Alt-$accel2> [list [self] invoke $path]; #+++
        }
        $path:cmd configure -text $text -underline $under -textvariable $var
    }
    #+++ DynamicHelp:sethelp $path $path
    :sethelp $path $path; #+++

    set res
}


# ----------------------------------------------------------------------------
#  Command Button::cget
# ----------------------------------------------------------------------------
#+++ proc Button::cget { path option } {
#    Widget::cget $path $option
#}


# ----------------------------------------------------------------------------
#  Command Button::identify
# ----------------------------------------------------------------------------
NXBwidget::Button public method identify { path args } {
    eval $path:cmd identify $args
}


# ----------------------------------------------------------------------------
#  Command Button::instate
# ----------------------------------------------------------------------------
NXBwidget::Button public method instate { path args } {
    eval $path:cmd instate $args
}


# ----------------------------------------------------------------------------
#  Command Button::state
# ----------------------------------------------------------------------------
NXBwidget::Button public method state { path args } {
    eval $path:cmd state $args
}


# ----------------------------------------------------------------------------
#  Command Button::invoke
# ----------------------------------------------------------------------------
NXBwidget::Button public method invoke { path } {
    if { ![string equal [$path:cmd cget -state] "disabled"] } {
	#+++
        if { ${:_theme}} {
            $path:cmd configure -state active
			$path:cmd state pressed
        } else {
            $path:cmd configure -state active -relief sunken
        }
        update idletasks
        set cmd [:getMegawidgetOption $path -armcommand]; #+++
        if { $cmd != "" } {
            uplevel \#0 $cmd
        }
        after 100
        $path:cmd configure -state [:getMegawidgetOption $path -state]; #+++
	#+++
        if { ${:_theme}} {
            $path:cmd state !pressed
        } else {
            set relief [:getMegawidgetOption $path -relief]; #+++
            if { [string equal $relief "link"] } {
                set relief flat
            }
            $path:cmd configure -relief $relief
        }
        set cmd [:getMegawidgetOption $path -disarmcommand]; #+++
        if { $cmd != "" } {
            uplevel \#0 $cmd
        }
        set cmd [:getMegawidgetOption $path -command]; #+++
        if { $cmd != "" } {
            uplevel \#0 $cmd
        }
    }
}

# ----------------------------------------------------------------------------
#  Command Button::_enter
# ----------------------------------------------------------------------------
NXBwidget::Button public method _enter { path } {
    #+++
    #variable _current
    #variable _pressed
    
    upvar 0 :_current _current
    upvar 0 :_pressed _pressed
    #+++
    
    puts "\n-->Button::_enter\n  path: $path\n"; #+++

    set _current $path
    if { ![string equal [$path:cmd cget -state] "disabled"] } {
        $path:cmd configure -state active
	#+++
        if { ${:_theme} } {
            # $path:cmd state active
        } else {
            if { $_pressed == $path } {
                $path:cmd configure -relief sunken
            } elseif { [string equal [:cget $path -relief] "link"] } {
                $path:cmd configure -relief raised
            }
        }
    }
}


# ----------------------------------------------------------------------------
#  Command Button::_leave
# ----------------------------------------------------------------------------
NXBwidget::Button public method _leave { path } {
    #+++
    #variable _current
    #variable _pressed
    
    upvar 0 :_current _current
    upvar 0 :_pressed _pressed
    #+++

    set _current ""
    if { ![string equal [$path:cmd cget -state] "disabled"] } {
        $path:cmd configure -state [:cget $path -state]; #+++
	#+++
        if { ${:_theme} } {
        } else {
            set relief [:cget $path -relief]; #+++
            if { $_pressed == $path } {
                if { [string equal $relief "link"] } {
                    set relief raised
                }
                $path:cmd configure -relief $relief
            } elseif { [string equal $relief "link"] } {
                $path:cmd configure -relief flat
            }
        }
    }
}


# ----------------------------------------------------------------------------
#  Command Button::_press
# ----------------------------------------------------------------------------
NXBwidget::Button public method _press { path } {
    #+++ variable _pressed
    upvar 0 :_pressed _pressed; #+++

    if { ![string equal [$path:cmd cget -state] "disabled"] } {
        set _pressed $path
	#+++
        if { ${:_theme}} {
            ttk::clickToFocus $path
            $path state pressed
        } else {
            $path:cmd configure -relief sunken
        }
        set cmd [:getMegawidgetOption $path -armcommand]; #+++
        if { $cmd != "" } {
            uplevel \#0 $cmd
	    set repeatdelay [:getMegawidgetOption $path -repeatdelay]; #+++
	    set repeatint [:getMegawidgetOption $path -repeatinterval]; #+++
            if { $repeatdelay > 0 } {
                #+++ after $repeatdelay "Button::_repeat $path"
		after $repeatdelay "[self] _repeat $path"; #+++
            } elseif { $repeatint > 0 } {
                #+++ after $repeatint "Button::_repeat $path"
		after $repeatint "[self] _repeat $path"
	    }
        }
    }
}


# ----------------------------------------------------------------------------
#  Command Button::_release
# ----------------------------------------------------------------------------
NXBwidget::Button public method _release { path } {
    #+++
    #variable _current
    #variable _pressed
    
    upvar 0 :_current _current
    upvar 0 :_pressed _pressed
    #+++

    if { $_pressed == $path } {
        set _pressed ""
	#+++ after cancel "Button::_repeat $path"
	after cancel "[self] _repeat $path"; #+++
	#+++
        if { ${:_theme}} {
            $path state !pressed
        } else {
            set relief [:getMegawidgetOption $path -relief]; #+++
            if { [string equal $relief "link"] } {
                set relief raised
            }
            $path:cmd configure -relief $relief
        }
	set cmd [:getMegawidgetOption $path -disarmcommand]; #+++
        if { $cmd != "" } {
            uplevel \#0 $cmd
        }
        if { $_current == $path &&
             ![string equal [$path:cmd cget -state] "disabled"] && \
	     [set cmd [:getMegawidgetOption $path -command]] != "" } {
            uplevel \#0 $cmd; #+++
        }
    }
}


# ----------------------------------------------------------------------------
#  Command Button::_repeat
# ----------------------------------------------------------------------------
NXBwidget::Button public method _repeat { path } {
    #+++
    variable _current
    variable _pressed
    
    upvar 0 :_current _current
    upvar 0 :_pressed _pressed
    #+++

    if { $_current == $path && $_pressed == $path &&
         ![string equal [$path:cmd cget -state] "disabled"] &&
         [set cmd [:getMegawidgetOption $path -armcommand]] != "" } {
        uplevel \#0 $cmd; #+++
    }
    if { $_pressed == $path &&
         ([set delay [:getMegawidgetOption $path -repeatinterval]] >0 ||
          [set delay [:getMegawidgetOption $path -repeatdelay]] > 0) } {
        after $delay "[self] _repeat $path"; #+++
    }
}


#+++ (added by me)
NXBwidget::Button public method storePath {path} {
    set :wpath $path
}


#+++ (added by me)
NXBwidget::Button public method retrievePath {} {
    return ${:wpath}
}
