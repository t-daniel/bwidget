#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide myspinbox 1.0

package require nx
package require widget
package require spinbox
package require mainframe
package require labelentry



nx::Class create Demo {
    #variable status
    #variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set :mainwin [NXBwidget::MainFrame new]
    ${:mainwin} create .mainframe \
                       -menu         $descmenu \
                       -textvariable DemoGlob::status \
                       -progressvar  DemoGlob::prgindic
}


Demo public method create { } {
    :createMainwindow
    set frame    [${:mainwin} getframe [${:mainwin} retrievePath]]
    
    
    #create the SpinBox
    set spin  [NXBwidget::SpinBox new]
    $spin create $frame.spin \
                   -range {1 100 1} -textvariable DemoGlob::spinbox \
                   -helptext "This is the SpinBox"
    
    set ent   [NXBwidget::LabelEntry new]
    $ent create $frame.ent -label "Selected \n item" -labelwidth 10 -labelanchor w \
                   -textvariable DemoGlob::spinbox -editable 0 \
                   -helptext "This is an Entry reflecting\nthe selected item of the SpinBox"
    
    
    #In the statusbar the value of the SpinBox can be seen.
    pack [$spin retrievePath] -pady 4 -fill x
    
    pack [$ent retrievePath] -pady 4 -fill x
    
    
    #pack the mainframe
    pack [${:mainwin} retrievePath] -fill both -expand yes
    
    ${:mainwin} place . 300 180 right
    
    
    update idletasks
}


Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "SpinBox demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

namespace eval DemoGlob {
    array set var {}
    set status ""
    set prgindic 0
    set spinbox 0
}

set dm [Demo new]
wm geom . [wm geom .]
