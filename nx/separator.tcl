# ------------------------------------------------------------------------------
#  separator.tcl
#  This file is part of Unifix BWidget Toolkit
# ------------------------------------------------------------------------------
#  Index of commands:
#     - Separator::create
#     - Separator::configure
#     - Separator::cget
# ------------------------------------------------------------------------------
package provide separator 1.0

package require nx
package require widget


namespace eval NXBwidget {

nx::Class create Separator -superclass NXBwidget::Widget {

    #+++
    :public method initVars args {
	next
	#no classvariables/no mixin classes
    }
    
    
    #+++
    :public method init {} {
	:initVars;  #+++
	
	:define Separator separator; #+++

	#+++
	:declare Separator {
	    {-background TkResource ""         0 frame}
	    {-cursor     TkResource ""         0 frame}
	    {-relief     Enum       groove     0 {ridge groove}}
	    {-orient     Enum       horizontal 1 {horizontal vertical}}
	    {-bg         Synonym    -background}
	}
	:addmap Separator "" :cmd { -background {} -cursor {} }; #+++

	bind Separator <Destroy> [list [self] destroy %W]; #+++
    }

}; #END -class
    
}; #END - namespace




# ------------------------------------------------------------------------------
#  Command Separator::create
# ------------------------------------------------------------------------------
NXBwidget::Separator public method create { path args } {
    :storePath $path; #+++ (added by me)

    array set maps [list Separator {} :cmd {}]
    array set maps [:parseArgs Separator $args]; #+++
    eval [list frame $path] $maps(:cmd) -class Separator
    :initFromODB Separator $path $maps(Separator); #+++

    #+++
    if { [:cget $path -orient] == "horizontal" } {
	$path configure -borderwidth 1 -height 2
    } else {
	$path configure -borderwidth 1 -width 2
    }

    #+++
    if { [string equal [:cget $path -relief] "groove"] } {
	$path configure -relief sunken
    } else {
	$path configure -relief raised
    }
    
    
    return [next "Separator $path"]; #+++
}


# ------------------------------------------------------------------------------
#  Command Separator::configure
# ------------------------------------------------------------------------------
#+++ configure changed to separator_configure
NXBwidget::Separator public method configure { path args } {
    set res [:widget_configure $path $args]; #+++

    #+++
    if { [:hasChanged $path -relief relief] } {
        if { $relief == "groove" } {
            $path:cmd configure -relief sunken
        } else {
            $path:cmd configure -relief raised
        }
    }

    return $res
}


# ------------------------------------------------------------------------------
#  Command Separator::cget
# ------------------------------------------------------------------------------
#NXBwidget::Separator public method cget { path option } {
#    return [next "$path $option"]; #+++
#}


#+++ (added by me)
NXBwidget::Separator public method storePath {path} {
    set :wpath $path
}


#+++ (added by me)
NXBwidget::Separator public method retrievePath {} {
    return ${:wpath}
}

