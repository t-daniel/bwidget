#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide mymsgdialog 1.0

package require nx
package require widget
package require messagedialog
package require mainframe



nx::Class create Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set :mainwin [NXBwidget::MainFrame new]
    ${:mainwin} create .mainframe \
                       -menu         $descmenu
    
    #-textvariable Demo::status \
                       -progressvar  Demo::prgindic
}


Demo public method create { } {
    :createMainwindow
    
    
    set frame    [${:mainwin} getframe [${:mainwin} retrievePath]]
    
    
    #create the normal Dialog
    #set button1 [button $frame.but1 -text "Normal \n Dialog" -command ::_show_dialog]
    
    
    #create the MessageDialog
    set button2 [button $frame.but2 -text "Message \n Dialog" -command ::_show_msgdlg]
    
    
    #create the PasswordDialog
    #set button3 [button $frame.but3 -text "Password \n Dialog" -command Demo::_show_passdlg]
    
    
    #create the ProgressDialog
    #set button4 [button $frame.but4 -text "Progress \n Dialog" -command Demo::_show_prgdlg]
    
    
    #pack $button1 -side top -padx 4
    pack $button2 -side top -padx 4
    #pack $button3 -side top -padx 4
    #pack $button4 -side top -padx 4
    
    
    #pack the mainframe
    pack [${:mainwin} retrievePath] -fill both -expand yes
    update idletasks
}


Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "Dialog demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

set dm [Demo new]
wm geom . [wm geom .]






proc _show_dialog { } {
    #variable tmpl
    set tmpl(side)   bottom
    set tmpl(anchor) c

    set dlg [NXBwidget::Dialog new]
    $dlg create  .tmpldlg -parent . -modal local \
                 -separator 1 \
                 -title   "Normal dialog" \
                 -side    $tmpl(side)    \
                 -anchor  $tmpl(anchor) \
                 -default 0 -cancel 1
    
    $dlg add [$dlg retrievePath] -name ok
    $dlg add [$dlg retrievePath] -name cancel
    
    set msg [message [$dlg getframe [$dlg retrievePath]].msg -text "Template\nDialog" -justify center -anchor c]
    pack $msg -fill both -expand yes -padx 100 -pady 100
    $dlg draw [$dlg retrievePath]
    #not needed  pack [$dlg retrievePath] -fill both -expand yes
    destroy $dlg
}








proc _show_msgdlg { } {
    #variable msg
    set msg(type) ok
    set msg(icon) info
    set msg(buttons) ""

    destroy .msgdlg
    set msgdlg [NXBwidget::MessageDlg new]
    $msgdlg create .msgdlg -parent . \
        -message "This is a message" \
        -type    $msg(type) \
        -icon    $msg(icon) \
        -buttons $msg(buttons)
}


proc _show_passdlg { } {
    PasswdDlg .passwd -parent .
}


proc _show_prgdlg {} {
    #-type  infinite;  progress indicator runs from left to right, and back again ...
    ProgressDlg .progress -parent . -title "Wait..." \
        -width        20 \
	-type         infinite \
        -textvariable Dialog::progmsg \
        -variable     Dialog::progval \
        -stop         "Stop" \
        -command      {destroy .progress}
        
        
    #update the PorgressBar on the Dialog
    set Dialog::progmsg "Process running..."
    set Dialog::progval 0
    Demo::_update_progbar
}


proc _update_progbar {} {
    if {${Dialog::progval} < 10} {
        set Dialog::progval 2
        #puts "a: ${Dialog::progval}"
        update
        after 250 Demo::_update_progbar
    }
}
