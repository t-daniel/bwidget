#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}



namespace eval Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


proc Demo::createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


proc Demo::create { } {
    Demo::createMainwindow
    
    
    set frame    [${Demo::mainwin} getframe]
    
    
    #some padding
    set lab0 [label $frame.lab0 -text ""]
    pack $lab0 -pady 2 -fill x
    
    
    #create the PanelFrame
    set pfr  [PanelFrame $frame.pfr -text "This is a PanelFrame" -relief sunken -borderwidth 2]
    pack $pfr -pady 2 -fill x
    
    
    #fill the PanelFrame
    set pframe [$pfr getframe]
    set lab1 [label $pframe.lab1 -text "Label_1"]
    pack $lab1 -pady 2 -fill x
    
    set lab2 [label $pframe.lab2 -text "Label_2"]
    pack $lab2 -pady 2 -fill x
    
    set lb    [listbox $pframe.lb -height 8 -width 20 -highlightthickness 0]
    for {set i 1} {$i <= 4} {incr i} {
       $lb insert end "Value_${i}"
       pack $lb -side top -padx 4 -pady 2
    }
    
    
    #pack the mainframe
    pack ${Demo::mainwin} -fill both -expand yes
    update idletasks
}



proc Demo::main {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "PanelFrame demo"

    Demo::create
    BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    wm deiconify .
    raise .
    focus -force .
}

Demo::main
wm geom . [wm geom .]
