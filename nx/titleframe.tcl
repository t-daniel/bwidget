# ------------------------------------------------------------------------------
#  titleframe.tcl
#  This file is part of Unifix BWidget Toolkit
# ------------------------------------------------------------------------------
#  Index of commands:
#     - TitleFrame::create
#     - TitleFrame::configure
#     - TitleFrame::cget
#     - TitleFrame::getframe
#     - TitleFrame::_place
# ------------------------------------------------------------------------------
package provide titleframe 1.0

package require nx
package require widget


namespace eval NXBwidget {

nx::Class create TitleFrame -superclass NXBwidget::Widget {
    
    #+++
    :public method initVars args {
	next
	#no classvariables/no mixin classes
    }
    
    
    #+++
    :public method init {} {
	:initVars;  #+++
    
    
	:define TitleFrame titleframe; #+++

	#+++
        :declare TitleFrame {
            {-relief      TkResource groove 0 frame}
            {-borderwidth TkResource 2      0 frame}
            {-font        TkResource ""     0 label}
            {-foreground  TkResource ""     0 label}
            {-state       TkResource ""     0 label}
            {-background  TkResource ""     0 frame}
            {-text        String     ""     0}
            {-ipad        Int        4      0 "%d >=0"}
            {-side        Enum       left   0 {left center right}}
            {-baseline    Enum       center 0 {top center bottom}}
            {-fg          Synonym    -foreground}
            {-bg          Synonym    -background}
            {-bd          Synonym    -borderwidth}
        }

	#+++
	:addmap TitleFrame "" :cmd {-background {}}
	:addmap TitleFrame "" .l   {
		-background {} -foreground {} -text {} -font {}
	}
	:addmap TitleFrame "" .l   {-state {}}
	:addmap TitleFrame "" .p   {-background {}}
	:addmap TitleFrame "" .b   {
		-background {} -relief {} -borderwidth {}
	}
	:addmap TitleFrame "" .b.p {-background {}}
	:addmap TitleFrame "" .f   {-background {}}
	#+++
    }

}; #END - class

}; #END - namespace




# ------------------------------------------------------------------------------
#  Command TitleFrame::create
# ------------------------------------------------------------------------------
NXBwidget::TitleFrame public method create { path args } {
    puts "\nTitleFrame::create - BEGIN\n"; #+++
    :storePath $path; #+++ (added by me)
    
    :widget_init TitleFrame $path $args; #+++

    set frame  [eval [list frame $path] [:subcget $path :cmd] \
	    -class TitleFrame -relief flat -bd 0 -highlightthickness 0]; #+++

    set padtop [eval [list frame $path.p] [:subcget $path :cmd] \
	    -relief flat -borderwidth 0]; #+++
    set border [eval [list frame $path.b] [:subcget $path .b] -highlightthickness 0]; #+++
    set label  [eval [list label $path.l] [:subcget $path .l] \
                    -highlightthickness 0 \
                    -relief flat \
                    -bd     0 -padx 2 -pady 0]; #+++
    set padbot [eval [list frame $border.p] [:subcget $path .p] \
	    -relief flat -bd 0 -highlightthickness 0]; #+++
    set frame  [eval [list frame $path.f] [:subcget $path .f] \
	    -relief flat -bd 0 -highlightthickness 0]; #+++
    set height [winfo reqheight $label]

    #+++
    switch [:getoption $path -side] {
        left   { set relx 0.0; set x 5;  set anchor nw }
        center { set relx 0.5; set x 0;  set anchor n  }
        right  { set relx 1.0; set x -5; set anchor ne }
    }
    set bd [:getoption $path -borderwidth]; #+++
    #+++
    switch [:getoption $path -baseline] {
        top    {
	    set y    0
	    set htop $height
	    set hbot 1
	}
        center {
	    set y    0
	    set htop [expr {$height/2}]
	    set hbot [expr {$height/2+$height%2+1}]
	}
        bottom {
	    set y    [expr {$bd+1}]
	    set htop 1
	    set hbot $height
	}
    }
    $padtop configure -height $htop
    $padbot configure -height $hbot

    set pad [:getoption $path -ipad]; #+++
    pack $padbot -side top -fill x
    pack $frame  -in $border -fill both -expand yes -padx $pad -pady $pad

    pack $padtop -side top -fill x
    pack $border -fill both -expand yes

    place $label -relx $relx -x $x -anchor $anchor -y $y

    bind $label <Configure> [list [self] _place $path]; #+++
    bind $path  <Destroy>   [list [self] destroy %W]; #+++

    puts "\nTitleFrame::create - END\n"; #+++
    return [next "TitleFrame $path"]; #+++
}


# ------------------------------------------------------------------------------
#  Command TitleFrame::configure
# ------------------------------------------------------------------------------
NXBwidget::TitleFrame public method configure { path args } {
    set res [:widget_configure $path $args]; #+++
    puts "\n-->TitleFrame configure\n"

    #+++
    if { [:hasChanged $path -ipad pad] } {
        pack configure $path.f -padx $pad -pady $pad
    }
    #+++
    if { [:hasChanged $path -borderwidth val] |
         [:hasChanged $path -font        val] |
         [:hasChanged $path -side        val] |
         [:hasChanged $path -baseline    val] } {
        :_place $path
    }

    return $res
}


# ------------------------------------------------------------------------------
#  Command TitleFrame::cget
# ------------------------------------------------------------------------------
#NXBwidget::TitleFrame public method cget { path option } {
#    return [:cget $path $option]; #+++
#}


# ------------------------------------------------------------------------------
#  Command TitleFrame::getframe
# ------------------------------------------------------------------------------
NXBwidget::TitleFrame public method getframe { path } {
    return $path.f
}


# ------------------------------------------------------------------------------
#  Command TitleFrame::_place
# ------------------------------------------------------------------------------
NXBwidget::TitleFrame public method _place { path } {
    set height [winfo height $path.l]
    #+++
    switch [:getoption $path -side] {
        left    { set relx 0.0; set x 10;  set anchor nw }
        center  { set relx 0.5; set x 0;   set anchor n  }
        right   { set relx 1.0; set x -10; set anchor ne }
    }
    set bd [:getoption $path -borderwidth]; #+++
    #+++
    switch [:getoption $path -baseline] {
        top    { set htop $height; set hbot 1; set y 0 }
        center { set htop [expr {$height/2}]; set hbot [expr {$height/2+$height%2+1}]; set y 0 }
        bottom { set htop 1; set hbot $height; set y [expr {$bd+1}] }
    }
    $path.p   configure -height $htop
    $path.b.p configure -height $hbot

    place $path.l -relx $relx -x $x -anchor $anchor -y $y
}


#+++ (added by me)
NXBwidget::TitleFrame public method storePath {path} {
    set :wpath $path
}


#+++ (added by me)
NXBwidget::TitleFrame public method retrievePath {} {
    return ${:wpath}
}
