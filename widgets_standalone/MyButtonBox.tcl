#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}



namespace eval Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


proc Demo::createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


proc Demo::create { } {
    Demo::createMainwindow
    
    
    set frame    [${Demo::mainwin} getframe]
    
    
    #create the ButtonBox
    set bbox [ButtonBox $frame.bbox1 -spacing 0 -padx 1 -pady 1]
    
    #add image to the Buttonbox
    $bbox add -image [Bitmap::get info] \
        -highlightthickness 0 -takefocus 0 -relief link -borderwidth 1 -padx 1 -pady 1 \
        -helptext "Get info" \
        -command  "Demo::buttoncmd {Get info}";  #this comes from the widget Button
    
    $bbox add -image [Bitmap::get new] \
        -highlightthickness 0 -takefocus 0 -relief link -borderwidth 1 -padx 1 -pady 1 \
        -helptext "Create a new file" \
        -command  "Demo::buttoncmd {Create new file}";  #this comes from the widget Button
    pack $bbox -side top
    
    
    #pack the mainframe
    pack ${Demo::mainwin} -fill both -expand yes
    update idletasks
}


proc Demo::buttoncmd { txt } {
    puts "Button '${txt}' pressed!"
}



proc Demo::main {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "ButtonBox demo"

    Demo::create
    BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    wm deiconify .
    raise .
    focus -force .
}

Demo::main
wm geom . [wm geom .]
