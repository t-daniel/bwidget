#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide mylabelentry 1.0

package require nx
package require widget
package require labelentry



nx::Class create Demo {
    variable status
    variable prgindic
    variable var
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


Demo public method create { } {
    #Demo::createMainwindow
    #set frame    [${Demo::mainwin} getframe]
    
    set frame [frame .topf]
    pack $frame -pady 2 -fill x
    
    set DemoGlob::var(frame) $frame
    
    
    #create the LabelEntry
    set ent   [NXBwidget::LabelEntry new]
    #-command  {puts  "You typed: [$ent cget ${DemoGlob::var(frame)}.ent  -text]"}
    $ent create  $frame.ent -label "Input:" -labelwidth 10 -labelanchor w \
                   -textvariable DemoGlob::var(spin,var) -editable 1 \
                   -command  "puts  \"You typed: \[eval \{$ent cget \${DemoGlob::var(frame)}.ent  -text\}\]\" " \
                   -helptext "The LabelEntry Widget"
    pack [$ent retrievePath] -pady 4 -fill x
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    update idletasks
}



Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "LabelEntry demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

namespace eval DemoGlob {
    array set var {}
}

set dm [Demo new]
wm geom . [wm geom .]
