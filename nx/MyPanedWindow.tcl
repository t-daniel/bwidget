#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide mypanedwindow 1.0

package require nx
package require bwidget
package require panedwindow
package require titleframe



nx::Class create Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


Demo public method create { } {
    #Demo::createMainwindow
    
    #set frame    [${Demo::mainwin} getframe]
    
    
    set frame [frame .topf]
    pack $frame -pady 2 -fill x
    
    
    #create the PanedWindow
    #create the left side
    set pw    [NXBwidget::PanedWindow new]
    $pw create $frame.pw -side top
    set pane  [$pw add $frame.pw -weight 1]
    
    set titleLeft [NXBwidget::TitleFrame new]
    $titleLeft create $pane.lf -text "Left side"
    pack [$titleLeft retrievePath] -fill both -expand yes
    
    
    #create the right side
    set pane [$pw add $frame.pw -weight 2]
    set titleRight   [NXBwidget::TitleFrame new]
    $titleRight create $pane.rf -text "Right side"
    pack [$titleRight retrievePath] -fill both -expand yes
    
    
    pack [$pw retrievePath]  -fill both  -expand yes
    
    $pw place . 300 40 right;  #set size and position of the window
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    update idletasks
}



Demo public method init args {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "PanedWindow demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    wm geometry . 300x200
    raise .
    focus -force .
}

set dm [Demo new]
wm geom . [wm geom .]
