#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}



namespace eval Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


proc Demo::createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


proc Demo::create { } {
    Demo::createMainwindow
    
    
    set frame    [${Demo::mainwin} getframe]
    
    
    #create the ScrolledWindow. ScrollableFrame should be used, to make e.g. a scrollable list.
    set sw [ScrolledWindow $frame.sw -relief sunken -borderwidth 2]
    set sf [ScrollableFrame $sw.f]
    $sw setwidget $sf
    set subf [$sf getframe]
    
    for {set i 0} {$i <= 15} {incr i} {
      #creates Labels as list items in the scrollable frame
      #set lab [label $subf.lab_${i} -text "Label_${i}"]
      #pack $lab -side top -padx 4 -pady 2
      
      #crates Buttons as list items
      set but   [Button $subf.but_${i} -text "Button ${i}" \
                   -repeatdelay 300 \
                   -command  "Demo::buttoncmd $i" \
                   -helptext "This is a Button widget"]
      pack $but  -side top -padx 4 -pady 2
    }
    
    pack $sw  -side top  -expand yes -fill both
    
    
    #pack the mainframe
    pack ${Demo::mainwin} -fill both -expand yes
    update idletasks
}


proc Demo::buttoncmd {args} {
    puts "Button ${args} pressed!"
}



proc Demo::main {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "ScrolledWindow demo"

    Demo::create
    BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    wm deiconify .
    raise .
    focus -force .
}

Demo::main
wm geom . [wm geom .]
