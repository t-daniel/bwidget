# ------------------------------------------------------------------------------
#  labelframe.tcl
#  This file is part of Unifix BWidget Toolkit
#  $Id: labelframe.tcl,v 1.6.2.1 2011/02/14 16:56:09 oehhar Exp $
# ------------------------------------------------------------------------------
#  Index of commands:
#     - LabelFrame::create
#     - LabelFrame::getframe
#     - LabelFrame::configure
#     - LabelFrame::cget
#     - LabelFrame::align
# ------------------------------------------------------------------------------
package provide labelframe 1.0

package require nx
package require widget
package require label


namespace eval NXBwidget {

nx::Class create LabelFrame -superclass NXBwidget::Widget {
    
    #+++
    :public method initVars args {
	#initialize the classvariables of the superclasses + mixinclasses and the ones of self
	next
        
        set :_inst_Label [NXBwidget::Label new]
    }
    
    #+++
    :public method init {} {
    	:initVars;  #+++
        
        #+++
        :define LabelFrame labelframe Label
        
        :bwinclude LabelFrame Label .l \
          remove     {
            -highlightthickness -highlightcolor -highlightbackground
            -takefocus -relief -borderwidth
            -cursor
            -dragenabled -draginitcmd -dragendcmd -dragevent -dragtype
            -dropenabled -droptypes -dropovercmd  -dropcmd} \
          initialize {-anchor w}
            
        :declare LabelFrame {
            {-relief      TkResource flat 0 frame}
            {-borderwidth TkResource 0    0 frame}
            {-side        Enum       left 1 {left right top bottom}}
            {-bd          Synonym    -borderwidth}
        }
        
        :addmap LabelFrame "" :cmd {-background {}}
        :addmap LabelFrame "" .f   {-background {} -relief {} -borderwidth {}}

        :syncoptions LabelFrame Label .l {-text {} -underline {}}
        
        bind BwLabelFrame <FocusIn> [list ${:_inst_Label} setfocus %W.l]
        bind BwLabelFrame <Destroy> [list [self] _destroy %W]
        #+++
    }

}

}



# ----------------------------------------------------------------------------
#  Command LabelFrame::create
# ----------------------------------------------------------------------------
NXBwidget::LabelFrame public method create { path args } {
    :storePath $path; #+++
    
    #+++ Widget::init LabelFrame $path $args
    :widget_init LabelFrame $path $args; #+++

    #+++
    if {[:theme]} {
        set path  [eval [list ttk::frame $path] [:subcget $path :cmd] \
            -takefocus 0 \
            -class LabelFrame]
    }  else  {
        set path  [eval [list frame $path] [:subcget $path :cmd] \
            -relief flat -bd 0 -takefocus 0 -highlightthickness 0 \
            -class LabelFrame]
    }
    
    puts "\n-->LabelFrame::create\n  \
         path: $path\n  \
	 args: $args\n"
    
    #+++
    #set label [eval [list Label::create $path.l] [Widget::subcget $path .l] \
    #    -takefocus 0 -dropenabled 0 -dragenabled 0]
    
    set lbtemp [NXBwidget::Label new]
    set label [eval [list $lbtemp create $path.l] [:subcget $path .l] \
        -takefocus 0 -dropenabled 0 -dragenabled 0]
    #+++
    
    
    #+++
    if {[:theme]} {
        set frame [eval [list ttk::frame $path.f] [:subcget $path .f] \
            -takefocus 0]
    }  else  {
        set frame [eval [list frame $path.f] [:subcget $path .f] \
            -highlightthickness 0 -takefocus 0]
    }

    #+++
    switch  [:getoption $path -side] {
        left   {set packopt "-side left"}
        right  {set packopt "-side right"}
        top    {set packopt "-side top -fill x"}
        bottom {set packopt "-side bottom -fill x"}
    }
    
    puts "\n-->LabelFrame::create\n  \
         packopt: $packopt\n  \
	 label: $label\n  \
	 frame: $frame\n"

    eval [list pack $label] $packopt
    pack $frame -fill both -expand yes

    bindtags $path [list $path BwLabelFrame [winfo toplevel $path] all]

    #+++
    return [next "LabelFrame $path"]
}


# ----------------------------------------------------------------------------
#  Command LabelFrame::getframe
# ----------------------------------------------------------------------------
NXBwidget::LabelFrame public method getframe { path } {
    return $path.f
}


# ----------------------------------------------------------------------------
#  Command LabelFrame::configure
# ----------------------------------------------------------------------------
NXBwidget::LabelFrame public method configure { path args } {
    return [:widget_configure $path $args]
}


# ----------------------------------------------------------------------------
#  Command LabelFrame::cget
# ----------------------------------------------------------------------------
#+++ NXBwidget::LabelFrame public method cget { path option } {
#    return [Widget::cget $path $option]
#}


# ----------------------------------------------------------------------------
#  Command LabelFrame::align
#  This command align label of all widget given by args of class LabelFrame
#  (or "derived") by setting their width to the max one +1
# ----------------------------------------------------------------------------
NXBwidget::LabelFrame public method align { args } {
    set maxlen 0
    set wlist  {}
    foreach wl $args {
        foreach w $wl {
            upvar 0 NXBwidget::_class _class; #+++ added by me
            #+++
            if { ![info exists _class($w)] } {
                continue
            }
            #+++ set class $Widget::_class($w)
            set class _class($w); #+++
            if { [string equal $class "LabelFrame"] } {
                set textopt  -text
                set widthopt -width
            } else {
                #+++ upvar 0 Widget::${class}::map classmap
                upvar 0 :${class}_map classmap; #+++ (siehe Widget::addmap)
                set textopt  ""
                set widthopt ""
                set notdone  2
                foreach {option lmap} [array get classmap] {
                    foreach {subpath subclass realopt} $lmap {
                        if { [string equal $subclass "LabelFrame"] } {
                            if { [string equal $realopt "-text"] } {
                                set textopt $option
                                incr notdone -1
                                break
                            }
                            if { [string equal $realopt "-width"] } {
                                set widthopt $option
                                incr notdone -1
                                break
                            }
                        }
                    }
                    if { !$notdone } {
                        break
                    }
                }
                if { $notdone } {
                    continue
                }
            }
            set len [string length [$w cget $textopt]]
            if { $len > $maxlen } {
                set maxlen $len
            }
            lappend wlist $w $widthopt
        }
    }
    incr maxlen
    foreach {w widthopt} $wlist {
        $w configure $widthopt $maxlen
    }
}


NXBwidget::LabelFrame public method _destroy { path } {
    :destroy $path; #+++
}


#+++ (added by me)
NXBwidget::LabelFrame public method storePath {path} {
    set :wpath $path
}


#+++ (added by me)
NXBwidget::LabelFrame public method retrievePath {} {
    return ${:wpath}
}
