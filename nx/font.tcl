# ----------------------------------------------------------------------------
#  font.tcl
#  This file is part of Unifix BWidget Toolkit
# ----------------------------------------------------------------------------
#  Index of commands:
#     - SelectFont::create
#     - SelectFont::configure
#     - SelectFont::cget
#     - SelectFont::_draw
#     - SelectFont::_destroy
#     - SelectFont::_modstyle
#     - SelectFont::_update
#     - SelectFont::_getfont
#     - SelectFont::_init
# ----------------------------------------------------------------------------
package provide selectfont 1.0

package require nx
package require widget
package require dialog
package require labelframe
package require scrolledwindow



namespace eval NXBwidget {

nx::Class create SelectFont -superclass NXBwidget::Widget {
    
    #+++
    :public method initVars args {
	#initialize the classvariables of the superclasses + mixinclasses and the ones of self
	next
	
	variable _families
	variable _styleOff
	array set _styleOff [list bold normal italic roman]
	variable _sizes     {4 5 6 7 8 9 10 11 12 13 14 15 16 \
	    17 18 19 20 21 22 23 24}
	
	# Set up preset lists of fonts, so the user can avoid the painfully slow
	# loadfont process if desired.
	if { [string equal $::tcl_platform(platform) "windows"] } {
	    puts "\n-->SelectFont::initVars\n  \
	         platform:windows"
	    set presetVariable [list	\
		7x14			\
		Arial			\
		{Arial Narrow}		\
		{Lucida Sans}		\
		{MS Sans Serif}		\
		{MS Serif}		\
		{Times New Roman}	\
		]
	    set presetFixed    [list	\
		6x13			\
		{Courier New}		\
		FixedSys		\
		Terminal		\
		]
	    set presetAll      [list	\
		6x13			\
		7x14			\
		Arial			\
		{Arial Narrow}		\
		{Courier New}		\
		FixedSys		\
		{Lucida Sans}		\
		{MS Sans Serif}		\
		{MS Serif}		\
		Terminal		\
		{Times New Roman}	\
		]
	} else {
	    puts "\n-->SelectFont::initVars\n  \
	         platform:unix"
	    set presetVariable [list	\
		helvetica		\
		lucida			\
		lucidabright		\
		{times new roman}	\
		]
	    set presetFixed    [list	\
		courier			\
		fixed			\
		{lucida typewriter}	\
		screen			\
		serif			\
		terminal		\
		]
	    set presetAll      [list	\
		courier			\
		fixed			\
		helvetica		\
		lucida			\
		lucidabright		\
		{lucida typewriter}	\
		screen			\
		serif			\
		terminal		\
		{times new roman}	\
		]
	}
	array set _families [list \
	    presetvariable	$presetVariable	\
	    presetfixed		$presetFixed	\
	    presetall		$presetAll	\
	    ]
	
	puts "\n  _families: [array get _families]\n"
	
	variable _widget
	
	array set ::NXBwidget::SelectFont_[[self] info name]_path {}; #+++ added by me
	
	set :_inst_Dialog [NXBwidget::Dialog new]
	set :_inst_LabelFrame [NXBwidget::LabelFrame new]
	set :_inst_ScrolledWindow [NXBwidget::ScrolledWindow new]
    }
    
    
    #+++
    :public method init {} {
    	:initVars;  #+++
	
	#+++
	:define SelectFont font Dialog LabelFrame ScrolledWindow
	
	:declare SelectFont {
	    {-title		String		"Font selection" 0}
	    {-parent	String		"" 0}
	    {-background	TkResource	"" 0 frame}

	    {-type		Enum		dialog        0 {dialog toolbar}}
	    {-font		TkResource	""            0 label}
	    {-initialcolor	String		""            0}
	    {-families	String		"all"         1}
	    {-querysystem	Boolean		1             0}
	    {-nosizes	Boolean		0             1}
	    {-styles	String		"bold italic underline overstrike" 1}
	    {-command	String		""            0}
	    {-sampletext	String		"Sample Text" 0}
	    {-bg		Synonym		-background}
	}
	#+++
    }

}

}




# ----------------------------------------------------------------------------
#  Command SelectFont::create
# ----------------------------------------------------------------------------
NXBwidget::SelectFont public method create { path args } {
    :storePath $path; #+++
    
    variable _families
    variable _sizes
    #variable $path
    upvar 0  ::NXBwidget::SelectFont_[[self] info name]_path  data

    # Initialize the internal rep of the widget options
    :widget_init SelectFont "$path#SelectFont" $args; #+++

    #+++
    if { [:getoption "$path#SelectFont" -querysystem] } {
        :loadfont [:getoption "$path#SelectFont" -families]
    }

    #+++
    set bg [:getoption "$path#SelectFont" -background]
    set _styles [:getoption "$path#SelectFont" -styles]
    if { [:getoption "$path#SelectFont" -type] == "dialog" } {
        ${:_inst_Dialog} create $path -modal local -anchor e -default 0 -cancel 1 \
	    -background $bg \
            -title  [:getoption "$path#SelectFont" -title] \
            -parent [:getoption "$path#SelectFont" -parent]

        set frame [${:_inst_Dialog} getframe $path]
        set topf  [frame $frame.topf -relief flat -borderwidth 0 -background $bg]

        set labf1 [${:_inst_LabelFrame} create $topf.labf1  -text "Font" -name font \
                       -side top -anchor w -relief flat -background $bg]
        set sw    [${:_inst_ScrolledWindow} create [${:_inst_LabelFrame} getframe $labf1].sw \
                       -background $bg]
        set lbf   [listbox $sw.lb \
                       -height 5 -width 25 -exportselection false -selectmode browse]
	
	#+++
        ${:_inst_ScrolledWindow} setwidget $sw $lbf
	
        ${:_inst_LabelFrame} configure $labf1 -focus $lbf; #+++
	
	if { [:getoption "$path#SelectFont" -querysystem] } {
	    set fam [:getoption "$path#SelectFont" -families]
	} else {
	    set fam "preset"
	    append fam [:getoption "$path#SelectFont" -families]
	}
        eval [list $lbf insert end] $_families($fam)


        #+++ set script "set [list SelectFont::${path}(family)] \[%W curselection\];\
	#	        SelectFont::_update [list $path]"

	set scmd "set ::NXBwidget::SelectFont_[[self] info name]_path(family) \[%W curselection\]"
	set script "[self] eval $scmd;\
		        [self] _update [list $path]"
	#+++


        bind $lbf <ButtonRelease-1> $script
        bind $lbf <space>           $script
	bind $lbf <1>               [list focus %W]
	bind $lbf <Up> $script
	bind $lbf <Down> $script
        pack $sw -fill both -expand yes

        set labf2 [${:_inst_LabelFrame} create $topf.labf2 -text "Size" -name size \
                       -side top -anchor w -relief flat -background $bg]
	
        set sw    [${:_inst_ScrolledWindow} create [${:_inst_LabelFrame} getframe $labf2].sw \
                       -scrollbar vertical -background $bg]
        set lbs   [listbox $sw.lb \
                       -height 5 -width 6 -exportselection false -selectmode browse]
	
	
        ${:_inst_ScrolledWindow} setwidget $sw $lbs
        ${:_inst_LabelFrame} configure $labf2 -focus $lbs
        eval [list $lbs insert end] $_sizes
	
	#+++
        #set script "set [list SelectFont::${path}(size)] \[%W curselection\];\
	#		SelectFont::_update [list $path]"
	
	set scmd "set ::NXBwidget::SelectFont_[[self] info name]_path(size) \[%W curselection\]"
	set script "[self] eval $scmd;\
			[self] _update [list $path]"
	#+++
	
        bind $lbs <ButtonRelease-1> $script
        bind $lbs <space>           $script
	bind $lbs <1>               [list focus %W]
	bind $lbs <Up> $script
	bind $lbs <Down> $script
        pack $sw -fill both -expand yes

	#+++
        set labf3 [${:_inst_LabelFrame} create $topf.labf3 -text "Style" -name style \
                       -side top -anchor w -relief sunken -bd 1 -background $bg]
        set subf  [${:_inst_LabelFrame} getframe $labf3]
	#+++
	
        foreach st $_styles {
            set name [lindex [:getname $st] 0]; #+++
            if { $name == "" } {
                set name [string toupper $name 0]
            }

	    #+++
            checkbutton $subf.$st -text $name \
                -variable   ::NXBwidget::SelectFont_[[self] info name]_path\($st\) \
                -background $bg \
                -command    [list [self] _update $path]
	    #+++
            bind $subf.$st <Return> break
            pack $subf.$st -anchor w
        }
        ${:_inst_LabelFrame} configure $labf3 -focus $subf.[lindex $_styles 0]; #+++

        pack $labf1 -side left -anchor n -fill both -expand yes
	#+++
	if { ![:getoption "$path#SelectFont" -nosizes] } {
	        pack $labf2 -side left -anchor n -fill both -expand yes -padx 8
	}
        pack $labf3 -side left -anchor n -fill both -expand yes

        set botf [frame $frame.botf -width 100 -height 50 \
                      -bg white -bd 0 -relief flat \
                      -highlightthickness 1 -takefocus 0 \
                      -highlightbackground black \
                      -highlightcolor black]

        set lab  [label $botf.label \
                      -background white -foreground black \
                      -borderwidth 0 -takefocus 0 -highlightthickness 0 \
                      -text [:getoption "$path#SelectFont" -sampletext]]
        place $lab -relx 0.5 -rely 0.5 -anchor c

	pack $topf -pady 4 -fill both -expand yes

	#+++
	if { [:getoption "$path#SelectFont" -initialcolor] != ""} {
		set thecolor [:getoption "$path#SelectFont" -initialcolor]
		set colf [frame $frame.colf]
			
		set frc [frame $colf.frame -width 50 -height 20 -bg $thecolor -bd 0 -relief flat\
			-highlightthickness 1 -takefocus 0 \
			-highlightbackground black \
			-highlightcolor black]
			
		#+++
		#set script "set [list SelectFont::${path}(fontcolor)] \[tk_chooseColor -parent $colf.button -initialcolor \[set [list SelectFont::${path}(fontcolor)]\]\];\
		#	SelectFont::_update [list $path]"
			
		set script "set [list ::NXBwidget::SelectFont_[[self] info name]_path(fontcolor)] \[tk_chooseColor -parent $colf.button -initialcolor \[set [list ::NXBwidget::SelectFont_[[self] info name]_path(fontcolor)]\]\];\
			[self] _update [list $path]"
		#+++
		
		set name [lindex [:getname colorPicker] 0]; #+++
		if { $name == "" } {
			set name "Color..."
		}
		set but  [button $colf.button -command $script \
			-text $name]
		
		$lab configure -foreground $thecolor
		$frc configure -bg $thecolor
		
		pack $but -side left
		pack $frc -side left -padx 5
		
		set data(frc) $frc
		set data(fontcolor) $thecolor

		pack $colf -pady 4 -fill x -expand true        
	
	} else {
		set data(fontcolor) -1
	}
	pack $botf -pady 4 -fill x

	#+++
        ${:_inst_Dialog} add $path -name ok
        ${:_inst_Dialog} add $path -name cancel
	#+++

        set data(label) $lab
        set data(lbf)   $lbf
        set data(lbs)   $lbs

        :_getfont $path; #+++

	#+++ Widget::create SelectFont $path 0
	next "SelectFont $path 0"; #+++

        return [:_draw $path]; #+++
    } else {
	if { [:getoption "$path#SelectFont" -querysystem] } {
	    set fams [:getoption "$path#SelectFont" -families]; #+++
	} else {
	    set fams "preset"
	    append fams [:getoption "$path#SelectFont" -families]; #+++
	}
	if {[:theme]} {
	    ttk::frame $path

	    #+++
	    set lbf [ttk::combobox $path.font \
			 -takefocus 0 -exportselection 0 \
			 -values   $_families($fams) \
			 -textvariable ::NXBwidget::SelectFont_[[self] info name]_path(family) \
			 -state readonly]

	    set lbs [ttk::combobox $path.size \
			 -takefocus 0 -exportselection 0 \
			 -width    4 \
			 -values   $_sizes \
			 -textvariable ::NXBwidget::SelectFont_[[self] info name]_path(size) \
			 -state readonly]
	    #+++

	    bind $lbf <<ComboboxSelected>> [list [self] _update $path]
	    bind $lbs <<ComboboxSelected>> [list [self] _update $path]
	} else {
	    frame $path -background $bg
	    
	    #+++
	    set cmbbox [NXBwidget::ComboBox new]
	    #+++
	    
	    #+++
	    set lbf [$cmbbox create $path.font \
			 -highlightthickness 0 -takefocus 0 -background $bg \
			 -values   $_families($fams) \
			 -textvariable ::NXBwidget::SelectFont_[[self] info name]_path\(family\) \
			 -editable 0 \
			 -modifycmd [list [self] _update $path]]
	    
	    set lbs [$cmbbox create $path.size \
			 -highlightthickness 0 -takefocus 0 -background $bg \
			 -width    4 \
			 -values   $_sizes \
			 -textvariable ::NXBwidget::SelectFont_[[self] info name]_path\(size\) \
			 -editable 0 \
			 -modifycmd [list [self] _update $path]]
	    #+++
	}
	bind $path <Destroy> [list [self] _destroy $path]; #+++
        pack $lbf -side left -anchor w
        pack $lbs -side left -anchor w -padx 4
	
	#+++
	set __bmp [NXBwidget::Bitmap new]
	$__bmp create ${:BWIDGET_LIBRARY}/images
	#+++
	
        foreach st $_styles {
	    if {${:_theme}} {
		#+++
		ttk::checkbutton $path.$st -takefocus 0 \
		    -style BWSlim.Toolbutton \
		    -image [$__bmp get $st] \
		    -variable ::NXBwidget::SelectFont_[[self] info name]_path($st) \
		    -command [list [self] _update $path]
		#+++
	    } else {
		button $path.$st \
		    -highlightthickness 0 -takefocus 0 -padx 0 -pady 0 \
		    -background $bg \
		    -image [$__bmp get $st] \
		    -command [list [self] _modstyle $path $st]
	    }
            pack $path.$st -side left -anchor w
        }
        set data(label) ""
        set data(lbf)   $lbf
        set data(lbs)   $lbs
        :_getfont $path; #+++

	return [next "SelectFont $path"]; #+++
    }

    return $path
}


# ----------------------------------------------------------------------------
#  Command SelectFont::configure
# ----------------------------------------------------------------------------
NXBwidget::SelectFont public method configure { path args } {
    set _styles [:getoption "$path#SelectFont" -styles]; #+++

    set res [:widget_configure "$path#SelectFont" $args]; #+++

    if { [:hasChanged "$path#SelectFont" -font font] } {
        :_getfont $path; #+++
    }
    #+++
    if { [:hasChanged "$path#SelectFont" -background bg] } {
        switch -- [:getoption "$path#SelectFont" -type] {
            dialog {
                ${:_inst_Dialog} configure $path -background $bg
                set topf [${:_inst_Dialog} getframe $path].topf
                $topf configure -background $bg
                foreach labf {labf1 labf2} {
                    ${:_inst_LabelFrame} configure $topf.$labf -background $bg
                    set subf [${:_inst_LabelFrame} getframe $topf.$labf]
                    ${:_inst_ScrolledWindow} configure $subf.sw -background $bg
                    $subf.sw.lb configure -background $bg
                }
                ${:_inst_LabelFrame} configure $topf.labf3 -background $bg
                set subf [${:_inst_LabelFrame} getframe $topf.labf3]
                foreach w [winfo children $subf] {
                    $w configure -background $bg
                }
            }
            toolbar {
                $path configure -background $bg
		
		#+++
		set cmbbox [NXBwidget::ComboBox new]
		
                $cmbbox configure $path.font -background $bg
                $cmbbox configure $path.size -background $bg
		#+++
                foreach st $_styles {
                    $path.$st configure -background $bg
                }
            }
        }
    }
    return $res
}


# ----------------------------------------------------------------------------
#  Command SelectFont::cget
# ----------------------------------------------------------------------------
#+++ NXBwidget::SelectFont public method cget { path option } {
#    return [next "\"$path#SelectFont\" $option"]; #+++
#}


# ----------------------------------------------------------------------------
#  Command SelectFont::loadfont
# ----------------------------------------------------------------------------
NXBwidget::SelectFont public method loadfont {{which all}} {
    variable _families
    
    puts "\n-->SelectFont::loadfont - BEGIN\n  \
         _families: [array get _families]\n"

    # initialize families
    if {![info exists _families(all)]} {
	set _families(all) [lsort -dictionary [font families]]
    }
    if {[regexp {fixed|variable} $which] \
	    && ![info exists _families($which)]} {
	# initialize families
	set _families(fixed) {}
	set _families(variable) {}
	foreach family $_families(all) {
	    if { [font metrics [list $family] -fixed] } {
		lappend _families(fixed) $family
	    } else {
		lappend _families(variable) $family
	    }
	}
    }
    
    puts "\n-->SelectFont::loadfont - END\n  \
         _families: [array get _families]\n"
    
    return
}


# ----------------------------------------------------------------------------
#  Command SelectFont::_draw
# ----------------------------------------------------------------------------
NXBwidget::SelectFont public method _draw { path } {
    #variable $path
    upvar 0  ::NXBwidget::SelectFont_[[self] info name]_path  data

    $data(lbf) selection clear 0 end
    $data(lbf) selection set $data(family)
    $data(lbf) activate $data(family)
    $data(lbf) see $data(family)
    $data(lbs) selection clear 0 end
    $data(lbs) selection set $data(size)
    $data(lbs) activate $data(size)
    $data(lbs) see $data(size)
    :_update $path; #+++

    #+++
    if { [${:_inst_Dialog} draw $path] == 0 } {
        set result [:getoption "$path#SelectFont" -font]
    	set color $data(fontcolor)
	
	if { $color == "" } {
		set color #000000
	}

    } else {
        set result ""
        if {$data(fontcolor) == -1} {
            set color -1
        } else {
            set color ""
        }
    }
    unset data
    :destroy "$path#SelectFont"; #+++
    destroy $path
    if { $color != -1 } {
    	return [list $result $color]
    } else {
    	return $result
    }
}


# ----------------------------------------------------------------------------
#  Command SelectFont::_modstyle
# ----------------------------------------------------------------------------
NXBwidget::SelectFont public method _modstyle { path style } {
    #variable $path
    upvar 0  ::NXBwidget::SelectFont_[[self] info name]_path  data

    $path.$style configure -relief [expr {$data($style) ? "raised" : "sunken"}]
    set data($style) [expr {!$data($style)}]
    :_update $path; #+++
}


# ----------------------------------------------------------------------------
#  Command SelectFont::_update
# ----------------------------------------------------------------------------
NXBwidget::SelectFont public method _update { path } {
    variable _families
    variable _sizes
    variable _styleOff
    #variable $path
    upvar 0  ::NXBwidget::SelectFont_[[self] info name]_path  data
    
    #+++
    set type [:getoption "$path#SelectFont" -type]
    set _styles [:getoption "$path#SelectFont" -styles]
    if { [:getoption "$path#SelectFont" -querysystem] } {
	set fams [:getoption "$path#SelectFont" -families]
    } else {
	set fams "preset"
	append fams [:getoption "$path#SelectFont" -families]; #+++
    }
    if { $type == "dialog" } {
        set curs [$path:cmd cget -cursor]
        $path:cmd configure -cursor watch
    }
    
    puts "\n-->SelectFont::_update\n  \
         path: $path\n  \
         _fams: $fams\n\n  \
	 _families: [array get _families]\n\n  \
         data: [array get data]\n"
    
    if { [:getoption "$path#SelectFont" -type] == "dialog" } {
        set font [list [lindex $_families($fams) $data(family)] \
		[lindex $_sizes $data(size)]]
	puts "\n-->SelectFont::_update\n  \
	     font: $font\n  \
	     index: $data(family)\n"
    } else {
        set font [list $data(family) $data(size)]
    }
    foreach st $_styles {
        if { $data($st) } {
            lappend font $st
        } elseif {[info exists _styleOff($st)]} {
	    # This adds the default bold/italic value to a font
	    #lappend font $_styleOff($st)
	}
    }
    #+++ Widget::setoption "$path#SelectFont" -font $font
    
    set :__selectedFont $font; #+++ added by me
    
    :widget_configure "$path#SelectFont"  [list -font $font]  no
    #+++
    if { $type == "dialog" } {
	puts "\n-->SelectFont::_update\n  \
	     SETTING Sample-Label (font: '$font')\n"
        $data(label) configure -font $font
        $path:cmd configure -cursor $curs
	if { ($data(fontcolor) != "") && ($data(fontcolor) != -1) } {
		$data(label) configure -foreground $data(fontcolor)
		$data(frc) configure -bg $data(fontcolor)
	} elseif { $data(fontcolor) == "" }  {
		#If no color is selected, restore previous one
		set data(fontcolor) [$data(label) cget -foreground]

	}
    } elseif { [set cmd [:getoption "$path#SelectFont" -command]] != "" } {
        uplevel \#0 $cmd
	#+++
    }
}


# ----------------------------------------------------------------------------
#  Command SelectFont::_getfont
# ----------------------------------------------------------------------------
NXBwidget::SelectFont public method _getfont { path } {
    variable _families
    variable _sizes
    #variable $path
    upvar 0  ::NXBwidget::SelectFont_[[self] info name]_path  data
    
    puts "\n-->SelectFont::getfont - BEGIN\n  \
         _families: [array get _families]\n"

    array set font [font actual [:getoption "$path#SelectFont" -font]]; #+++
    set data(bold)       [expr {![string equal $font(-weight) "normal"]}]
    set data(italic)     [expr {![string equal $font(-slant)  "roman"]}]
    set data(underline)  $font(-underline)
    set data(overstrike) $font(-overstrike)
    set _styles [:getoption "$path#SelectFont" -styles]; #+++
    if { [:getoption "$path#SelectFont" -querysystem] } {
	set fams [:getoption "$path#SelectFont" -families]
    } else {
	set fams "preset"
	append fams [:getoption "$path#SelectFont" -families]
    }
    #+++
    if { [:getoption "$path#SelectFont" -type] == "dialog" } {
        set idxf [lsearch $_families($fams) $font(-family)]
        set idxs [lsearch $_sizes    $font(-size)]
        set data(family) [expr {$idxf >= 0 ? $idxf : 0}]
        set data(size)   [expr {$idxs >= 0 ? $idxs : 0}]
    } else {
	set data(family) $font(-family)
	set data(size)   $font(-size)
	if {![:theme]} {
	    foreach st $_styles {
		$path.$st configure \
		    -relief [expr {$data($st) ? "sunken":"raised"}]
	    }
	}
    }
    
    puts "\n-->SelectFont::getfont - END\n  \
         _families: [array get _families]\n"
}


# ----------------------------------------------------------------------------
#  Command SelectFont::_destroy
# ----------------------------------------------------------------------------
NXBwidget::SelectFont public method _destroy { path } {
    #variable $path
    upvar 0  ::NXBwidget::SelectFont_[[self] info name]_path  data
    unset data
    :destroy "$path#SelectFont"; #+++
}


#+++ (added by me)
NXBwidget::SelectFont public method storePath {path} {
    set :wpath $path
}


#+++ (added by me)
NXBwidget::SelectFont public method retrievePath {} {
    return ${:wpath}
}
