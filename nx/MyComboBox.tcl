#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

lappend auto_path .
package provide mycombobox 1.0

package require nx
package require bwidget
package require combobox
package require labelentry



nx::Class create Demo {
    variable status
    variable prgindic
    variable var; #required by the ComboBox
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


Demo public method create { } {
    #Demo::createMainwindow
    
    #set frame    [${Demo::mainwin} getframe]
    
    set frame [frame .topf]
    pack $frame -pady 2 -fill x
    
    
    #create the ComboBox
    set combo [NXBwidget::ComboBox new]
    $combo create $frame.combo \
                   -textvariable ::var(combo,var) \
                   -values {"first value" "second value" "third value" "fourth value" "fifth value"} \
                   -helptext "This is the ComboBox"
    pack [$combo retrievePath] -pady 4 -fill x
    
    #create the Entry
    set ent [NXBwidget::LabelEntry new]
    $ent create $frame.ent -label "Selected \n item" -labelwidth 10 -labelanchor w \
                   -textvariable ::var(combo,var) -editable 0 \
                   -helptext "This is an Entry reflecting\nthe selected item of ComboBox"
    pack [$ent retrievePath] -pady 4 -fill x
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    
    update idletasks
}



Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "ComboBox demo"
    
    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

set dm [Demo new]
wm geom . [wm geom .]
