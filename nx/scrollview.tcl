# ------------------------------------------------------------------------------
#  scrollview.tcl
#  This file is part of Unifix BWidget Toolkit
#  $Id: scrollview.tcl,v 1.7 2003/11/05 18:04:29 hobbs Exp $
# ------------------------------------------------------------------------------
#  Index of commands:
#     - ScrolledWindow::create
#     - ScrolledWindow::configure
#     - ScrolledWindow::cget
#     - ScrolledWindow::_set_hscroll
#     - ScrolledWindow::_set_vscroll
#     - ScrolledWindow::_update_scroll
#     - ScrolledWindow::_set_view
#     - ScrolledWindow::_resize
# ------------------------------------------------------------------------------
package provide scrollview 1.0

package require nx
package require widget



namespace eval NXBwidget {

nx::Class create ScrollView -superclass NXBwidget::Widget {
    
    #+++
    :public method initVars args {
	#initialize the classvariables of the superclasses + mixinclasses and the ones of self
	next
    }
    
    
    #+++
    :public method init {} {
    	:initVars;  #+++
	
	:define ScrollView scrollview; #+++
	
	#+++
	:tkinclude ScrollView canvas :cmd \
    	    include {-relief -borderwidth -background -width -height -cursor} \
    	    initialize {-relief flat -borderwidth 0 -width 30 -height 30 \
    	    -cursor crosshair}
	
	#+++
	:declare ScrollView {
	    {-width       TkResource 30        0 canvas}
	    {-height      TkResource 30        0 canvas}
	    {-background  TkResource ""        0 canvas}
	    {-foreground  String     black     0}
	    {-fill        String     ""        0}
	    {-relief      TkResource flat      0 canvas}
	    {-borderwidth TkResource 0         0 canvas}
	    {-cursor      TkResource crosshair 0 canvas}
	    {-window      String     ""        0}
	    {-fg          Synonym    -foreground}
	    {-bg          Synonym    -background}
	    {-bd          Synonym    -borderwidth}
	}
	
	#+++
	bind BwScrollView <B1-Motion>   [list [self] _set_view %W motion %x %y]
	bind BwScrollView <ButtonPress-1> [list [self] _set_view %W set %x %y]
	bind BwScrollView <Configure>     [list [self] _resize %W]
	bind BwScrollView <Destroy>       [list [self] _destroy %W]
    }
    
    
    #+++ Widget::define ScrollView scrollview

    #+++ Widget::tkinclude ScrollView canvas :cmd \
    #	    include {-relief -borderwidth -background -width -height -cursor} \
    #	    initialize {-relief flat -borderwidth 0 -width 30 -height 30 \
    #		-cursor crosshair}

    #+++ Widget::declare ScrollView {
    #    {-width       TkResource 30        0 canvas}
    #    {-height      TkResource 30        0 canvas}
    #    {-background  TkResource ""        0 canvas}
    #    {-foreground  String     black     0}
    #    {-fill        String     ""        0}
    #    {-relief      TkResource flat      0 canvas}
    #    {-borderwidth TkResource 0         0 canvas}
    #    {-cursor      TkResource crosshair 0 canvas}
    #    {-window      String     ""        0}
    #    {-fg          Synonym    -foreground}
    #    {-bg          Synonym    -background}
    #    {-bd          Synonym    -borderwidth}
    #}

    #+++
    #bind BwScrollView <B1-Motion>   [list ScrollView::_set_view %W motion %x %y]
    #bind BwScrollView <ButtonPress-1> [list ScrollView::_set_view %W set %x %y]
    #bind BwScrollView <Configure>     [list ScrollView::_resize %W]
    #bind BwScrollView <Destroy>       [list ScrollView::_destroy %W]
}; #End - class

}



# ------------------------------------------------------------------------------
#  Command ScrollView::create
# ------------------------------------------------------------------------------
#+++ in parameter args the option -window must be an class-instance
NXBwidget::ScrollView public method create { path args } {
    :storePath $path; #+++
    
    puts "\n-->ScrollView::create\n  path: $path\n  args: $args\n"
    
    #+++ Widget::init ScrollView $path $args
    :widget_init ScrollView $path $args; #+++
    
    eval [list canvas $path] [:subcget $path :cmd] -highlightthickness 0; #+++

    #+++ Widget::create ScrollView $path
    next "ScrollView $path"; #+++

    #+++ Widget::getVariable $path _widget
    :getVariable $path _widget; #+++

    #+++
    #set w               [:cget $path -window]
    set _inst_w               [:cget $path -window]; #+++ added by me
    set w  		[$_inst_w retrievePath]; #+++ added by me (this is the path)
    set _widget(bd)     [:cget $path -borderwidth]
    set _widget(width)  [:cget $path -width]
    set _widget(height) [:cget $path -height]

    if {[winfo exists $w]} {
        set _widget(oldxscroll) [$_inst_w cget $w -xscrollcommand]; #+++ w changed to _inst_w
        set _widget(oldyscroll) [$_inst_w cget $w -yscrollcommand]; #+++ w changed to _inst_w
	
	#+++ below w changed to _inst_w
        $_inst_w configure $w \
            -xscrollcommand [list [self] _set_hscroll $path] \
            -yscrollcommand [list [self] _set_vscroll $path]; #+++
    }
    #+++
    $path:cmd create rectangle -2 -2 -2 -2 \
        -fill    [:cget $path -fill]       \
        -outline [:cget $path -foreground] \
        -tags    view

    bindtags $path [list $path BwScrollView [winfo toplevel $path] all]

    return $path
}


# ------------------------------------------------------------------------------
#  Command ScrollView::configure
# ------------------------------------------------------------------------------
NXBwidget::ScrollView public method configure { path args } {
    :getVariable $path _widget; #+++

    set oldw [:getoption $path -window]; #+++
    set res  [:widget_configure $path $args]; #+++

    #+++
    if { [:hasChanged $path -window w] } {
        if { [winfo exists $oldw] } {
            $oldw configure \
                -xscrollcommand $_widget(oldxscroll) \
                -yscrollcommand $_widget(oldyscroll)
        }
        if { [winfo exists $w] } {
            set _widget(oldxscroll) [$w cget -xscrollcommand]
            set _widget(oldyscroll) [$w cget -yscrollcommand]
            $w configure \
                -xscrollcommand [list [self] _set_hscroll $path] \
                -yscrollcommand [list [self] _set_vscroll $path]; #+++
        } else {
            $path:cmd coords view -2 -2 -2 -2
            set _widget(oldxscroll) {}
            set _widget(oldyscroll) {}
        }
    }

    #+++
    if { [:hasChanged $path -fill fill] |
         [:hasChanged $path -foreground fg] } {
        $path:cmd itemconfigure view \
            -fill    $fill \
            -outline $fg
    }

    return $res
}


# ------------------------------------------------------------------------------
#  Command ScrollView::cget
# ------------------------------------------------------------------------------
#+++ NXBwidget::ScrollView public method cget { path option } {
#    return [Widget::cget $path $option]
#}


# ------------------------------------------------------------------------------
#  Command ScrollView::_set_hscroll
# ------------------------------------------------------------------------------
NXBwidget::ScrollView public method _set_hscroll { path vmin vmax } {
    :getVariable $path _widget; #+++

    set c  [$path:cmd coords view]
    set x0 [expr {$vmin*$_widget(width)+$_widget(bd)}]
    set x1 [expr {$vmax*$_widget(width)+$_widget(bd)-1}]
    $path:cmd coords view $x0 [lindex $c 1] $x1 [lindex $c 3]
    if { $_widget(oldxscroll) != "" } {
        uplevel \#0 $_widget(oldxscroll) $vmin $vmax
    }
}


# ------------------------------------------------------------------------------
#  Command ScrollView::_set_vscroll
# ------------------------------------------------------------------------------
NXBwidget::ScrollView public method _set_vscroll { path vmin vmax } {
    :getVariable $path _widget; #+++

    set c  [$path:cmd coords view]
    set y0 [expr {$vmin*$_widget(height)+$_widget(bd)}]
    set y1 [expr {$vmax*$_widget(height)+$_widget(bd)-1}]
    $path:cmd coords view [lindex $c 0] $y0 [lindex $c 2] $y1
    if { $_widget(oldyscroll) != "" } {
        uplevel \#0 $_widget(oldyscroll) $vmin $vmax
    }
}


# ------------------------------------------------------------------------------
#  Command ScrollView::_update_scroll
# ------------------------------------------------------------------------------
NXBwidget::ScrollView public method _update_scroll { path callscroll hminmax vminmax } {
    :getVariable $path _widget; #+++

    set c    [$path:cmd coords view]
    set hmin [lindex $hminmax 0]
    set hmax [lindex $hminmax 1]
    set vmin [lindex $vminmax 0]
    set vmax [lindex $vminmax 1]
    set x0   [expr {$hmin*$_widget(width)+$_widget(bd)}]
    set x1   [expr {$hmax*$_widget(width)+$_widget(bd)-1}]
    set y0   [expr {$vmin*$_widget(height)+$_widget(bd)}]
    set y1   [expr {$vmax*$_widget(height)+$_widget(bd)-1}]
    $path:cmd coords view $x0 $y0 $x1 $y1
    if { $callscroll } {
        if { $_widget(oldxscroll) != "" } {
            uplevel \#0 $_widget(oldxscroll) $hmin $hmax
        }
        if { $_widget(oldyscroll) != "" } {
            uplevel \#0 $_widget(oldyscroll) $vmin $vmax
        }
    }
}


# ------------------------------------------------------------------------------
#  Command ScrollView::_set_view
# ------------------------------------------------------------------------------
NXBwidget::ScrollView public method _set_view { path cmd x y } {
    :getVariable $path _widget; #+++

    set __w [:getoption $path -window]; #+++ (-window contains NOW a classinstance NOT a path)
    
    set w [$__w retrievePath]; #+++ added by me (this is the path)

    puts "\n-->ScrollView::_set_view\n  __w: $__w\n  w: $w\n  path: $path\n"
    
    if {[winfo exists $w]} {
        if {[string equal $cmd "set"]} {
            set c  [$path:cmd coords view]
            set x0 [lindex $c 0]
            set y0 [lindex $c 1]
            set x1 [lindex $c 2]
            set y1 [lindex $c 3]
            if {$x >= $x0 && $x <= $x1 &&
                $y >= $y0 && $y <= $y1} {
                set _widget(dx) [expr {$x-$x0}]
                set _widget(dy) [expr {$y-$y0}]
                return
            } else {
                set x0 [expr {$x-($x1-$x0)/2}]
                set y0 [expr {$y-($y1-$y0)/2}]
                set _widget(dx) [expr {$x-$x0}]
                set _widget(dy) [expr {$y-$y0}]
                set vh [expr {double($x0-$_widget(bd))/$_widget(width)}]
                set vv [expr {double($y0-$_widget(bd))/$_widget(height)}]
            }
        } elseif {[string equal $cmd "motion"]} {
            set vh [expr {double($x-$_widget(dx)-$_widget(bd))/$_widget(width)}]
            set vv [expr {double($y-$_widget(dy)-$_widget(bd))/$_widget(height)}]
        }
	#+++
        #$w xview moveto $vh
        #$w yview moveto $vv
	
	$__w xview $w moveto $vh; #+++
        $__w yview $w moveto $vv; #+++
	
	#+++
        #:_update_scroll $path 1 [$w xview] [$w yview]; #+++
	
	:_update_scroll $path 1 [$__w xview $w] [$__w yview $w]; #+++
	#+++
    }
}


# ------------------------------------------------------------------------------
#  Command ScrollView::_resize
# ------------------------------------------------------------------------------
NXBwidget::ScrollView public method _resize { path } {
    :getVariable $path _widget; #+++

    set _widget(bd)     [:getoption $path -borderwidth]; #+++
    set _widget(width)  [expr {[winfo width  $path]-2*$_widget(bd)}]
    set _widget(height) [expr {[winfo height $path]-2*$_widget(bd)}]
    set w [:getoption $path -window]; #+++
    if { [winfo exists $w] } {
        :_update_scroll $path 0 [$w xview] [$w yview]; #+++
    }
}


# ------------------------------------------------------------------------------
#  Command ScrollView::_destroy
# ------------------------------------------------------------------------------
NXBwidget::ScrollView public method _destroy { path } {
    :getVariable $path _widget; #+++

    set w [:getoption $path -window]; #+++
    if { [winfo exists $w] } {
        $w configure \
            -xscrollcommand $_widget(oldxscroll) \
            -yscrollcommand $_widget(oldyscroll)
    }
    :destroy $path; #+++
}


#+++ (added by me)
NXBwidget::ScrollView public method storePath {path} {
    set :wpath $path
}


#+++ (added by me)
NXBwidget::ScrollView public method retrievePath {} {
    return ${:wpath}
}
