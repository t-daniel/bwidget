#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide mytitleframe_mainframe 1.0

package require nx
package require bwidget
package require mainframe



nx::Class create Demo {
    #set :status ""
    #set :prgindic ""
    set :mainwin ""
    
    set :pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    puts "\nNEW MainFrame\n"
    set :mainwin [NXBwidget::MainFrame new]
    
    puts "\nCREATE MainFrame\n"
    ${:mainwin} create .mainframe \
                       -menu         $descmenu \
                       -textvariable DemoGlobal::status \
                       -progressvar  DemoGlobal::prgindic
}


Demo public method create { } {
    :createMainwindow
    
    
    #set frame    [${:mainwin} getframe]
    
    
    #create the TitleFrame
    #set topf  [frame $frame.topf]
    #set titf1 [TitleFrame $topf.titf1 -text "My Titleframe"]
    #pack the Titleframe
    #pack $titf1 -side left -fill both -padx 4 -expand yes
    #pack $topf -pady 2 -fill x
    
    
    #pack the mainframe
    pack [${:mainwin} retrievePath] -fill both -expand yes
    update idletasks
}


Demo public method init args {
    variable DEMODIR
    puts "\nIN MAIN"

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    puts "  DEMODIR: $DEMODIR"

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "MainFrame demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    ${:mainwin} place . 300 300 right
    
    wm deiconify .
    raise .
    focus -force .
}

namespace eval DemoGlobal {
    set :status ""
    set :prgindic ""
}

set demo [Demo new]

#Demo::main;  abgeloest durch Demo::init
wm geom . [wm geom .]

