# ------------------------------------------------------------------------------
#  pagesmgr.tcl
#  This file is part of Unifix BWidget Toolkit
#  $Id: pagesmgr.tcl,v 1.6.2.1 2011/02/14 16:56:09 oehhar Exp $
# ------------------------------------------------------------------------------
#  Index of commands:
#     - PagesManager::create
#     - PagesManager::configure
#     - PagesManager::cget
#     - PagesManager::compute_size
#     - PagesManager::add
#     - PagesManager::delete
#     - PagesManager::raise
#     - PagesManager::page
#     - PagesManager::pages
#     - PagesManager::getframe
#     - PagesManager::_test_page
#     - PagesManager::_select
#     - PagesManager::_redraw
#     - PagesManager::_draw_area
#     - PagesManager::_realize
# ------------------------------------------------------------------------------
package provide pagesmanager 1.0

package require nx
package require widget
package require Tcl 8.1.1



namespace eval NXBwidget {

nx::Class create PagesManager -superclass NXBwidget::Widget {
    #+++ Widget::define PagesManager pagesmgr

    #+++ Widget::declare PagesManager {
    #    {-background TkResource "" 0 frame}
    #    {-cursor     TkResource "" 0 frame}
    #    {-width      Int        0  0 "%d >= 0"}
    #    {-height     Int        0  0 "%d >= 0"}
    #}

    #+++ Widget::addmap PagesManager "" :cmd { -width {} -height {} -cursor {} }
    
    
    #+++
    :public method initVars args {
	next
	
	array set :pmpath {}; #+++
    }
    
    
    #+++
    :public method init {} {
	:initVars; #+++
	
	:define PagesManager pagesmgr; #+++
	
	#+++
	:declare PagesManager {
	    {-background TkResource "" 0 frame}
	    {-cursor     TkResource "" 0 frame}
	    {-width      Int        0  0 "%d >= 0"}
	    {-height     Int        0  0 "%d >= 0"}
	}
	
	#+++
	:addmap PagesManager "" :cmd { -width {} -height {} -cursor {} }
    }
}

}



# ------------------------------------------------------------------------------
#  Command PagesManager::create
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method create { path args } {
    :storePath $path; #+++
    
    #+++ variable $path;  $path is a global variable in the original code
    upvar 0 :pmpath data; #+++
    
    #+++ upvar 0  $path data

    #+++ Widget::init PagesManager $path $args
    
    #+++
    :widget_init PagesManager $path $args

    set data(select)   ""
    set data(pages)    {}
    set data(cpt)      0
    set data(realized) 0

    # --- creation du canvas -----------------------------------------------------------------
    eval canvas $path -relief flat -bd 0 -highlightthickness 0 \
	    [:subcget $path :cmd]; #+++

    #+++
    #bind $path <Configure> [list PagesManager::_realize $path]
    #bind $path <Destroy>   [list PagesManager::_destroy $path]
    
    bind $path <Configure> [list [self] _realize $path]; #+++
    bind $path <Destroy>   [list [self] _destroy $path]; #+++
    #+++

    return [next "PagesManager $path"]; #+++
}


# ------------------------------------------------------------------------------
#  Command PagesManager::configure
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method configure { path args } {
    return [:widget_configure $path $args]
}


# ------------------------------------------------------------------------------
#  Command PagesManager::cget
# ------------------------------------------------------------------------------
#+++ proc PagesManager::cget { path option } {
#    return [Widget::cget $path $option]
#}


# ------------------------------------------------------------------------------
#  Command PagesManager::compute_size
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method compute_size { path } {
    #+++ variable $path
    upvar 0 :pmpath data; #+++
    
    #+++ upvar 0  $path data

    set wmax 0
    set hmax 0
    update idletasks
    foreach page $data(pages) {
        set w    [winfo reqwidth  $path.f$page]
        set h    [winfo reqheight $path.f$page]
        set wmax [expr {$w>$wmax ? $w : $wmax}]
        set hmax [expr {$h>$hmax ? $h : $hmax}]
    }
    #+++ configure $path -width $wmax -height $hmax
    
    :configure $path -width $wmax -height $hmax; #+++
}


# ------------------------------------------------------------------------------
#  Command PagesManager::add
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method add { path page } {
    #+++ variable $path
    upvar 0 :pmpath data; #+++
    
    #+++ upvar 0  $path data

    if { [lsearch -exact $data(pages) $page] != -1 } {
        return -code error "page \"$page\" already exists"
    }

    lappend data(pages) $page

    #+++
    if {${:_theme}} {
        ttk::frame $path.f$page
    }  else  {
        frame $path.f$page -relief flat \
            -background [:cget $path -background] -borderwidth 0; #+++
    }

    puts "\n-->PagesManager::add\n  RET: $path.f$page\n  data(): [array get :pmpath]\n"
    return $path.f$page
}


# ------------------------------------------------------------------------------
#  Command PagesManager::delete
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method delete { path page } {
    #+++ variable $path
    upvar 0 :pmpath data; #+++
    
    #+++ upvar 0  $path data

    set pos [:_test_page $path $page]; #+++
    set data(pages) [lreplace $data(pages) $pos $pos]
    if { $data(select) == $page } {
        set data(select) ""
    }
    :destroy $path.f$page; #+++
    :_redraw $path; #+++
}


# ------------------------------------------------------------------------------
#  Command PagesManager::raise
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method raise { path {page ""} } {
    #+++ variable $path
    upvar 0 :pmpath data; #+++
    
    #+++ upvar 0  $path data

    if { $page != "" } {
        :_test_page $path $page; #+++
        :_select $path $page; #+++
    }
    return $data(select)
}


# ------------------------------------------------------------------------------
#  Command PagesManager::page - deprecated, use pages
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method page { path first {last ""} } {
    #+++ variable $path
    upvar 0 :pmpath data; #+++
    
    #+++ upvar 0  $path data

    if { $last == "" } {
        return [lindex $data(pages) $first]
    } else {
        return [lrange $data(pages) $first $last]
    }
}


# ------------------------------------------------------------------------------
#  Command PagesManager::pages
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method pages { path {first ""} {last ""} } {
    #+++ variable $path
    upvar 0 :pmpath data; #+++
    
    #+++ upvar 0  $path data

    if { ![string length $first] } {
	return $data(pages)
    }

    if { ![string length $last] } {
        return [lindex $data(pages) $first]
    } else {
        return [lrange $data(pages) $first $last]
    }
}


# ------------------------------------------------------------------------------
#  Command PagesManager::getframe
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method getframe { path page } {
    return $path.f$page
}


# ------------------------------------------------------------------------------
#  Command PagesManager::_test_page
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method _test_page { path page } {
    #+++ variable $path
    upvar 0 :pmpath data; #+++
    
    #+++ upvar 0  $path data
    
    puts "\n-->PagesManager::_test_page:\n  data: [array get data]\n"

    if { [set pos [lsearch $data(pages) $page]] == -1 } {
        return -code error "page \"$page\" does not exists"
    }
    return $pos
}


# ------------------------------------------------------------------------------
#  Command PagesManager::_select
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method _select { path page } {
    #+++ variable $path
    upvar 0 :pmpath data; #+++
    
    #+++ upvar 0  $path data

    set oldsel $data(select)
    if { $page != $oldsel } {
        set data(select) $page
        :_draw_area $path; #+++
    }
}


# ------------------------------------------------------------------------------
#  Command PagesManager::_redraw
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method _redraw { path } {
    #+++ variable $path
    upvar 0 :pmpath data; #+++
    
    #+++ upvar 0  $path data

    if { !$data(realized) } {
        return
    }
    :_draw_area $path; #+++
}


# ------------------------------------------------------------------------------
#  Command PagesManager::_draw_area
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method _draw_area { path } {
    #+++ variable $path
    upvar 0 :pmpath data; #+++
    
    #+++ upvar 0  $path data

    set w   [winfo width  $path]
    set h   [winfo height $path]
    set sel $data(select)
    if { $sel != "" } {
        if { [llength [$path:cmd find withtag "window"]] } {
            $path:cmd coords "window" 0 0
            $path:cmd itemconfigure "window"    \
                -width  $w        \
                -height $h    \
                -window $path.f$sel
        } else {
            $path:cmd create window 0 0 \
                -width  $w          \
                -height $h       \
                -anchor nw                      \
                -tags   "window"                \
                -window $path.f$sel
        }
    } else {
        $path:cmd delete "window"
    }
}


# ------------------------------------------------------------------------------
#  Command PagesManager::_realize
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method _realize { path } {
    #+++ variable $path
    upvar 0 :pmpath data; #+++
    
    #+++ upvar 0  $path data

    if { [set width  [:cget $path -width]]  == 0 ||
         [set height [:cget $path -height]] == 0 } {
        :compute_size $path; #+++
    }

    set data(realized) 1
    :_draw_area $path; #+++
    
    bind $path <Configure> [list [self] _draw_area $path]; #+++
}


# ------------------------------------------------------------------------------
#  Command PagesManager::_destroy
# ------------------------------------------------------------------------------
NXBwidget::PagesManager public method _destroy { path } {
    #+++ variable $path
    upvar 0 :pmpath data; #+++
    
    #+++upvar 0  $path data
    
    :destroy $path; #+++
    unset data
}


#+++ (added by me)
NXBwidget::PagesManager public method storePath {path} {
    set :wpath $path
}


#+++ (added by me)
NXBwidget::PagesManager public method retrievePath {} {
    return ${:wpath}
}
