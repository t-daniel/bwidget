#Item für ListBox.

package provide item 1.0

package require nx
package require widget

namespace eval NXBwidget {

nx::Class create Item -superclass NXBwidget::Widget {

    #:public method initVars args {
    #    next
    #}

    :public method item_init {} {
    #:initVars
    :define Item item
    :declare Item {
        {-indent     Int        0   0 "%d >= 0"}
        {-text       String     ""  0}
        {-font       String     ""  0}
        {-foreground String     ""  0}
        {-image      TkResource ""  0 label}
        {-window     String     ""  0}
        {-data       String     ""  0}

        {-fill       Synonym    -foreground}
        {-fg         Synonym    -foreground}
    }
    }

    #:public method create { path args } {
    #:widget_init Item $path $args; #+++
#
#    next [list Item $path 0]; #+++
#
    #return $path
    #}
}
    

}
