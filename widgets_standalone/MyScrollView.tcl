#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}



namespace eval Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


proc Demo::createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


proc Demo::create { } {
    Demo::createMainwindow
    
    
    set frame    [${Demo::mainwin} getframe]
    
    
    
    #create a Label
    set lab [label $frame.lab1 -text "Drag the white rectangle \n to scroll the list on the right side"]
    pack $lab -pady 2 -fill x
    
    
    
    #create the PanedWindow
    #create the left side
    set pw    [PanedWindow $frame.panedwin -side top]
    set pleft  [$pw add -weight 1]
    
    
    #create the right side
    set pright [$pw add -weight 2]
    #Create the ScrolledWindow. ScrollableFrame should be used, to make e.g. a scrollable list.
    #ScrollView will interact with ScrollableFrame.
    set sw [ScrolledWindow $pright.scrwin -relief sunken -borderwidth 2]
    set sf [ScrollableFrame $sw.scrframe]
    $sw setwidget $sf
    set subf [$sf getframe]
    
    for {set i 0} {$i <= 15} {incr i} {
      #crates Buttons as list items
      set but   [Button $subf.but_${i} -text "Button ${i}" \
                   -repeatdelay 300 \
                   -command  "Demo::buttoncmd $i" \
                   -helptext "This is a Button widget"]
      pack $but  -side top -padx 4 -pady 2
    }
    pack $sw  -side top  -expand yes -fill both
    
    pack $pw  -fill both  -expand yes
    
    
    
    #create the ScrollView
    Demo::createScrollView $pleft $sf
    
    
    
    #pack the mainframe
    pack ${Demo::mainwin} -fill both -expand yes
    update idletasks
}


proc Demo::buttoncmd {args} {
    puts "Button ${args} pressed!"
}


proc Demo::createScrollView {container  wnd} {
    #set w .top
    #toplevel $w
    #wm withdraw $w
    #wm protocol $w WM_DELETE_WINDOW {
    #    # don't kill me
    #}
    #wm resizable $w 0 0
    #wm title $w "Drag rectangle to scroll directory tree"
    #wm transient $w .
    
    
    ScrollView $container.scrview -window $wnd -fill white -relief sunken -bd 1 \
	    -width 300 -height 300
    pack $container.scrview -fill both -expand yes
}



proc Demo::main {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "ScrollView demo"

    Demo::create
    BWidget::place . 300 300 center;  #center  changed to  right (the position of the demo application); width=300  height=300
    wm deiconify .
    raise .
    focus -force .
}

Demo::main
wm geom . [wm geom .]
