#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide mydragdrop 1.0

package require nx
package require widget
package require mainframe
package require label
package require labelframe
package require labelentry
package require titleframe



nx::Class create Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set :mainwin [NXBwidget::MainFrame new]
    ${:mainwin} create .mainframe \
                       -menu         $descmenu \
                       -textvariable ::status \
                       -progressvar  ::prgindic
}


Demo public method create { } {
    :createMainwindow
    
    set frame    [${:mainwin} getframe [${:mainwin} retrievePath]]
    
    
    #create the Drag-source & Drop-target (using options: -dragenabled,  -dropenabled)
    set titf1 [NXBwidget::TitleFrame new]
    $titf1 create  $frame.titf1 -text "Drag sources"
    set subf  [$titf1 getframe [$titf1 retrievePath]]

    set ent1  [NXBwidget::LabelEntry new]
    $ent1 create  $subf.e1 -label "DragEntry" -labelwidth 14 -dragenabled 1 -dragevent 3
    
    set labf1 [NXBwidget::LabelFrame new]
    $labf1 create  $subf.f1 -text "Label (text)" -width 14
    set f     [$labf1 getframe [$labf1 retrievePath]]
    
    set lab   [NXBwidget::Label new]
    $lab create  $f.l -text "Drag this text or the text in the inputfiled DragEntry \n (with the 3rd mouse button and \n move over the inputfield DropEntry)" -dragenabled 1 -dragevent 3
    pack [$lab retrievePath]

    set labf2 [NXBwidget::LabelFrame new]
    $labf2 create  $subf.f2 -text "Label (bitmap)" -width 14
    set f     [$labf2 getframe [$labf2 retrievePath]]
    
    set lab   [NXBwidget::Label new]
    $lab create  $f.l -bitmap info -dragenabled 1 -dragevent 3
    pack [$lab retrievePath]
    pack [$ent1 retrievePath] [$labf1 retrievePath] [$labf2 retrievePath] -side top -fill x -pady 4
    
    
    set titf2 [NXBwidget::TitleFrame new]
    $titf2 create  $frame.titf2 -text "Drop targets"
    set subf  [$titf2 getframe [$titf2 retrievePath]]

    set ent1  [NXBwidget::LabelEntry new]
    $ent1 create $subf.e1 -label "DropEntry" -labelwidth 14 -dropenabled 1
    
    set labf1 [NXBwidget::LabelFrame new]
    $labf1 create  $subf.f1 -text "Label" -width 14
    set f     [$labf1 getframe [$labf1 retrievePath]]
    
    set lab   [NXBwidget::Label new]
    $lab create  $f.l -dropenabled 1 -highlightthickness 1
    pack [$lab retrievePath] -fill x
    pack [$ent1 retrievePath] [$labf1 retrievePath] -side top -fill x -pady 4

    pack [$titf1 retrievePath] [$titf2 retrievePath] -pady 4
    
    
    #pack the mainframe
    pack [${:mainwin} retrievePath] -fill both -expand yes
    update idletasks
}


Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "Drag & Drop demo"

    :create
    
    #BWidget::place . 450 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

set dm [Demo new]
wm geom . [wm geom .]
