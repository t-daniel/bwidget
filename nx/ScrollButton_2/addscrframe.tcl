package provide addonscrframe 1.0

package require nx


namespace eval NXBwidget {

nx::Class create AddonScrollableFrame {

    :public method registerWidget { widget } {
        set :scrwidget $widget
    }

    :public method yview { path args } {
        ${:scrwidget} yview $path $args; #+++ added by me
        next "$path $args"
    }

    :public method xview { path args } {
        ${:scrwidget} xview $path $args; #+++ added by me
        next "$path $args"
    }

}; #END - class

}
