# ----------------------------------------------------------------------------
#  dialog.tcl
#  This file is part of Unifix BWidget Toolkit
#  $Id: dialog.tcl,v 1.15.2.1 2010/08/04 13:07:59 oehhar Exp $
# ----------------------------------------------------------------------------
#  Index of commands:
#     - Dialog::create
#     - Dialog::configure
#     - Dialog::cget
#     - Dialog::getframe
#     - Dialog::add
#     - Dialog::itemconfigure
#     - Dialog::itemcget
#     - Dialog::invoke
#     - Dialog::setfocus
#     - Dialog::enddialog
#     - Dialog::draw
#     - Dialog::withdraw
#     - Dialog::_destroy
# ----------------------------------------------------------------------------
package provide dialog 1.0

package require nx
package require widget
package require buttonbox



# JDC: added -transient and -place flag
namespace eval NXBwidget {

nx::Class create Dialog -superclass NXBwidget::Widget {
    
    #+++
    :public method initVars args {
	#initialize the classvariables of the superclasses + mixinclasses and the ones of self
	next
	
	#array set :_widget {}
	
	set :_inst_ButtonBox [NXBwidget::ButtonBox new]; #+++
    }
    
    #+++
    :public method init {} {
    	:initVars;  #+++
	
	#+++
	:define Dialog dialog ButtonBox

	:bwinclude Dialog ButtonBox .bbox \
	    remove	   {-orient} \
	    initialize {-spacing 10 -padx 10}
	
	:declare Dialog {
	    {-title	      String	 ""	  0}
	    {-geometry    String	 ""	  0}
	    {-modal	      Enum	 local	  0 {none local global}}
	    {-bitmap      TkResource ""	  1 label}
	    {-image	      TkResource ""	  1 label}
	    {-separator   Boolean	 0	  1}
	    {-cancel      Int	 -1	  0 "%d >= -1"}
	    {-parent      String	 ""	  0}
	    {-side	      Enum	 bottom	  1 {bottom left top right}}
	    {-anchor      Enum	 c	  1 {n e w s c}}
	    {-class	      String	 Dialog	  1}
	    {-transient   Boolean	 1	  1}
	    {-place	      Enum	 center	  0 {none center left right above below}}
	}

        :addmap Dialog "" :cmd   {-background {}}
        :addmap Dialog "" .frame {-background {}}

        bind BwDialog <Destroy> [list [self] _destroy %W]
	#+++
    }

}

}




# ----------------------------------------------------------------------------
#  Command Dialog::create
# ----------------------------------------------------------------------------
NXBwidget::Dialog public method create { path args } {
    :storePath $path; #+++
    
    global   tcl_platform
    #variable _widget
    upvar 0 ::NXBwidget_Dialog_widget _widget; #+++ _widget muss global sein, da später tkwait darauf zugreifen wird

    array set maps [list Dialog {} .bbox {}]
    array set maps [:parseArgs Dialog $args]; #+++

    # Check to see if the -class flag was specified
    set dialogClass "Dialog"
    array set dialogArgs $maps(Dialog)
    if { [info exists dialogArgs(-class)] } {
	set dialogClass $dialogArgs(-class)
    }

    if { [string equal $tcl_platform(platform) "unix"] } {
	set re raised
	set bd 1
    } else {
	set re flat
	set bd 0
    }
    
    #+++ added by me
    if {[winfo exists $path]} {
	:_destroy $path
    }
    #+++
    
    toplevel $path -relief $re -borderwidth $bd -class $dialogClass
    wm withdraw $path

    :initFromODB Dialog $path $maps(Dialog); #+++

    bindtags $path [list $path BwDialog all]
    wm overrideredirect $path 1
    wm title $path [:cget $path -title]
    set parent [:cget $path -parent]; #+++
    if { ![winfo exists $parent] } {
        set parent [winfo parent $path]
    }
    # JDC: made transient optional
    #+++
    if { [:getoption $path -transient] } {
	wm transient $path [winfo toplevel $parent]
    }

    set side [:cget $path -side]; #+++
    if { [string equal $side "left"] || [string equal $side "right"] } {
        set orient vertical
    } else {
        set orient horizontal
    }

    set __bbox [NXBwidget::ButtonBox new]
    
    set bbox  [eval [list $__bbox create $path.bbox] $maps(.bbox) \
		   -orient $orient]; #+++
    set frame [frame $path.frame -relief flat -borderwidth 0]
    set bg [:cget $path -background]; #+++
    $path configure -background $bg
    $frame configure -background $bg
    #+++
    if { [set bitmap [:getoption $path -image]] != "" } {
        set label [label $path.label -image $bitmap -background $bg]
	#+++
    } elseif { [set bitmap [:getoption $path -bitmap]] != "" } {
        set label [label $path.label -bitmap $bitmap -background $bg]
    }
    #+++
    if { [:getoption $path -separator] } {
	#+++ Separator::create $path.sep -orient $orient -background $bg
	set __sep [NXBwidget::Separator new]
	$__sep create $path.sep -orient $orient -background $bg
    }
    set _widget($path,realized) 0
    set _widget($path,nbut)     0

    #+++
    set cancel [:getoption $path -cancel]
    bind $path <Escape>  [list $__bbox invoke $path.bbox $cancel]
    if {$cancel != -1} {
        wm protocol $path WM_DELETE_WINDOW [list $__bbox invoke $path.bbox $cancel]
    }
    bind $path <Return>  [list $__bbox invoke $path.bbox default]
    
    #+++

    return [next "Dialog $path"]; #+++
}


# ----------------------------------------------------------------------------
#  Command Dialog::configure
# ----------------------------------------------------------------------------
NXBwidget::Dialog public method configure { path args } {
    set res [:configure $path $args]; #+++

    #+++
    if { [:hasChanged $path -title title] } {
        wm title $path $title
    }
    #+++
    if { [:hasChanged $path -background bg] } {
        if { [winfo exists $path.label] } {
            $path.label configure -background $bg
        }
        if { [winfo exists $path.sep] } {
            #+++ Separator::configure $path.sep -background $bg
	    set __sep [NXBwidget::Separator new]
	    $__sep configure $path.sep -background $bg
        }
    }
    #+++
    if { [:hasChanged $path -cancel cancel] } {
	set __bbox [NXBwidget::ButtonBox new]
        bind $path <Escape>  [list $__bbox invoke $path.bbox $cancel]
        if {$cancel == -1} {
            wm protocol $path WM_DELETE_WINDOW ""
        } else {
            wm protocol $path WM_DELETE_WINDOW [list $__bbox invoke $path.bbox $cancel]
        }
    }
    return $res
}


# ----------------------------------------------------------------------------
#  Command Dialog::cget
# ----------------------------------------------------------------------------
#+++ NXBwidget::Dialog public method cget { path option } {
#    return [Widget::cget $path $option]
#}


# ----------------------------------------------------------------------------
#  Command Dialog::getframe
# ----------------------------------------------------------------------------
NXBwidget::Dialog public method getframe { path } {
    return $path.frame
}


# ----------------------------------------------------------------------------
#  Command Dialog::add
# ----------------------------------------------------------------------------
NXBwidget::Dialog public method add { path args } {
    #variable _widget
    upvar 0 ::NXBwidget_Dialog_widget _widget; #+++

    if {[string equal $::tcl_platform(platform) "windows"]
	&& $::tk_version >= 8.4} {
	set width -11
    } else {
	set width 8
    }
    #+++
    set cmd [list ${:_inst_ButtonBox} add $path.bbox -width $width \
		 -command [list [self] enddialog $path $_widget($path,nbut)]]
    set res [eval $cmd $args]
    puts "\n-->Dialog::add\n  cmd: $cmd\n  args: $args\n"
    incr _widget($path,nbut)
    return $res
}


# ----------------------------------------------------------------------------
#  Command Dialog::itemconfigure
# ----------------------------------------------------------------------------
NXBwidget::Dialog public method itemconfigure { path index args } {
    #+++ return [eval [list ButtonBox::itemconfigure $path.bbox $index] $args]
    
    #set __bbox [NXBwidget::ButtonBox new]
    return [eval [list ${:_inst_ButtonBox} itemconfigure $path.bbox $index] $args]
}


# ----------------------------------------------------------------------------
#  Command Dialog::itemcget
# ----------------------------------------------------------------------------
NXBwidget::Dialog public method itemcget { path index option } {
    #+++
    #set __bbox [NXBwidget::ButtonBox new]
    return [${:_inst_ButtonBox} itemcget $path.bbox $index $option]
}


# ----------------------------------------------------------------------------
#  Command Dialog::invoke
# ----------------------------------------------------------------------------
NXBwidget::Dialog public method invoke { path index } {
    #+++
    #set __bbox [NXBwidget::ButtonBox new]
    ${:_inst_ButtonBox} invoke $path.bbox $index
}


# ----------------------------------------------------------------------------
#  Command Dialog::setfocus
# ----------------------------------------------------------------------------
NXBwidget::Dialog public method setfocus { path index } {
    #+++
    #set __bbox [NXBwidget::ButtonBox new]
    ${:_inst_ButtonBox} setfocus $path.bbox $index
}


# ----------------------------------------------------------------------------
#  Command Dialog::enddialog
# ----------------------------------------------------------------------------
NXBwidget::Dialog public method enddialog { path result } {
    #variable _widget
    upvar 0 ::NXBwidget_Dialog_widget _widget; #+++

    set _widget($path,result) $result
}


# ----------------------------------------------------------------------------
#  Command Dialog::draw
# ----------------------------------------------------------------------------
NXBwidget::Dialog public method draw { path {focus ""} {overrideredirect 0} {geometry ""}} {
    #variable _widget
    upvar 0 ::NXBwidget_Dialog_widget _widget; #+++

    set parent [:getoption $path -parent]; #+++
    if { !$_widget($path,realized) } {
        set _widget($path,realized) 1
        if { [llength [winfo children $path.bbox]] } {
            set side [:getoption $path -side]; #+++
            if {[string equal $side "left"] || [string equal $side "right"]} {
                set pad  -padx
                set fill y
            } else {
                set pad  -pady
                set fill x
            }
            pack $path.bbox -side $side -padx 1m -pady 1m \
		-anchor [:getoption $path -anchor]; #+++
            if { [winfo exists $path.sep] } {
                pack $path.sep -side $side -fill $fill $pad 2m
            }
        }
        if { [winfo exists $path.label] } {
            pack $path.label -side left -anchor n -padx 3m -pady 3m
        }
        pack $path.frame -padx 1m -pady 1m -fill both -expand yes
    }

    set geom [:getMegawidgetOption $path -geometry]; #+++
    if { $geom != "" } {
	wm geometry $path $geom
    }

    if { [string equal $geometry ""] && ($geom == "") } {
	set place [:getoption $path -place]; #+++
	if { ![string equal $place none] } {
	    if { [winfo exists $parent] } {
		:place $path 0 0 $place $parent; #+++
	    } else {
		:place $path 0 0 $place; #+++
	    }
	}
    } else {
	if { $geom != "" } {
	    wm geometry $path $geom
	} else {
	    wm geometry $path $geometry
	}
    }
    update idletasks
    wm overrideredirect $path $overrideredirect
    wm deiconify $path

    # patch by Bastien Chevreux (bach@mwgdna.com)
    # As seen on Windows systems *sigh*
    # When the toplevel is withdrawn, the tkwait command will wait forever.
    #  So, check that we are not withdrawn
    if {![winfo exists $parent] || \
	    ([wm state [winfo toplevel $parent]] != "withdrawn")} {
	tkwait visibility $path
    }
    :focus set $path; #+++
    if { [winfo exists $focus] } {
        focus -force $focus
    } else {
	#+++
	#set __bbox [NXBwidget::ButtonBox new]
        ${:_inst_ButtonBox} setfocus $path.bbox default
    }

    if { [set grab [:cget $path -modal]] != "none" } {
        :grab $grab $path
        if {[info exists _widget($path,result)]} { 
            unset _widget($path,result)
        }
        #+++ tkwait variable Dialog::_widget($path,result)
	tkwait variable ::NXBwidget_Dialog_widget($path,result)
	#+++
	
        if { [info exists _widget($path,result)] } {
            set res $_widget($path,result)
            unset _widget($path,result)
        } else {
            set res -1
        }
        :withdraw $path; #+++
        return $res
    }
    return ""
}


# ----------------------------------------------------------------------------
#  Command Dialog::withdraw
# ----------------------------------------------------------------------------
NXBwidget::Dialog public method withdraw { path } {
    #+++
    :grab release $path
    :focus release $path
    if { [winfo exists $path] } {
        wm withdraw $path
    }
}


# ----------------------------------------------------------------------------
#  Command Dialog::_destroy
# ----------------------------------------------------------------------------
NXBwidget::Dialog public method _destroy { path } {
    #variable _widget
    upvar 0 ::NXBwidget_Dialog_widget _widget; #+++

    :enddialog $path -1; #+++

    #+++
    :grab  release $path
    :focus release $path
    if {[info exists _widget($path,result)]} {
        unset _widget($path,result)
    }
    catch  {unset _widget($path,realized)}
    catch  {unset _widget($path,nbut)}

    puts "\n-->Dialog::_destroy\n  path: $path\n"
    :destroy $path; #+++
    
    ::destroy $path; #+++ added by me
}


#+++ (added by me)
NXBwidget::Dialog public method storePath {path} {
    set :wpath $path
}


#+++ (added by me)
NXBwidget::Dialog public method retrievePath {} {
    return ${:wpath}
}
