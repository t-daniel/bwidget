#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

lappend auto_path .
package provide mylistbox 1.0

package require nx
package require bwidget
package require listbox

nx::Class create Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


Demo public method create { } {
#    Demo::createMainwindow
    
    
#    set frame    [${Demo::mainwin} getframe]
    
    #+++
    set frame [frame .topf]
    pack $frame -pady 2 -fill x
    
    
    #+++
    #create the ListBox and fill it with some string-items
    set lb [NXBwidget::ListBox new]
    $lb create $frame.lb -height 8 -width 20 -highlightthickness 0
    for {set i 1} {$i <= 4} {incr i} {
       $lb insert $frame.lb end "Value_${i}" -text "Value_${i}"
       pack [$lb retrievePath] -side top -padx 4 -pady 2
    }
    
    
    #pack the mainframe
#    pack ${Demo::mainwin} -fill both -expand yes
    update idletasks
}



Demo public method init {} {
    set :DEMODIR [pwd]
    lappend ::auto_path [file dirname ${:DEMODIR}]; #DEMODIR is required for the line below !
#    package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "ListBox demo"

    :create
#    BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    wm deiconify .
    raise .
    focus -force .
}

set dm [Demo new]
wm geom . [wm geom .]
