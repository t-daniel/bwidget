#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide myentry 1.0

package require nx
package require widget
package require entry



nx::Class create Demo {
    variable status
    variable prgindic
    variable var
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


Demo public method create { } {
    #Demo::createMainwindow
    
    #set frame    [${Demo::mainwin} getframe]
    
    set frame [frame .topf]
    pack $frame -pady 2 -fill x
    
    
    #create the Entry
    set ent  [NXBwidget::Entry new]
    
    $ent create $frame.entry -text "Press enter" \
                  -command  "puts  \"The content of the Entry: \[$ent cget $frame.entry -text\]\"" \
                  -helptext "Entry widget"
                  
    #this parameter NOT works:  -helptext "Entry widget"
    
    pack [$ent retrievePath] -pady 4 -anchor w
    
    $ent place . 200 50 right
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    update idletasks
}



Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "Entry demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

set dm [Demo new]
wm geom . [wm geom .]
