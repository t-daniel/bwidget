# ----------------------------------------------------------------------------
#  progressdlg.tcl
#  This file is part of Unifix BWidget Toolkit
# ----------------------------------------------------------------------------
#  Index of commands:
#     - ProgressDlg::create
# ----------------------------------------------------------------------------
package provide progressdialog 1.0

package require nx
package require widget
package require dialog
package require progressbar
package require bitmap



namespace eval NXBwidget {

nx::Class create ProgressDlg -superclass NXBwidget::Widget {
    
    #+++
    :public method initVars args {
	#initialize the classvariables of the superclasses + mixinclasses and the ones of self
	next
	
	set :_inst_Dialog [NXBwidget::Dialog new]
	set :_inst_ProgressBar [NXBwidget::ProgressBar new]
    }
    
    #+++
    :public method init {} {
    	:initVars;  #+++
	
	#+++
	:define ProgressDlg progressdlg Dialog ProgressBar

	:bwinclude ProgressDlg Dialog :cmd \
	    remove {
            -modal -image -bitmap -side -anchor -cancel -default
            -homogeneous -padx -pady -spacing
        }

	:bwinclude ProgressDlg ProgressBar .frame.pb \
	    remove {-orient -width -height}
	
	
	:declare ProgressDlg {
	    {-width        TkResource 25 0 label}
	    {-height       TkResource 2  0 label}
	    {-textvariable TkResource "" 0 label}
	    {-font         TkResource "" 0 label}
	    {-stop         String "" 0}
	    {-command      String "" 0}
	}

	:addmap ProgressDlg :cmd .frame.msg \
	    {-width {} -height {} -textvariable {} -font {} -background {}}
	#+++
    }

}

}




# ----------------------------------------------------------------------------
#  Command ProgressDlg::create
# ----------------------------------------------------------------------------
NXBwidget::ProgressDlg public method create { path args } {
    :storePath $path; #+++
    
    array set maps [list ProgressDlg {} :cmd {} .frame.msg {} .frame.pb {}]
    array set maps [:parseArgs ProgressDlg $args]; #+++

    #+++
    set __bmp [NXBwidget::Bitmap new]
    $__bmp create ${:BWIDGET_LIBRARY}/images
    eval [list ${:_inst_Dialog} create] $path $maps(:cmd) \
	[list -image [$__bmp get hourglass] \
	     -modal none -side bottom -anchor e -class ProgressDlg]

    :initFromODB ProgressDlg "$path#ProgressDlg" $maps(ProgressDlg); #+++

    wm protocol $path WM_DELETE_WINDOW {;}

    set frame [${:_inst_Dialog} getframe $path]; #+++
    bind $frame <Destroy> [list [self] destroy $path\#ProgressDlg]; #+++
    $frame configure -cursor watch

    eval [list label $frame.msg] $maps(.frame.msg) \
	-relief flat -borderwidth 0 \
	    -highlightthickness 0 -anchor w -justify left
    pack $frame.msg -side top -pady 3m -anchor nw -fill x -expand yes

    #+++
    eval [list ${:_inst_ProgressBar} create] $frame.pb $maps(.frame.pb) -width 100
    pack $frame.pb -side bottom -anchor w -fill x -expand yes

    set stop [:cget "$path#ProgressDlg" -stop]
    set cmd  [:cget "$path#ProgressDlg" -command]; #+++
    if { $stop != "" && $cmd != "" } {
        ${:_inst_Dialog} add $path -text $stop -name $stop -command $cmd; #+++
    }
    #+++
    ${:_inst_Dialog} draw $path
    :grab local $path

    return [next [list ProgressDlg $path 0]]; #+++
}


# ----------------------------------------------------------------------------
#  Command ProgressDlg::configure
# ----------------------------------------------------------------------------
NXBwidget::ProgressDlg public method configure { path args } {
    puts "\nProgressDlg::configure\n  path: $path\”"
    return [next [list "$path#ProgressDlg" $args]]; #+++
}


# ----------------------------------------------------------------------------
#  Command ProgressDlg::cget
# ----------------------------------------------------------------------------
#+++ NXBwidget::ProgressDlg public method cget { path option } {
#    return [next [list "$path#ProgressDlg" $option]]; #+++
#}


#+++ (added by me)
NXBwidget::ProgressDlg public method storePath {path} {
    set :wpath $path
}


#+++ (added by me)
NXBwidget::ProgressDlg public method retrievePath {} {
    return ${:wpath}
}
