#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide mypanelframe 1.0

package require nx
package require bwidget
package require panelframe



nx::Class create Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


Demo public method create { } {
    #Demo::createMainwindow
    
    #set frame    [${Demo::mainwin} getframe]
    
    set frame [frame .topf]
    pack $frame -pady 2 -fill x
    
    
    #some padding
    set lab0 [label $frame.lab0 -text ""]
    pack $lab0 -pady 2 -fill x
    
    
    #create the PanelFrame
    set pfr  [NXBwidget::PanelFrame new]
    $pfr create $frame.pfr -text "This is a PanelFrame" -relief sunken -borderwidth 2
    pack [$pfr retrievePath] -pady 2 -fill x
    $pfr place . 300 300 right
    
    
    #fill the PanelFrame
    set pframe [$pfr getframe [$pfr retrievePath]]
    set lab1 [label $pframe.lab1 -text "Label_1"]
    pack $lab1 -pady 2 -fill x
    
    set lab2 [label $pframe.lab2 -text "Label_2"]
    pack $lab2 -pady 2 -fill x
    
    set lb    [listbox $pframe.lb -height 8 -width 20 -highlightthickness 0]
    for {set i 1} {$i <= 4} {incr i} {
       $lb insert end "Value_${i}"
       pack $lb -side top -padx 4 -pady 2
    }
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    
    update idletasks
}



Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "PanelFrame demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

set dm [Demo new]
wm geom . [wm geom .]
