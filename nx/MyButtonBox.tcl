#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide mybuttonbox 1.0

package require nx
package require bwidget
package require buttonbox
package require bitmap



nx::Class create Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


Demo public method create { } {
    #Demo::createMainwindow
    
    #set frame    [${Demo::mainwin} getframe]
    
    set frame [frame .topf]
    pack $frame -pady 2 -fill x
    
    
    #create the ButtonBox
    set bbox [NXBwidget::ButtonBox new]
    $bbox create $frame.bbox1 -spacing 0 -padx 1 -pady 1
    
    #add an image to the Buttonbox
    set bmp [NXBwidget::Bitmap new]
    $bmp create "${:DEMODIR}/img"
    set img [$bmp get info]
    
    $bbox add [$bbox retrievePath] -image $img \
        -highlightthickness 0 -takefocus 0 -relief link -borderwidth 1 -padx 1 -pady 1 \
        -helptext "Get info" \
        -command  "Demo::buttoncmd {Get info}";  #this comes from the widget Button
    
    
    $bmp create "${:DEMODIR}/images"
    set openimg [$bmp get new]
    $bbox add [$bbox retrievePath] -image $openimg \
        -highlightthickness 0 -takefocus 0 -relief link -borderwidth 1 -padx 1 -pady 1 \
        -helptext "Create a new file" \
        -command  "Demo::buttoncmd {Create new file}";  #this comes from the widget Button
    pack [$bbox retrievePath] -side top
    
    #set the size of the main window
    $bbox place . 240 50 right
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    update idletasks
}


Demo public method init {} {
    set :DEMODIR [pwd]
    lappend ::auto_path [file dirname ${:DEMODIR}]

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "ButtonBox demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}


namespace eval Demo {
    proc buttoncmd { btnName } {
        puts "Button '${btnName}' pressed!"
    }
}


set dm [Demo new]
wm geom . [wm geom .]
