#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide mystatusbar 1.0

package require nx
package require widget
package require statusbar
package require button



nx::Class create Demo {
    #variable status
    #variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable DemoGlob::status \
                       -progressvar  DemoGlob::prgindic]
}


Demo public method create { } {
    #Demo::createMainwindow
    
    #set frame    [${Demo::mainwin} getframe]
    
    set frame [frame .topf]
    pack $frame -pady 2 -fill x
    
    
    #create a Button
    set but   [NXBwidget::Button new]
    $but create $frame.but -text "Click it!" \
                   -command  "DemoGlob::buttoncmd \"This is the statusbar...\"" \
                   -helptext "This is a Button widget"
    pack [$but retrievePath]  -side top -padx 4
    
    
    #create the Statusbar
    set sbar $frame.s
    set instsb [NXBwidget::StatusBar new]
    $instsb create $sbar
    pack [$instsb retrievePath] -side bottom -fill x
    set f [$instsb getframe $sbar]
    
    set w [label $f.status -width 1 -anchor w -textvariable DemoGlob::status]
    # give the entry weight, as we want it to be the one that expands
    $instsb add [$instsb retrievePath] $w -weight 1
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    update idletasks
}


Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm geometry . 190x100
    wm title . "StatusBar demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

namespace eval DemoGlob {
    set status   ""
    
    proc buttoncmd {text} {
        set DemoGlob::status   "$text"
        update
        after 2300 "DemoGlob::buttoncmd \"\""
    }
}

set dm [Demo new]
wm geom . [wm geom .]
