#!/usr/bin/wish

wm withdraw .
wm title . "Tk-LabelEntry"
wm deiconify .
raise .

label .lab -text "Input"
entry .ent -width 20

grid .lab -column 0 -row 0
grid .ent -column 1 -row 0
