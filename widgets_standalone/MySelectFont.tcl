#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}



namespace eval Demo {
    variable status
    variable prgindic
    set font ""; #required by SelectFont
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


proc Demo::createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


proc Demo::create { } {
    Demo::createMainwindow
    
    
    set frame    [${Demo::mainwin} getframe]
    
    
    #create the SelectFont Widget
    set but1  [button $frame.but1 \
                   -text    "SelectFont Widget" \
                   -command Demo::_show_fontdlg]
    pack $but1 -pady 2 -fill x
    
    
    #pack the mainframe
    pack ${Demo::mainwin} -fill both -expand yes
    update idletasks
}


proc Demo::_show_fontdlg { } {
    set font [SelectFont .fontdlg -parent .]
    if { $font != "" } {
    	#For the puts below to work, put this:
    	#set ::SelectFont::__selectedFont  $font
    	#before the line:
    	#Widget::setoption "$path#SelectFont" -font $font
    	#into the method SelectFont::_update
        puts "\nThe selected font: [set ::SelectFont::__selectedFont]\n"
    }
}



proc Demo::main {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "SelectFont demo"

    Demo::create
    BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    wm deiconify .
    raise .
    focus -force .
}

Demo::main
wm geom . [wm geom .]
