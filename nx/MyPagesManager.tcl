#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide mypagesmanager 1.0

package require nx
package require bwidget
package require pagesmanager



nx::Class create Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


Demo public method create { } {
    #Demo::createMainwindow
    
    #set frame    [${Demo::mainwin} getframe]
    
    set frame [frame .topf]
    pack $frame -pady 2 -fill x
    
    
    #create the PagesManager
    set pm [NXBwidget::PagesManager new]
    $pm create $frame.pm
    pack [$pm retrievePath] -fill both -expand true
 
    set npages 5
    for { set i 0 } { $i < $npages } { incr i } {
       set p [$pm add [$pm retrievePath] $i]
       #pack [button .f.b$i -width 4 -text $i -command [list .pm raise $i]] -fill x
       grid [label $p.lbl0 -text "This is page $i"] -row 0 -column 0 -columnspan 2
       grid [label $p.lbl1 -text "Place some content here."] -row 1 -column 0 -columnspan 2
       
       grid [button $p.prev -text "Back" -command [list $pm raise [$pm retrievePath] [expr {$i-1}]]] -row 2 -column 0
       grid [button $p.next -text "Next" -command [list $pm raise [$pm retrievePath] [expr {$i+1}]]] -row 2 -column 1
       
       if { $i == 0 } { $p.prev configure -state disabled }
       if { $i >= ($npages-1) } { $p.next configure -state disabled }
    }
    $pm raise [$pm retrievePath] 0
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    
    update idletasks
}


Demo public method init args {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "PagesManager demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

set dm [Demo new]
wm geom . [wm geom .]
