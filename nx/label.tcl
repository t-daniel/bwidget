# ------------------------------------------------------------------------------
#  label.tcl
#  This file is part of Unifix BWidget Toolkit
#  $Id: label.tcl,v 1.10.2.3 2011/04/26 08:24:28 oehhar Exp $
# ------------------------------------------------------------------------------
#  Index of commands:
#     - Label::create
#     - Label::configure
#     - Label::cget
#     - Label::setfocus
#     - Label::_drag_cmd
#     - Label::_drop_cmd
#     - Label::_over_cmd
# ------------------------------------------------------------------------------
package provide label 1.0

package require nx
package require widget
package require dynamichelp
package require dragsite
package require dropsite



namespace eval NXBwidget {

nx::Class create Label -superclass NXBwidget::Widget -mixin {NXBwidget::DynamicHelp NXBwidget::DragSite NXBwidget::DropSite} {
    
    #+++
    :public method initVars args {
	#initialize the classvariables of the superclasses + mixinclasses and the ones of self
	next
	:dynhelp_init; #+++
	:dragsite_init; #+++
	:dropsite_init; #+++
    }
    
    
    #+++
    :public method init {} {
    	:initVars;  #+++
	
	:define Label label DragSite DropSite DynamicHelp
	
	if {${:_theme}} {
	    :tkinclude Label label .l \
	    remove { -foreground -text -textvariable -underline -state}
	} else {
	    :tkinclude Label label .l \
	        remove { -foreground -text -textvariable -underline }
	}
	
	:declare Label {
            {-name               String     ""     0}
            {-text               String     ""     0}
            {-textvariable       String     ""     0}
            {-underline          Int        -1     0 "%d >= -1"}
            {-focus              String     ""     0}
            {-foreground         TkResource ""     0 label}
            {-disabledforeground TkResource ""     0 button}
            {-state              Enum       normal 0  {normal disabled}}

            {-fg                 Synonym    -foreground}
	}
	
	:dyn_include Label balloon
	:drag_include    Label "" 1
        :drop_include    Label {
            TEXT    {move {}}
            IMAGE   {move {}}
            BITMAP  {move {}}
            FGCOLOR {move {}}
            BGCOLOR {move {}}
            COLOR   {move {}}
        }
	
	:syncoptions Label "" .l {-text {} -underline {}}
	
	bind BwLabel <FocusIn> [list [self] setfocus %W]
	bind BwLabel <Destroy> [list [self] _destroy %W]
    }
    
    
    #Widget::define Label label DragSite DropSite DynamicHelp

    #if {$::Widget::_theme} {
    #    Widget::tkinclude Label label .l \
    #        remove { -foreground -text -textvariable -underline -state}
    #	} else {
    #    Widget::tkinclude Label label .l \
    #        remove { -foreground -text -textvariable -underline }
    #	}

    #Widget::declare Label {
    #    {-name               String     ""     0}
    #    {-text               String     ""     0}
    #    {-textvariable       String     ""     0}
    #    {-underline          Int        -1     0 "%d >= -1"}
    #    {-focus              String     ""     0}
    #    {-foreground         TkResource ""     0 label}
    #    {-disabledforeground TkResource ""     0 button}
    #    {-state              Enum       normal 0  {normal disabled}}
    #
    #    {-fg                 Synonym    -foreground}
    #}

    #DynamicHelp::include Label balloon
    #DragSite::include    Label "" 1
    #DropSite::include    Label {
    #    TEXT    {move {}}
    #    IMAGE   {move {}}
    #    BITMAP  {move {}}
    #    FGCOLOR {move {}}
    #    BGCOLOR {move {}}
    #    COLOR   {move {}}
    #}

    #Widget::syncoptions Label "" .l {-text {} -underline {}}

    #bind BwLabel <FocusIn> [list Label::setfocus %W]
    #bind BwLabel <Destroy> [list Label::_destroy %W]

}; #END - class

}; #END - namespace



# ------------------------------------------------------------------------------
#  Command Label::create
# ------------------------------------------------------------------------------
NXBwidget::Label public method create { path args } {
    :storePath $path; #+++
    
    array set maps [list Label {} .l {}]
    array set maps [:parseArgs Label $args]; #+++
    frame $path -class Label -borderwidth 0 -highlightthickness 0 -relief flat -padx 0 -pady 0
    :initFromODB Label $path $maps(Label); #+++

    #+++
    if {${:_theme}} {
        eval [list ttk::label $path.l] $maps(.l)
	puts "\n-->Label::create - ttk::label\n"
    } else {
        eval [list label $path.l] $maps(.l)
	puts "\n-->Label::create - label\n"
    }

    #+++
    if {${:_theme}} {
        if { [:cget $path -state] != "normal" } {
            $path.l state disabled
		}
    } else {
        if { [:cget $path -state] == "normal" } {
            set fg [:cget $path -foreground]
        } else {
            set fg [:cget $path -disabledforeground]
        }
        $path.l configure -foreground $fg; #+++
    }

    set var [:cget $path -textvariable]; #+++
    #+++
    if {  $var == "" &&
          [:cget $path -image] == "" &&
          (${:_theme} || [:cget $path -bitmap] == "")} {
        set desc [:getname [:cget $path -name]]
        if { $desc != "" } {
	    puts "\n-->Label::create\n  \
	         desc: $desc\n"
            set text  [lindex $desc 0]
            set under [lindex $desc 1]
        } else {
            set text  [:cget $path -text]
            set under [:cget $path -underline]
        }
    } else {
        set under -1
        set text  ""
    }

    $path.l configure -text $text -textvariable $var \
	    -underline $under

    set accel [string tolower [string index $text $under]]
    if { $accel != "" } {
        bind [winfo toplevel $path] <Alt-$accel> "[self] setfocus $path"; #+++
    }

    bindtags $path   [list BwLabel [winfo toplevel $path] all]
    bindtags $path.l [list $path.l $path Label [winfo toplevel $path] all]
    pack $path.l -expand yes -fill both

    set dragendcmd [:cget $path -dragendcmd]; #+++
    
    #+++
    #DragSite::setdrag $path $path.l Label::_init_drag_cmd $dragendcmd 1
    #DropSite::setdrop $path $path.l Label::_over_cmd Label::_drop_cmd 1
    #DynamicHelp::sethelp $path $path.l 1
    
    :setdrag $path $path.l "[self] _init_drag_cmd" $dragendcmd 1
    :setdrop $path $path.l "[self] _over_cmd" "[self] _drop_cmd" 1
    :sethelp $path $path.l 1
    #+++

    return [next "Label $path"]
}


# ------------------------------------------------------------------------------
#  Command Label::configure
# ------------------------------------------------------------------------------
NXBwidget::Label public method configure { path args } {
    puts "\n-->Label::configure\n  path: $path  \
         args: $args\n"
    
    set oldunder [$path.l cget -underline]
    if { $oldunder != -1 } {
        set oldaccel [string tolower [string index [$path.l cget -text] $oldunder]]
    } else {
        set oldaccel ""
    }
    set res [:widget_configure $path $args]; #+++

    set cfg  [:hasChanged $path -foreground fg]; #+++
    set cst  [:hasChanged $path -state state]; #+++

    #+++
    if {${:_theme}} {
        if { $cfg } {
            $path.l configure -foreground $fg
        }
        if { $cst } {
            if { $state == "normal" } {
                $path.l state !disabled
            } else {
                $path.l state disabled
            }
        }
    } else {
        set cdfg [:hasChanged $path -disabledforeground dfg]; #+++
        if { $cst || $cfg || $cdfg } {
            if { $state == "normal" } {
                $path.l configure -fg $fg
            } else {
                $path.l configure -fg $dfg
            }
        }
	}

    set cv [:hasChanged $path -textvariable var]; #+++
    set cb [:hasChanged $path -image img]; #+++
    if {${:_theme}} {
        set ci 0
        set bmp ""
	} else {
        set ci [:hasChanged $path -bitmap bmp]; #+++
	}
    set cn [:hasChanged $path -name name]; #+++
    set ct [:hasChanged $path -text text]; #+++
    set cu [:hasChanged $path -underline under]; #+++

    if { $cv || $cb || $ci || $cn || $ct || $cu } {
        if {  $var == "" && $img == "" && $bmp == "" } {
            set desc [:getname $name]; #+++
            if { $desc != "" } {
                set text  [lindex $desc 0]
                set under [lindex $desc 1]
            }
        } else {
            set under -1
            set text  ""
        }
        set top [winfo toplevel $path]
        if { $oldaccel != "" } {
            bind $top <Alt-$oldaccel> {}
        }
        set accel [string tolower [string index $text $under]]
        if { $accel != "" } {
            bind $top <Alt-$accel> [list [self] setfocus $path]; #+++
        }
        $path.l configure -text $text -underline $under -textvariable $var
    }

    set force [:hasChanged $path -dragendcmd dragend]; #+++
    
    #+++
    #DragSite::setdrag $path $path.l Label::_init_drag_cmd $dragend $force
    #DropSite::setdrop $path $path.l Label::_over_cmd Label::_drop_cmd
    #DynamicHelp::sethelp $path $path.l
    
    :setdrag $path $path.l "[self] _init_drag_cmd" $dragend $force
    :setdrop $path $path.l "[self] _over_cmd" "[self] _drop_cmd"
    :sethelp $path $path.l
    #+++
    
    return $res
}


# ------------------------------------------------------------------------------
#  Command Label::cget
# ------------------------------------------------------------------------------
#+++ proc Label::cget { path option } {
#    return [Widget::cget $path $option]
#}


# ----------------------------------------------------------------------------
#  Command Label::identify
# ----------------------------------------------------------------------------
NXBwidget::Label public method identify { path args } {
    eval $path.l identify $args
}


# ----------------------------------------------------------------------------
#  Command Label::instate
# ----------------------------------------------------------------------------
NXBwidget::Label public method instate { path args } {
    eval $path.l instate $args
}


# ----------------------------------------------------------------------------
#  Command Label::state
# ----------------------------------------------------------------------------
NXBwidget::Label public method state { path args } {
    eval $path.l state $args
}


# ------------------------------------------------------------------------------
#  Command Label::setfocus
# ------------------------------------------------------------------------------
NXBwidget::Label public method setfocus { path } {
    if { [string equal [:cget $path -state] "normal"] } {
        set w [:cget $path -focus]; #+++
	#+++
        if { [winfo exists $w] && [:focusOK $w] } {
            focus $w
        }
    }
}


# ------------------------------------------------------------------------------
#  Command Label::_init_drag_cmd
# ------------------------------------------------------------------------------
NXBwidget::Label public method _init_drag_cmd { path X Y top } {
    set path [winfo parent $path]
    #+++
    if { [set cmd [:cget $path -draginitcmd]] != "" } {
        return [uplevel \#0 $cmd [list $path $X $Y $top]]
    }
    if { [set data [$path.l cget -image]] != "" } {
        set type "IMAGE"
        pack [label $top.l -image $data]
    } elseif { [set data [$path.l cget -bitmap]] != "" } {
        set type "BITMAP"
        pack [label $top.l -bitmap $data]
    } else {
        set data [$path.l cget -text]
        set type "TEXT"
    }
    set usertype [:getoption $path -dragtype]; #+++
    if { $usertype != "" } {
        set type $usertype
    }
    return [list $type {copy} $data]
}


# ------------------------------------------------------------------------------
#  Command Label::_drop_cmd
# ------------------------------------------------------------------------------
NXBwidget::Label public method _drop_cmd { path source X Y op type data } {
    set path [winfo parent $path]
    #+++
    if { [set cmd [:cget $path -dropcmd]] != "" } {
        return [uplevel \#0 $cmd [list $path $source $X $Y $op $type $data]]
    }
    if { $type == "COLOR" || $type == "FGCOLOR" } {
        :configure $path -foreground $data; #+++
    } elseif { $type == "BGCOLOR" } {
        :configure $path -background $data; #+++
    } else {
        set text   ""
        set image  ""
        set bitmap ""
        switch -- $type {
            IMAGE   {set image $data}
            BITMAP  {set bitmap $data}
            default {
                set text $data
                if { [set var [$path.l cget -textvariable]] != "" } {
                    :configure $path -image "" -bitmap ""; #+++
                    GlobalVar::setvar $var $data
                    return
                }
            }
        }
        :configure $path -text $text -image $image -bitmap $bitmap; #+++
    }
    return 1
}


# ------------------------------------------------------------------------------
#  Command Label::_over_cmd
# ------------------------------------------------------------------------------
NXBwidget::Label public method _over_cmd { path source event X Y op type data } {
    set path [winfo parent $path]
    #+++
    if { [set cmd [:cget $path -dropovercmd]] != "" } {
        return [uplevel \#0 $cmd [list $path $source $event $X $Y $op $type $data]]
    }
    #+++
    if { [:getoption $path -state] == "normal" ||
         $type == "COLOR" || $type == "FGCOLOR" || $type == "BGCOLOR" } {
        #+++ DropSite::setcursor based_arrow_down
	:setcursor based_arrow_down; #+++
        return 1
    }
    #+++ DropSite::setcursor dot
    :setcursor dot; #+++
    return 0
}


NXBwidget::Label public method _destroy { path } {
    #+++ Widget::destroy $path
    :destroy $path; #+++
}



#+++ (added by me)
NXBwidget::Label public method storePath {path} {
    set :wpath $path
}


#+++ (added by me)
NXBwidget::Label public method retrievePath {} {
    return ${:wpath}
}
