package provide buttonscroll 1.0

package require nx


namespace eval NXBwidget {

nx::Class create ButtonScroll {

:public method initVars args {
  set :_textpos 30
  next
}


:public method scrollUp {} {
    set cfont [list Times 20]
    
    if {${:_textpos} > 20} {
	set :_textpos [expr  {${:_textpos} - 1}]
	
	canvas .can
	.can create text 10 ${:_textpos} -anchor w -font $cfont \
	  -text "Press Me!"
	
	.can postscript -width 95 -height 15 -x 1 -y 20  -file out.ps
	#The Img package (http://sourceforge.net/projects/tkimg/?source=typ_redirect)
	#can convert a postscript to xbm as well.
	#set cmd "/cygdrive/c/ImageMagick-6.8.9-Q16/convert"
	#exec $cmd out.ps out.xbm
	exec convert out.ps out.xbm

	image create bitmap .img -file out.xbm
	:configure [:retrievePath] -image .img
    
	destroy .can
	destroy .img
    }
}


:public method scrollDown {} {
    set cfont [list Times 20]
    
    if {${:_textpos} < 40} {
	set :_textpos [expr  {${:_textpos} + 1}]
	
	canvas .can
	.can create text 10 ${:_textpos} -anchor w -font $cfont \
	  -text "Press Me!"
	
	.can postscript -width 95 -height 15 -x 1 -y 20  -file out.ps
	#The Img package (http://sourceforge.net/projects/tkimg/?source=typ_redirect)
	#can convert a postscript to xbm as well.
	#set cmd "/cygdrive/c/ImageMagick-6.8.9-Q16/convert"
	#exec $cmd out.ps out.xbm
	exec convert out.ps out.xbm

	image create bitmap .img -file out.xbm
	:configure [:retrievePath] -image .img
    
	destroy .can
	destroy .img
    }
}


:public method xview { path args } {
}


:public method yview { path args } {
    puts "\n-->Button::yview\n  \
         args: $args\n"
    set tmp [string trim $args "{}"]
    set lst [split $tmp " "]
    set value [lindex $lst 1]
    set un [lindex $lst 2]
    if {$un == "units"} {
	if {$value < 0} {
	    :scrollUp
	} else {
	    :scrollDown
	}
    }
}

}; #END - clcass

}
