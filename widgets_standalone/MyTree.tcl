#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}



namespace eval Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


proc Demo::createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


proc Demo::create { } {
    Demo::createMainwindow
    
    
    set frame    [${Demo::mainwin} getframe]
    
    
    #create the Tree
    #ScrolledWindow is required to make a scrollable tree
    set sw    [ScrolledWindow $frame.sw \
                  -relief sunken -borderwidth 2]
    set tree  [Tree $sw.tree \
                   -relief flat -borderwidth 0 -width 15 -highlightthickness 0\
		   -redraw 0 -dropenabled 1 -dragenabled 1 \
                   -dragevent 3 \
                   -droptypes {
                       TREE_NODE    {copy {} move {} link {}}
                       LISTBOX_ITEM {copy {} move {} link {}}
                   } \
                   -opencmd   "Demo::moddir 1 $sw.tree" \
                   -closecmd  "Demo::moddir 0 $sw.tree"]
    $sw setwidget $tree
    pack $sw    -side top  -expand yes -fill both
    
    
    #$list is required by the functions below (Demo::)
    #Listbox could be used to display e.g the files in a directory, that was
    #selcted in the Tree.
    set list [listbox .choose -height 0 -selectmode multiple]; #just an empty listbox
    #set list [ListBox::create $sw.lb \
    #              -relief flat -borderwidth 0 \
    #              -dragevent 3 \
    #              -dropenabled 1 -dragenabled 1 \
    #              -width 20 -highlightthickness 0 -multicolumn true \
    #              -redraw 0 -dragenabled 1 \
    #              -droptypes {
    #                  TREE_NODE    {copy {} move {} link {}}
    #                  LISTBOX_ITEM {copy {} move {} link {}}}]


    $tree bindText  <ButtonPress-1>        "Demo::select tree 1 $tree $list"
    $tree bindText  <Double-ButtonPress-1> "Demo::select tree 2 $tree $list"
    #$list bindText  <ButtonPress-1>        "Demo::select list 1 $tree $list"
    #$list bindText  <Double-ButtonPress-1> "Demo::select list 2 $tree $list"
    #$list bindImage <Double-ButtonPress-1> "Demo::select list 2 $tree $list"

    Demo::init $tree $list
    
    
    
    #pack the mainframe
    pack ${Demo::mainwin} -fill both -expand yes
    update idletasks
}


#required by the Tree (see demo/tree.tcl)
proc Demo::moddir { idx tree node } {
    if { $idx && [$tree itemcget $node -drawcross] == "allways" } {
        getdir $tree $node [$tree itemcget $node -data]
        if { [llength [$tree nodes $node]] } {
            $tree itemconfigure $node -image [Bitmap::get openfold]
        } else {
            $tree itemconfigure $node -image [Bitmap::get folder]
        }
    } else {
        $tree itemconfigure $node -image [Bitmap::get [lindex {folder openfold} $idx]]
    }
}


#required by the Tree
proc Demo::init { tree list args } {
    global   tcl_platform
    variable count

    set count 0
    if { $tcl_platform(platform) == "unix" } {
        #set rootdir [glob "~"]
	set rootdir "/"; #added by me
    } else {
        set rootdir "c:\\"
    }
    $tree insert end root home -text $rootdir -data $rootdir -open 1 \
        -image [Bitmap::get openfold]
    getdir $tree home $rootdir
    #+++ Demo::select tree 1 $tree $list home
    $tree configure -redraw 1
    #$list configure -redraw 1

    # ScrollView
    #set w .top
    #toplevel $w
    #wm withdraw $w
    #wm protocol $w WM_DELETE_WINDOW {
        # don't kill me
    #}
    #wm resizable $w 0 0 
    #wm title $w "Drag rectangle to scroll directory tree"
    #wm transient $w .
    #ScrollView $w.sv -window $tree -fill white -relief sunken -bd 1 \
    #    -width 300 -height 300
    #pack $w.sv -fill both -expand yes
}


#required by the Tree
proc Demo::getdir { tree node path } {
    variable count

    set lentries [glob -nocomplain [file join $path "*"]]
    set lfiles   {}
    foreach f $lentries {
        set tail [file tail $f]
        if { [file isdirectory $f] } {
            $tree insert end $node n:$count \
                -text      $tail \
                -image     [Bitmap::get folder] \
                -drawcross allways \
                -data      $f
            incr count
        } else {
            lappend lfiles $tail
        }
    }
    $tree itemconfigure $node -drawcross auto -data $lfiles
}


#required by the Tree
proc Demo::select { where num tree list node } {
    variable dblclick

    set dblclick 1
    if { $num == 1 } {
	#+++ && [lsearch [$tree selection get] $node] != -1
        if { $where == "tree" } {
            unset dblclick
            after 500 "Demo::edit tree $tree $list $node"
            return
        }
        #+++ if { $where == "list" && [lsearch [$list selection get] $node] != -1 } {
        #    unset dblclick
        #    after 500 "Demo::edit list $tree $list $node"
        #    return
        #}
        if { $where == "tree" } {
            select_node $tree $list $node
        } else {
            #+++ $list selection set $node
        }
    } elseif { $where == "list" && [$tree exists $node] } {
	puts ""
	#+++ set parent [$tree parent $node]
	#while { $parent != "root" } {
	#    $tree itemconfigure $parent -open 1
	#    set parent [$tree parent $parent]
	#}
	#select_node $tree $list $node
    }
}


#required by the Tree
proc Demo::select_node { tree list node } {
    $tree selection set $node
    update
    #+++ eval $list delete [$list item 0 end]

    set dir [$tree itemcget $node -data]
    if { [$tree itemcget $node -drawcross] == "allways" } {
        getdir $tree $node $dir
        set dir [$tree itemcget $node -data]
    }

    foreach subnode [$tree nodes $node] {
    #+++    $list insert end $subnode \
    #        -text  [$tree itemcget $subnode -text] \
    #        -image [Bitmap::get folder]
    }
    set num 0
    foreach f $dir {
        #+++$list insert end f:$num \
        #    -text  $f \
        #    -image [Bitmap::get file]
        incr num
    }
}


#required by the Tree
proc Demo::edit { where tree list node } {
    variable dblclick

    if { [info exists dblclick] } {
        return
    }

    if { $where == "tree" && [lsearch [$tree selection get] $node] != -1 } {
        set res [$tree edit $node [$tree itemcget $node -text]]
        if { $res != "" } {
            $tree itemconfigure $node -text $res
            #+++ if { [$list exists $node] } {
            #    $list itemconfigure $node -text $res
            #}
            $tree selection set $node
        }
        return
    }

    #+++ if { $where == "list" } {
    #    set res [$list edit $node [$list itemcget $node -text]]
    #    if { $res != "" } {
    #        $list itemconfigure $node -text $res
    #        if { [$tree exists $node] } {
    #            $tree itemconfigure $node -text $res
    #        } else {
    #            set cursel [$tree selection get]
    #            set index  [expr {[$list index $node]-[llength [$tree nodes $cursel]]}]
    #            set data   [$tree itemcget $cursel -data]
    #            set data   [lreplace $data $index $index $res]
    #            $tree itemconfigure $cursel -data $data
    #        }
    #        $list selection set $node
    #    }
    #}
}


#required by the Tree
proc Demo::expand { tree but } {
    if { [set cur [$tree selection get]] != "" } {
        if { $but == 0 } {
            $tree opentree $cur
        } else {
            $tree closetree $cur
        }
    }
}




proc Demo::main {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "Tree demo"

    Demo::create
    BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    wm deiconify .
    raise .
    focus -force .
}

Demo::main
wm geom . [wm geom .]
