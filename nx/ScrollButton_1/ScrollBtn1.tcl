#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package require nx
package require widget
package require mainframe
package require scrolledwindow
package require scrollframe
package require button



#Hier wird ein Button auf einem frame gescrollt.



nx::Class create Demo {
    #variable status
    #variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set :mainwin [NXBwidget::MainFrame new]
    ${:mainwin} create .mainframe \
                       -menu         $descmenu \
                       -textvariable DemoGlob::status \
                       -progressvar  DemoGlob::prgindic
}


Demo public method create { } {
    :createMainwindow
    
    set frame    [${:mainwin} getframe [${:mainwin} retrievePath]]
    
    
    #create the ScrolledWindow. ScrollableFrame should be used, to make e.g. a scrollable list.
    set sw [NXBwidget::ScrolledWindow new]
    $sw create $frame.sw -relief sunken -borderwidth 2
    
    set sf [NXBwidget::ScrollableFrame new]
    $sf create  [$sw retrievePath].f
    $sw setwidget [$sw retrievePath] $sf
    set subf [$sf getframe [$sf retrievePath]]
    
    for {set i 0} {$i <= 15} {incr i} {
      #creates Labels as list items in the scrollable frame
      #set lab [label $subf.lab_${i} -text "Label_${i}"]
      #pack $lab -side top -padx 4 -pady 2
      
      if {$i > 0} {
        set but   [NXBwidget::Button new]
        $but create $subf.but_${i} \
                   -repeatdelay 300 \
                   -relief flat \
                   -state disabled \
                   -command  "::buttoncmd $i" \
                   -helptext "This is a Button widget"
      } else {
        #crates Buttons as list items
        set but   [NXBwidget::Button new]
        $but create $subf.but_${i} -text "Button ${i}" \
                   -repeatdelay 300 \
                   -command  "::buttoncmd $i" \
                   -helptext "This is a Button widget"
      }
      pack [$but retrievePath]  -side top -padx 4 -pady 2
    }
    
    pack [$sw retrievePath]  -side top  -expand yes -fill both
    
    
    #pack the mainframe
    pack [${:mainwin} retrievePath] -fill both -expand yes
    update idletasks
}



Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    #package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "ScrolledWindow demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}


proc buttoncmd {args} {
    puts "Button ${args} pressed!"
}


namespace eval DemoGlob {
    variable status
    variable prgindic
}


set dm [Demo new]
wm geom . [wm geom .]
