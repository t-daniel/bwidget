# ----------------------------------------------------------------------------
#  scrollframe.tcl
#  This file is part of Unifix BWidget Toolkit
#  $Id: scrollframe.tcl,v 1.11 2009/07/17 15:29:51 oehhar Exp $
# ----------------------------------------------------------------------------
#  Index of commands:
#     - ScrollableFrame::create
#     - ScrollableFrame::configure
#     - ScrollableFrame::cget
#     - ScrollableFrame::getframe
#     - ScrollableFrame::see
#     - ScrollableFrame::xview
#     - ScrollableFrame::yview
#     - ScrollableFrame::_resize
# ----------------------------------------------------------------------------
package provide scrollframe 1.0

package require nx
package require widget



namespace eval NXBwidget {

nx::Class create ScrollableFrame -superclass NXBwidget::Widget {
    
    #+++
    :public method initVars args {
	#initialize the classvariables of the superclasses + mixinclasses and the ones of self
	next
	
	set :_widget ""
    }
    
    
    #+++
    :public method init {} {
    	:initVars;  #+++
	
	:define ScrollableFrame scrollframe; #+++
	
	#+++
	if {[:theme]} {
          :declare ScrollableFrame {
            {-width             Int        0  0 {}}
            {-height            Int        0  0 {}}
            {-areawidth         Int        0  0 {}}
            {-areaheight        Int        0  0 {}}
            {-constrainedwidth  Boolean    0 0}
            {-constrainedheight Boolean    0 0}
            {-xscrollcommand    TkResource "" 0 canvas}
            {-yscrollcommand    TkResource "" 0 canvas}
            {-xscrollincrement  TkResource "" 0 canvas}
            {-yscrollincrement  TkResource "" 0 canvas}
          }
	} else {
	  :declare ScrollableFrame {
            {-background        TkResource "" 0 frame}
            {-width             Int        0  0 {}}
            {-height            Int        0  0 {}}
            {-areawidth         Int        0  0 {}}
            {-areaheight        Int        0  0 {}}
            {-constrainedwidth  Boolean    0 0}
            {-constrainedheight Boolean    0 0}
            {-xscrollcommand    TkResource "" 0 canvas}
            {-yscrollcommand    TkResource "" 0 canvas}
            {-xscrollincrement  TkResource "" 0 canvas}
            {-yscrollincrement  TkResource "" 0 canvas}
            {-bg                Synonym    -background}
	  }
	}
	
	#+++
	:addmap ScrollableFrame "" :cmd {
        -width {} -height {} 
        -xscrollcommand {} -yscrollcommand {}
        -xscrollincrement {} -yscrollincrement {}
	}
	if { ! [:theme]} {
          :addmap ScrollableFrame "" .frame {-background {}}
	}
	
	bind BwScrollableFrame <Configure> [list [self] _resize %W]
	bind BwScrollableFrame <Destroy>   [list [self] destroy %W]
    }
    
    
    #Widget::define ScrollableFrame scrollframe

    # If themed, there is no background and -bg option
    #if {[Widget::theme]} {
    #    Widget::declare ScrollableFrame {
    #        {-width             Int        0  0 {}}
    #        {-height            Int        0  0 {}}
    #        {-areawidth         Int        0  0 {}}
    #        {-areaheight        Int        0  0 {}}
    #        {-constrainedwidth  Boolean    0 0}
    #        {-constrainedheight Boolean    0 0}
    #        {-xscrollcommand    TkResource "" 0 canvas}
    #        {-yscrollcommand    TkResource "" 0 canvas}
    #        {-xscrollincrement  TkResource "" 0 canvas}
    #        {-yscrollincrement  TkResource "" 0 canvas}
    #    }
    #} else {
    #    Widget::declare ScrollableFrame {
    #        {-background        TkResource "" 0 frame}
    #        {-width             Int        0  0 {}}
    #        {-height            Int        0  0 {}}
    #        {-areawidth         Int        0  0 {}}
    #        {-areaheight        Int        0  0 {}}
    #        {-constrainedwidth  Boolean    0 0}
    #        {-constrainedheight Boolean    0 0}
    #        {-xscrollcommand    TkResource "" 0 canvas}
    #        {-yscrollcommand    TkResource "" 0 canvas}
    #        {-xscrollincrement  TkResource "" 0 canvas}
    #        {-yscrollincrement  TkResource "" 0 canvas}
    #        {-bg                Synonym    -background}
    #    }
    #}

    #Widget::addmap ScrollableFrame "" :cmd {
    #    -width {} -height {} 
    #    -xscrollcommand {} -yscrollcommand {}
    #    -xscrollincrement {} -yscrollincrement {}
    #}
    #if { ! [Widget::theme]} {
    #    Widget::addmap ScrollableFrame "" .frame {-background {}}
    #}

    #variable _widget

    #bind BwScrollableFrame <Configure> [list ScrollableFrame::_resize %W]
    #bind BwScrollableFrame <Destroy>   [list Widget::destroy %W]
}

}; #END - namespace



# ----------------------------------------------------------------------------
#  Command ScrollableFrame::create
# ----------------------------------------------------------------------------
NXBwidget::ScrollableFrame public method create { path args } {
    :storePath $path; #+++
    
    #+++ Widget::init ScrollableFrame $path $args
    :widget_init ScrollableFrame $path $args; #+++

    set canvas [eval [list canvas $path] [:subcget $path :cmd] \
                    -highlightthickness 0 -borderwidth 0 -relief flat]

    #+++
    if {[:theme]} {
	set frame [eval [list ttk::frame $path.frame] \
		       [:subcget $path .frame]]
	set bg [ttk::style lookup TFrame -background]
    } else {
	set frame [eval [list frame $path.frame] \
		       [:subcget $path .frame] \
		       -highlightthickness 0 -borderwidth 0 -relief flat]
	set bg [$frame cget -background]
    }
    # Give canvas frame (or theme) background
    $canvas configure -background $bg

    #+++
    $canvas create window 0 0 -anchor nw -window $frame -tags win \
        -width  [:cget $path -areawidth] \
        -height [:cget $path -areaheight]

    bind $frame <Configure> \
        [list [self] _frameConfigure $canvas]; #+++
    # add <unmap> binding: <configure> is not called when frame
    # becomes so small that it suddenly falls outside of currently visible area.
    # but now we need to add a <map> binding too
    bind $frame <Map> \
        [list [self] _frameConfigure $canvas]; #+++
    bind $frame <Unmap> \
        [list [self] _frameConfigure $canvas 1]; #+++

    bindtags $path [list $path BwScrollableFrame [winfo toplevel $path] all]

    return [next "ScrollableFrame $path"]; #+++
}


# ----------------------------------------------------------------------------
#  Command ScrollableFrame::configure
# ----------------------------------------------------------------------------
NXBwidget::ScrollableFrame public method configure { path args } {
    set res [:widget_configure $path [list $args]]; #+++
    set upd 0

    set modcw [:hasChanged $path -constrainedwidth cw]; #+++
    set modw  [:hasChanged $path -areawidth w]; #+++
    if { $modcw || (!$cw && $modw) } {
        if { $cw } {
            set w [winfo width $path]
        }
        set upd 1
    }

    set modch [:hasChanged $path -constrainedheight ch]; #+++
    set modh  [:hasChanged $path -areaheight h]; #+++
    if { $modch || (!$ch && $modh) } {
        if { $ch } {
            set h [winfo height $path]
        }
        set upd 1
    }

    if { $upd } {
        $path:cmd itemconfigure win -width $w -height $h
    }
    return $res
}


# ----------------------------------------------------------------------------
#  Command ScrollableFrame::cget
# ----------------------------------------------------------------------------
#+++ NXBwidget::ScrollableFrame public method cget { path option } {
#    return [Widget::cget $path $option]
#}


# ----------------------------------------------------------------------------
#  Command ScrollableFrame::getframe
# ----------------------------------------------------------------------------
NXBwidget::ScrollableFrame public method getframe { path } {
    return $path.frame
}

# ----------------------------------------------------------------------------
#  Command ScrollableFrame::see
# ----------------------------------------------------------------------------
NXBwidget::ScrollableFrame public method see { path widget {vert top} {horz left} {xOffset 0} {yOffset 0}} {
    set x0  [winfo x $widget]
    set y0  [winfo y $widget]
    set x1  [expr {$x0+[winfo width  $widget]}]
    set y1  [expr {$y0+[winfo height $widget]}]
    set xb0 [$path:cmd canvasx 0]
    set yb0 [$path:cmd canvasy 0]
    set xb1 [$path:cmd canvasx [winfo width  $path]]
    set yb1 [$path:cmd canvasy [winfo height $path]]
    set dx  0
    set dy  0
    
    if { [string equal $horz "left"] } {
	if { $x1 > $xb1 } {
	    set dx [expr {$x1-$xb1}]
	}
	if { $x0 < $xb0+$dx } {
	    set dx [expr {$x0-$xb0}]
	}
    } elseif { [string equal $horz "right"] } {
	if { $x0 < $xb0 } {
	    set dx [expr {$x0-$xb0}]
	}
	if { $x1 > $xb1+$dx } {
	    set dx [expr {$x1-$xb1}]
	}
    }

    if { [string equal $vert "top"] } {
	if { $y1 > $yb1 } {
	    set dy [expr {$y1-$yb1}]
	}
	if { $y0 < $yb0+$dy } {
	    set dy [expr {$y0-$yb0}]
	}
    } elseif { [string equal $vert "bottom"] } {
	if { $y0 < $yb0 } {
	    set dy [expr {$y0-$yb0}]
	}
	if { $y1 > $yb1+$dy } {
	    set dy [expr {$y1-$yb1}]
	}
    }

    if {($dx + $xOffset) != 0} {
	set x [expr {($xb0+$dx+$xOffset)/[winfo width $path.frame]}]
	$path:cmd xview moveto $x
    }
    if {($dy + $yOffset) != 0} {
	set y [expr {($yb0+$dy+$yOffset)/[winfo height $path.frame]}]
	$path:cmd yview moveto $y
    }
}


# ----------------------------------------------------------------------------
#  Command ScrollableFrame::xview
# ----------------------------------------------------------------------------
NXBwidget::ScrollableFrame public method xview { path args } {
    return [eval [list $path:cmd xview] $args]
}


# ----------------------------------------------------------------------------
#  Command ScrollableFrame::yview
# ----------------------------------------------------------------------------
#+++ wird von TK aufgerufen. Path ist der Pfad zu dem scrollableframe zugeordnet ist.
#+++ args ist "moveto Zahl".
#+++ yview wird in ScrolledWindow::setwidget gesetzt.
NXBwidget::ScrollableFrame public method yview { path args } {
    puts "\n-->ScrollableFrame::yview\n  path: $path\n  args: $args\n"
    return [eval [list $path:cmd yview] $args]
}


# ----------------------------------------------------------------------------
#  Command ScrollableFrame::_resize
# ----------------------------------------------------------------------------
NXBwidget::ScrollableFrame public method _resize { path } {
    #+++
    if { [:getoption $path -constrainedwidth] } {
        $path:cmd itemconfigure win -width [winfo width $path]
    }
    if { [:getoption $path -constrainedheight] } {
        $path:cmd itemconfigure win -height [winfo height $path]
    }
    # scollregion must also be reset when canvas size changes
    :_frameConfigure $path; #+++
}


# ----------------------------------------------------------------------------
#  Command ScrollableFrame::_frameConfigure
# ----------------------------------------------------------------------------
NXBwidget::ScrollableFrame public method _max {a b} {return [expr {$a <= $b ? $b : $a}]}

NXBwidget::ScrollableFrame public method _frameConfigure {canvas {unmap 0}} {
    # This ensures that we don't get funny scrollability in the frame
    # when it is smaller than the canvas space
    # use [winfo] to get height & width of frame

    # [winfo] doesn't work for unmapped frame
    set frameh [expr {$unmap ? 0 : [winfo height $canvas.frame]}]
    set framew [expr {$unmap ? 0 : [winfo width  $canvas.frame]}]

    set height [:_max $frameh [winfo height $canvas]]; #+++
    set width  [:_max $framew [winfo width  $canvas]]; #+++

    $canvas:cmd configure -scrollregion [list 0 0 $width $height]
}


#+++ (added by me)
NXBwidget::ScrollableFrame public method storePath {path} {
    set :wpath $path
}


#+++ (added by me)
NXBwidget::ScrollableFrame public method retrievePath {} {
    return ${:wpath}
}
