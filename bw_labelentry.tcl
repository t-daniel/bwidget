#!/usr/bin/wish
package require BWidget

wm withdraw .
wm title . "BWidget-LabelEntry"
wm deiconify .
raise .

set ent [LabelEntry .ent -label "Input"]; #das einfache LabelEntry

#das LabelEntry mit Zusatzfunktionalitaeten
#set ent [LabelEntry .ent -label "Input" \
#        -labelwidth 10 -labelanchor w \
#        -command  {puts  "The typed text: [LabelEntry::cget .ent -text]"} \
#        -helptext "The LabelEntry Widget" \
#        -show "*"]

pack $ent -pady 4 -fill x
