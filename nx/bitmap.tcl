# ------------------------------------------------------------------------------
#  bitmap.tcl
#  This file is part of Unifix BWidget Toolkit
#  $Id: bitmap.tcl,v 1.4 2003/10/20 21:23:52 damonc Exp $
# ------------------------------------------------------------------------------
#  Index of commands:
#     - Bitmap::get
#     - Bitmap::_init
# ----------------------------------------------------------------------------
package provide bitmap 1.0

package require nx
package require widget
package require xpmtoimage


namespace eval NXBwidget {

nx::Class create Bitmap -superclass NXBwidget::Widget {
    
    #+++
    :public method initVars args {
    	next
        array set :_bmp {}
        set :path ""
        set :_types {
            photo  .gif
            photo  .ppm
            bitmap .xbm
            photo  .xpm
        }
    }
    
    
    #+++ Widget::define Bitmap bitmap -classonly

    #+++ variable path
    #variable _bmp
    #variable _types {
    #    photo  .gif
    #    photo  .ppm
    #    bitmap .xbm
    #    photo  .xpm
    #}

    #+++ proc use {} {}
    
    
    #+++
    :public method init {} {
	:initVars;  #+++
        
        :define Bitmap bitmap -classonly
    }


}; #END - class

}; #END - Namespace



#+++ added by me
NXBwidget::Bitmap public method create {dir} {
    :_init $dir
}



# ----------------------------------------------------------------------------
#  Command Bitmap::get
# ----------------------------------------------------------------------------
NXBwidget::Bitmap public method get { name } {
    #+++
    #variable path
    #variable _bmp
    #variable _types
    
    upvar 0 :path path
    upvar 0 :_bmp _bmp
    upvar 0 :_types _types
    #+++

    if {[info exists _bmp($name)]} {
        puts "\nRET1: $_bmp($name)\n"
        return $_bmp($name)
    }

    # --- Nom de fichier avec extension ---------------------------------
    set ext [file extension $name]
    if { $ext != "" } {
        if { ![info exists _bmp($ext)] } {
            error "$ext not supported"
        }

        if { [file exists $name] } {
            if {[string equal $ext ".xpm"]} {
                set _bmp($name) [xpm-to-image $name]
                puts "\nRET2: $_bmp($name)\n"
                return $_bmp($name)
            }
            if {![catch {set _bmp($name) [image create $_bmp($ext) -file $name]}]} {
                puts "\nRET3: $_bmp($name)\n"
                return $_bmp($name)
            }
        }
    }

    foreach dir $path {
        foreach {type ext} $_types {
            if { [file exists [file join $dir $name$ext]] } {
                if {[string equal $ext ".xpm"]} {
                    set _bmp($name) [xpm-to-image [file join $dir $name$ext]]
                    puts "\nRET4: $_bmp($name)\n"
                    return $_bmp($name)
                } else {
                    if {![catch {set _bmp($name) [image create $type -file [file join $dir $name$ext]]}]} {
                        puts "\nRET5: $_bmp($name)  Type: $type\n"
                        return $_bmp($name)
                    }
                }
            }
        }
    }

    return -code error "$name not found"
}


# ----------------------------------------------------------------------------
#  Command Bitmap::_init
# ----------------------------------------------------------------------------
NXBwidget::Bitmap public method _init { dir } {
    global   env
    #+++
    #variable path
    #variable _bmp
    #variable _types
    
    upvar 0 :path path
    upvar 0 :_bmp _bmp
    upvar 0 :_types _types
    #+++
    

    #+++ set path [list "." [file join $::BWIDGET::LIBRARY images]]
    set path [list "." [file join $dir]]; #+++
    set supp [image types]
    foreach {type ext} $_types {
        if { [lsearch $supp $type] != -1} {
            set _bmp($ext) $type
        }
    }
    
    puts "\nBitmap::_init\n  _bmp: [array get _bmp]\n  Path: $path\n  _types: $_types\n"
}


#+++ Bitmap::_init
