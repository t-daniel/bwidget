#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide mytitleframe 1.0

package require nx
package require bwidget
package require titleframe



nx::Class create Demo {
    #set :status ""
    #set :prgindic ""
    set :mainwin ""
    
    set :pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    puts "\nNEW MainFrame\n"
    set :mainwin [NXBwidget::MainFrame new]
    
    nsf::var::set ${:mainwin} status "";  #variable Demo::status moved here
    nsf::var::set ${:mainwin} prgindic "";  #variable Demo::prgindic moved here
    
    puts "\nCREATE MainFrame\n"
    ${:mainwin} create .mainframe \
                       -menu         $descmenu \
                       -textvariable :status \
                       -progressvar  :prgindic
}


Demo public method create { } {
    #:createMainwindow; #wird nicht gebraucht
    
    #set frame    [${Demo::mainwin} getframe]
    
    
    #create the TitleFrame
    set topf  [frame .topf]
    set titf1 [NXBwidget::TitleFrame new]
    #$titf1 configure;  #muss nicht aufgerufen werden
    $titf1 create $topf.titf1 -text "This is the Titleframe widget"
    puts "\n-->MAIN - after create\n"
    
    #pack the Titleframe
    pack [$titf1 retrievePath] -side left -fill both -padx 4 -expand yes
    pack $topf -pady 2 -fill x
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    update idletasks
}


Demo public method init args {
    variable DEMODIR
    puts "\nIN MAIN"

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    puts "  DEMODIR: $DEMODIR"

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm geometry . 300x200
    wm title . "TitleFrame demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    #${:mainwin} place . 300 300 right
    
    wm deiconify .
    raise .
    focus -force .
}

set demo [Demo new]

#Demo::main;  abgeloest durch Demo::init
wm geom . [wm geom .]
