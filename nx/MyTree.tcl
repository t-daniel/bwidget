#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide mytree 1.0

package require tree
package require mainframe
package require scrolledwindow



nx::Class create Demo -superclass NXBwidget::Widget {
    #variable status
    #variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set :mainwin [NXBwidget::MainFrame new]
    ${:mainwin} create  .mainframe \
                       -menu         $descmenu \
                       -textvariable ::status \
                       -progressvar  ::prgindic
}


Demo public method create { } {
    :createMainwindow
    
    set frame    [${:mainwin} getframe [${:mainwin} retrievePath]]
    
    
    #create the Tree
    #ScrolledWindow is required to make a scrollable tree
    set sw    [NXBwidget::ScrolledWindow new]
    $sw create  $frame.sw \
                  -relief sunken -borderwidth 2
    
    set tree  [NXBwidget::Tree new]
    $tree create  [$sw retrievePath].tree \
                   -relief flat -borderwidth 0 -width 15 -highlightthickness 0\
		   -redraw 0 -dropenabled 1 -dragenabled 1 \
                   -dragevent 3 \
                   -droptypes {
                       TREE_NODE    {copy {} move {} link {}}
                       LISTBOX_ITEM {copy {} move {} link {}}
                   } \
                   -opencmd   "::moddir 1 $tree" \
                   -closecmd  "::moddir 0 $tree"
		   
		   #-opencmd   "::moddir 1 .tree"
                   #-closecmd  "::moddir 0 .tree"
    
    $sw setwidget [$sw retrievePath] $tree
    pack [$sw retrievePath]    -side top  -expand yes -fill both
    
    
    #$list is required by the functions below (Demo::)
    #Listbox could be used to display e.g the files in a directory, that was
    #selcted in the Tree.
    
    set list [listbox .choose -height 0 -selectmode multiple]; #just an empty listbox
    
    #set list [ListBox::create $sw.lb \
    #              -relief flat -borderwidth 0 \
    #              -dragevent 3 \
    #              -dropenabled 1 -dragenabled 1 \
    #              -width 20 -highlightthickness 0 -multicolumn true \
    #              -redraw 0 -dragenabled 1 \
    #              -droptypes {
    #                  TREE_NODE    {copy {} move {} link {}}
    #                  LISTBOX_ITEM {copy {} move {} link {}}}]


    $tree bindText  [$tree retrievePath]  <ButtonPress-1>        "::select tree 1 $tree $list"
    $tree bindText  [$tree retrievePath]  <Double-ButtonPress-1> "::select tree 2 $tree $list"
    #$list bindText  <ButtonPress-1>        "Demo::select list 1 $tree $list"
    #$list bindText  <Double-ButtonPress-1> "Demo::select list 2 $tree $list"
    #$list bindImage <Double-ButtonPress-1> "Demo::select list 2 $tree $list"

    
    ::tree_init $tree $list
    
    
    #pack the mainframe
    pack [${:mainwin} retrievePath] -fill both -expand yes
    ${:mainwin} place . 300 250 right
    update idletasks
}


Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    #package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "Tree demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}



#required by the Tree (see demo/tree.tcl)
proc moddir { idx tree node } {
    set __bmp [NXBwidget::Bitmap new]
    $__bmp create [nsf::var::set $__bmp BWIDGET_LIBRARY]/images
    
    if { $idx && [$tree itemcget [$tree retrievePath] $node -drawcross] == "allways" } {
        getdir $tree $node [$tree itemcget [$tree retrievePath] $node -data]
        if { [llength [$tree nodes [$tree retrievePath] $node]] } {
            $tree itemconfigure [$tree retrievePath] $node -image [$__bmp get openfold]
        } else {
            $tree itemconfigure [$tree retrievePath] $node -image [$__bmp get folder]
        }
    } else {
        $tree itemconfigure [$tree retrievePath] $node -image [$__bmp get [lindex {folder openfold} $idx]]
    }
}


#required by the Tree
#tree: classinstance of Tree
proc tree_init { tree list } {
    global   tcl_platform
    variable count
    
    set __bmp [NXBwidget::Bitmap new]
    $__bmp create [nsf::var::set $__bmp BWIDGET_LIBRARY]/images

    set count 0
    if { $tcl_platform(platform) == "unix" } {
        #set rootdir [glob "~"]
	set rootdir "/"; #added by me
    } else {
        set rootdir "c:\\"
    }
    $tree insert  [$tree retrievePath]  end root home -text $rootdir -data $rootdir -open 1 \
        -image [$__bmp get openfold]
    ::getdir $tree home $rootdir
    ::select tree 1 $tree $list home
    $tree configure [$tree retrievePath] -redraw 1
    #$list configure -redraw 1

    # ScrollView
    #set w .top
    #toplevel $w
    #wm withdraw $w
    #wm protocol $w WM_DELETE_WINDOW {
        # don't kill me
    #}
    #wm resizable $w 0 0 
    #wm title $w "Drag rectangle to scroll directory tree"
    #wm transient $w .
    #ScrollView $w.sv -window $tree -fill white -relief sunken -bd 1 \
    #    -width 300 -height 300
    #pack $w.sv -fill both -expand yes
}


#required by the Tree
proc getdir { tree node path } {
    variable count
    
    set __bmp [NXBwidget::Bitmap new]
    $__bmp create [nsf::var::set $__bmp BWIDGET_LIBRARY]/images

    set lentries [glob -nocomplain [file join $path "*"]]
    set lfiles   {}
    foreach f $lentries {
        set tail [file tail $f]
        if { [file isdirectory $f] } {
            $tree insert  [$tree retrievePath]  end $node n:$count \
                -text      $tail \
                -image     [$__bmp get folder] \
                -drawcross allways \
                -data      $f
            incr count
        } else {
            lappend lfiles $tail
        }
    }
    $tree itemconfigure  [$tree retrievePath]  $node -drawcross auto -data $lfiles
}


#required by the Tree
proc select { where num tree list node } {
    variable dblclick

    set dblclick 1
    if { $num == 1 } {
	# && [lsearch [$tree selection get] $node] != -1
        if { $where == "tree" } {
            unset dblclick
            after 500 "::edit tree $tree $list $node"
            return
        }
        #if { $where == "list" && [lsearch [$list selection get] $node] != -1 } {
        #    unset dblclick
        #    after 500 "Demo::edit list $tree $list $node"
        #    return
        #}
        if { $where == "tree" } {
            ::select_node $tree $list $node
        } else {
            $list selection set $node
        }
    } elseif { $where == "list" && [$tree exists $node] } {
	#set parent [$tree parent $node]
	#while { $parent != "root" } {
	#    $tree itemconfigure $parent -open 1
	#    set parent [$tree parent $parent]
	#}
	::select_node $tree $list $node
    }
}


#required by the Tree
proc select_node { tree list node } {
    $tree selection [$tree retrievePath] set $node
    update
    #eval $list delete [$list item 0 end]

    set dir [$tree itemcget [$tree retrievePath] $node -data]
    if { [$tree itemcget [$tree retrievePath] $node -drawcross] == "allways" } {
        ::getdir $tree $node $dir
        set dir [$tree itemcget [$tree retrievePath] $node -data]
    }

    foreach subnode [$tree nodes [$tree retrievePath] $node] {
        #$list insert end $subnode \
        #    -text  [$tree itemcget $subnode -text] \
        #    -image [Bitmap::get folder]
    }
    set num 0
    foreach f $dir {
        #$list insert end f:$num \
        #    -text  $f \
        #    -image [Bitmap::get file]
        incr num
    }
}


#required by the Tree
proc edit { where tree list node } {
    variable dblclick

    if { [info exists dblclick] } {
        return
    }

    if { $where == "tree" && [lsearch [$tree selection [$tree retrievePath] get] $node] != -1 } {
        set res [$tree edit [$tree retrievePath] $node [$tree itemcget [$tree retrievePath] $node -text]]
        if { $res != "" } {
            $tree itemconfigure [$tree retrievePath] $node -text $res
            #if { [$list exists $node] } {
            #    $list itemconfigure $node -text $res
            #}
            $tree selection [$tree retrievePath] set $node
        }
        return
    }

    #if { $where == "list" } {
    #    set res [$list edit $node [$list itemcget $node -text]]
    #    if { $res != "" } {
    #        $list itemconfigure $node -text $res
    #        if { [$tree exists $node] } {
    #            $tree itemconfigure $node -text $res
    #        } else {
    #            set cursel [$tree selection get]
    #            set index  [expr {[$list index $node]-[llength [$tree nodes $cursel]]}]
    #            set data   [$tree itemcget $cursel -data]
    #            set data   [lreplace $data $index $index $res]
    #            $tree itemconfigure $cursel -data $data
    #        }
    #        $list selection set $node
    #    }
    #}
}


#required by the Tree
proc expand { tree but } {
    if { [set cur [$tree selection [$tree retrievePath] get]] != "" } {
        if { $but == 0 } {
            $tree opentree [$tree retrievePath] $cur
        } else {
            $tree closetree [$tree retrievePath] $cur
        }
    }
}



set dm [Demo new]
wm geom . [wm geom .]
