#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide myselectfont 1.0

package require nx
package require widget
package require selectfont



nx::Class create Demo {
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [NXBwidget::MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable ::status \
                       -progressvar  ::prgindic]
}


Demo public method create { } {
    #Demo::createMainwindow
    
    #set frame    [${Demo::mainwin} getframe]
    
    set frame [frame .topf]
    pack $frame -pady 2 -fill x
    
    
    #create the SelectFont Widget
    set but1  [button $frame.but1 \
                   -text    "SelectFont Widget" \
                   -command ::_show_fontdlg]
    pack $but1 -pady 2 -fill x
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    update idletasks
}


Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "SelectFont demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

proc _show_fontdlg { } {
    set font [NXBwidget::SelectFont new]
    $font create  .fontdlg -parent .
    if { $font != "" } {
        puts "\nThe selected font: [nsf::var::set $font __selectedFont]\n"
    }
}


set dm [Demo new]
wm geom . [wm geom .]
