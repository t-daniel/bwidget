#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package require nx
package require widget
package require mainframe
package require scrolledwindow
package require scrollframe
package require button
package require label
package require buttonscroll
package require addonscrframe


#Hier wird das Text auf einem Button gescrollt.


nx::Class create Demo {
    #variable status
    #variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set :mainwin [NXBwidget::MainFrame new]
    ${:mainwin} create .mainframe \
                       -menu         $descmenu \
                       -textvariable DemoGlob::status \
                       -progressvar  DemoGlob::prgindic
}


Demo public method create { } {
    :createMainwindow
    
    set mainframe    [${:mainwin} getframe [${:mainwin} retrievePath]]
    
    #add a Button, that can scroll its text
    set scrbtn [NXBwidget::Button new -object-mixin NXBwidget::ButtonScroll]
    $scrbtn create $mainframe.scrbtn \
                   -command  "::buttoncmd ScrButton" \
                   -helptext "This is a Scrollable Button widget"
    DemoGlob::setFont $scrbtn
    grid [$scrbtn retrievePath] -row 0 -column 0
    
    
    #create the ScrolledWindow. ScrollableFrame should be used, to make e.g. a scrollable list.
    set sw [NXBwidget::ScrolledWindow new]
    $sw create $mainframe.sw -relief flat -borderwidth 0
    
    set sf [NXBwidget::ScrollableFrame new -object-mixin NXBwidget::AddonScrollableFrame]
    $sf create  [$sw retrievePath].f -width 15
    $sw setwidget [$sw retrievePath] $sf
    set subf [$sf getframe [$sf retrievePath]]
    
    for {set i 0} {$i <= 15} {incr i} {
      #crates Labels as list items
      set lab   [NXBwidget::Label new]
      $lab create $subf.lab_${i} -text ""
      pack [$lab retrievePath]  -side top -padx 4 -pady 2
    }
    
    #add the Button to the ScrollableFrame
    $sf registerWidget $scrbtn
    
    grid [$sw retrievePath] -row 0 -column 1
    
    
    #pack the mainframe
    pack [${:mainwin} retrievePath] -fill both -expand yes
    update idletasks
}



Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    #package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "ScrolledWindow demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}


proc buttoncmd {args} {
    puts "Button ${args} pressed!"
}


namespace eval DemoGlob {
    variable status
    variable prgindic

    proc setFont { btn } {
	set cfont [list Times 20]
	canvas .can
	.can create text 10 20 -anchor w -font $cfont \
	  -text "Press Me!"
	
	.can postscript -width 95 -height 15 -x 1 -y 20  -file out.ps
	#The Img package (http://sourceforge.net/projects/tkimg/?source=typ_redirect)
	#can convert a postscript to xbm as well.
	
	#On Windows,specifiy the full path of convert
	#set cmd "/cygdrive/c/ImageMagick-6.8.9-Q16/convert"
	#exec $cmd out.ps out.xbm
	
	exec convert out.ps out.xbm

	image create bitmap .img -file out.xbm
	$btn configure [$btn retrievePath] -image .img
    
	destroy .can
	destroy .img
    }

}


set dm [Demo new]
wm geom . [wm geom .]
