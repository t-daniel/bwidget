#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}



namespace eval Demo {
    variable status
    #variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


proc Demo::createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


proc Demo::create { } {
    #Demo::createMainwindow
    
    #set frame    [${Demo::mainwin} getframe]
    
    set frame [frame .topf]
    pack $frame -pady 2 -fill x
    
    
    #create a Button
    set but   [Button $frame.but -text "Click it!" \
                   -command  "Demo::buttoncmd \"This is the Statusbar...\"" \
                   -helptext "This is a Button widget"]
    pack $but  -side top -padx 4
    
    
    #create the Statusbar
    set sbar $frame.s
    StatusBar $sbar
    pack $sbar -side bottom -fill x
    set f [$sbar getframe]

    set w [label $f.status -width 1 -anchor w -textvariable Demo::status]
    $sbar add $w -weight 1
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    update idletasks
}


proc Demo::buttoncmd {text} {
    set Demo::status   "$text"
    update
    after 2300 "Demo::buttoncmd \"\""
}



proc Demo::main {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "StatusBar demo"

    Demo::create
    BWidget::place . 190 100 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    wm deiconify .
    raise .
    focus -force .
}

Demo::main
wm geom . [wm geom .]
