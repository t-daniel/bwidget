#!/usr/bin/wish

wm withdraw .
wm title . "Tk GUI"

wm deiconify .
raise .

listbox .lstbox -height 0 -selectmode single
for {set i 1} {$i <= 4} {incr i} {
       .lstbox insert end "Item_${i}"
       pack .lstbox -side top -padx 4 -pady 2
}

button .btnok -text "ok" -command exit
pack .lstbox .btnok -expand true -fill both

