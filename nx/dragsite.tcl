# ------------------------------------------------------------------------------
#  dragsite.tcl
#  This file is part of Unifix BWidget Toolkit
#  $Id: dragsite.tcl,v 1.8 2003/10/20 21:23:52 damonc Exp $
# ------------------------------------------------------------------------------
#  Index of commands:
#     - DragSite::include
#     - DragSite::setdrag
#     - DragSite::register
#     - DragSite::_begin_drag
#     - DragSite::_init_drag
#     - DragSite::_end_drag
#     - DragSite::_update_operation
# ----------------------------------------------------------------------------
package provide dragsite 1.0

package require nx
package require widget
package require bitmap



namespace eval NXBwidget {

nx::Class create DragSite -superclass NXBwidget::Widget {
    
    #+++
    :public method dragsite_init {} {

	:define DragSite dragsite -classonly; #+++

	#+++
	:declare DragSite [list \
	        [list	-dragevent	Enum	1	0	[list 1 2 3]] \
	        [list	-draginitcmd	String	""	0] \
	        [list	-dragendcmd	String	""	0] \
	        ]

	set :_drag_top ".drag"
	set :dr_tabops "";  #+++ _tabops renamed to dr_tabops (dropsite has also _tabops)
	set :_state ""
	set :_x0 ""
	set :_y0 ""

	#+++
	bind BwDrag1 <ButtonPress-1> "[self] _begin_drag press  %W %s %X %Y"
	bind BwDrag1 <B1-Motion>     "[self] _begin_drag motion %W %s %X %Y"
	bind BwDrag2 <ButtonPress-2> "[self] _begin_drag press  %W %s %X %Y"
	bind BwDrag2 <B2-Motion>     "[self] _begin_drag motion %W %s %X %Y"
	bind BwDrag3 <ButtonPress-3> "[self] _begin_drag press  %W %s %X %Y"
	bind BwDrag3 <B3-Motion>     "[self] _begin_drag motion %W %s %X %Y"
	#+++

	#+++ proc use {} {}
    }
}

}; #END - namespace


# ----------------------------------------------------------------------------
#  Command DragSite::include
# ----------------------------------------------------------------------------
#+++ Class Dropsite has also an include
NXBwidget::DragSite public method drag_include { class type event } {
    puts "\n-->DragSite::drag_include:\n  class: $class\n  type: $type\n  event: $event\n"
    
    set dragoptions [list \
	    [list	-dragenabled	Boolean	0	0] \
	    [list	-draginitcmd	String	""	0] \
	    [list	-dragendcmd	String	""	0] \
	    [list	-dragtype	String	$type	0] \
	    [list	-dragevent	Enum	$event	0	[list 1 2 3]] \
	    ]
    :declare $class $dragoptions; #+++
}


# ----------------------------------------------------------------------------
#  Command DragSite::setdrag
#  Widget interface to register
# ----------------------------------------------------------------------------
NXBwidget::DragSite public method setdrag { path subpath initcmd endcmd {force 0}} {
    puts "\n-->DragSite::setdrag:\n  path: $path\n  subpath: $subpath\n  initcmd: $initcmd\n  endcmd: $endcmd\n"
    
    set cen       [:hasChanged $path -dragenabled en]; #+++
    set cdragevt  [:hasChanged $path -dragevent   dragevt]; #+++
    if { $en } {
        if { $force || $cen || $cdragevt } {
            :drag_register $subpath \
                -draginitcmd $initcmd \
                -dragendcmd  $endcmd  \
                -dragevent   $dragevt
        }
    } else {
        :drag_register $subpath
    }
}


# ----------------------------------------------------------------------------
#  Command DragSite::register
# ----------------------------------------------------------------------------
#+++ register renamed to drag_register
NXBwidget::DragSite public method drag_register { path args } {
    upvar \#0 :dr_$path drag; #+++ dropsite has also a :$path classvariable

    if { [info exists drag] } {
        bind $path $drag(evt) {}
        unset drag
    }
    
    puts "\n-->DragSite::drag_register:\n  path: $path\n  args: $args\n"
    
    :widget_init DragSite .drag$path $args; #+++
    set event   [:getMegawidgetOption .drag$path -dragevent]; #+++
    set initcmd [:getMegawidgetOption .drag$path -draginitcmd]
    set endcmd  [:getMegawidgetOption .drag$path -dragendcmd]
    set tags    [bindtags $path]
    set idx     [lsearch $tags "BwDrag*"]
    :destroy .drag$path; #+++
    if { $initcmd != "" } {
        if { $idx != -1 } {
            bindtags $path [lreplace $tags $idx $idx BwDrag$event]
        } else {
            bindtags $path [concat $tags BwDrag$event]
        }
        set drag(initcmd) $initcmd
        set drag(endcmd)  $endcmd
        set drag(evt)     $event
    } elseif { $idx != -1 } {
        bindtags $path [lreplace $tags $idx $idx]
    }
}


# ----------------------------------------------------------------------------
#  Command DragSite::_begin_drag
# ----------------------------------------------------------------------------
NXBwidget::DragSite public method _begin_drag { event source state X Y } {
    #+++ variable _x0
    #variable _y0
    #variable _state
    
    upvar 0 :_x0 _x0
    upvar 0 :_y0 _y0
    upvar 0 :_state _state
    #+++
    
    puts "\n-->DragSite::_begin_drag:\n  event: $event\n  source: $source\n"

    switch -- $event {
        press {
            set _x0    $X
            set _y0    $Y
            set _state "press"
        }
        motion {
            if { ![info exists _state] } {
                # This is just extra protection. There seem to be
                # rare cases where the motion comes before the press.
                return
            }
            if { [string equal $_state "press"] } {
                if { abs($_x0-$X) > 3 || abs($_y0-$Y) > 3 } {
                    set _state "done"
                    :drag_init_drag $source $state $X $Y; #+++
                }
            }
        }
    }
}


# ----------------------------------------------------------------------------
#  Command DragSite::_init_drag
# ----------------------------------------------------------------------------
#+++ _init_drag renamed to drag_init_drag (class Dropsite has also an _init_drag)
NXBwidget::DragSite public method drag_init_drag { source state X Y } {
    #+++ variable _topw
    upvar 0 :_drag_top _topw; #+++
    
    upvar \#0 :dr_$source drag; #+++ :$source renamed :dr_$source
    
    puts "\n-->DragSite::drag_init_drag\n  source: $source\n  \
         _topw: $_topw\n  drag: [array get drag]\n"

    destroy  $_topw
    toplevel $_topw
    wm withdraw $_topw
    wm overrideredirect $_topw 1

    set info [uplevel \#0 $drag(initcmd) [list $source $X $Y .drag]]
    if { $info != "" } {
        set type [lindex $info 0]
        set ops  [lindex $info 1]
        set data [lindex $info 2]

        if { [winfo children $_topw] == "" } {
            if { [string equal $type "BITMAP"] || [string equal $type "IMAGE"] } {
                #+++ label $_topw.l -image [Bitmap::get dragicon] -relief flat -bd 0
		
		#+++
		set mybmp [NXBwidget::Bitmap new]
		$mybmp create "[pwd]/images"
		label $_topw.l -image [$mybmp get dragicon] -relief flat -bd 0
		#+++
		
            } else {
                #+++ label $_topw.l -image [Bitmap::get dragfile] -relief flat -bd 0
		
		#+++
		set mybmp [NXBwidget::Bitmap new]
		$mybmp create "[pwd]/images"
		label $_topw.l -image [$mybmp get dragfile] -relief flat -bd 0
		#+++
            }
            pack  $_topw.l
        }
        wm geometry $_topw +[expr {$X+1}]+[expr {$Y+1}]
        wm deiconify $_topw
        if {[catch {tkwait visibility $_topw}]} {
            return
        }
	#+++
        #BWidget::grab  set $_topw
        #BWidget::focus set $_topw
	
	:grab  set $_topw
        :focus set $_topw
	#+++

        bindtags $_topw [list $_topw DragTop]
	
	#+++ this will call DropSite::_init_drag !
        :_init_drag $_topw $drag(evt) $source $state $X $Y $type $ops $data
    } else {
        destroy $_topw
    }
}


# ----------------------------------------------------------------------------
#  Command DragSite::_end_drag
# ----------------------------------------------------------------------------
NXBwidget::DragSite public method _end_drag { source target op type data result } {
    #+++ variable _topw
    upvar 0 :_drag_top _topw; #+++
    upvar \#0 :dr_$source drag; #+++
    
    puts "\n-->DragSite::_end_drag\n  source: $source\n  \
         target: $target\n  _topw: $_topw\n  drag: [array get drag]\n"

    :grab  release $_topw; #+++
    :focus release $_topw; #+++
    destroy $_topw
    if { $drag(endcmd) != "" } {
        uplevel \#0 $drag(endcmd) [list $source $target $op $type $data $result]
    }
}
