#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide mybitmap 1.0

package require nx
package require bwidget
package require bitmap



nx::Class create Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    #variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


Demo public method create { } {
    #Demo::createMainwindow
    
    #set frame    [${Demo::mainwin} getframe]
    
    set topf [frame .topf]
    pack $topf -pady 2 -fill x
    
    
    #create the left side
    #set pw    [PanedWindow $frame.pw -side top]
    #set pane  [$pw add -weight 1]
    
    
    #create the image (using img/info.gif)
    set bmp [NXBwidget::Bitmap new]
    $bmp create "${:DEMODIR}/img"
    set bgimage [$bmp get info]
    
    
    set img1  [label $topf.x  -image ${bgimage} \
	    -foreground grey90 -background white]
    pack $img1 -side top -padx 50 -pady 50
    
    
    #create the right side
    #set pane [$pw add -weight 2]
    #set img2 [label $pane.bw -bitmap @${Demo::DEMODIR}/demo/bwidget.xbm \
	#    -foreground grey90 -background white]
    #place $img2 -relx 1 -rely 1 -anchor se;  #place a bitmap over another one
    #pack $img2 -side top
    
    
    #pack the PanedWindow
    #pack $pw  -fill both  -expand yes
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    update idletasks
}



Demo public method init args {
    set :DEMODIR [pwd]

    lappend ::auto_path [file dirname ${:DEMODIR}]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm geometry . 300x200
    wm title . "Bitmap demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

set dm [Demo new]
wm geom . [wm geom .]
