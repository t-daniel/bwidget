#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide mylabelframe 1.0

package require nx
package require widget
package require labelframe
package require button



nx::Class create Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


Demo public method create { } {
    #Demo::createMainwindow
    
    #set frame    [${Demo::mainwin} getframe]
    
    set frame [frame .topf]
    pack $frame -pady 2 -fill x
    
    
    #create two LabelFrames
    set labf1 [NXBwidget::LabelFrame new]
    $labf1 create $frame.labf1 -text "First frame" -side top \
                   -anchor w -relief sunken -borderwidth 1
                   
    set labf2 [NXBwidget::LabelFrame new]
    $labf2 create $frame.labf2 -text "Second frame" -side top \
                   -anchor w -relief sunken -borderwidth 1
                   
    pack [$labf1 retrievePath] [$labf2 retrievePath] -side left -padx 4 -anchor n
    
    
    #fill the two LabelFrames
    set subf  [$labf1 getframe [$labf1 retrievePath]]
    set but1   [NXBwidget::Button new]
    $but1 create $subf.but1 -text "Button_1" \
                   -repeatdelay 300 \
                   -command  {puts "Button_1 pressed."} \
                   -helptext "This is a Button widget"
    pack [$but1 retrievePath]  -side left -padx 4 -pady 4
                   
    set subf  [$labf2 getframe [$labf2 retrievePath]]
    set but2   [NXBwidget::Button new]
    $but2 create $subf.but2 -text "Button_2" \
                   -repeatdelay 300 \
                   -command  {puts "Button_2 pressed."} \
                   -helptext "This is a Button widget"
    pack [$but2 retrievePath]  -side left -padx 4 -pady 4
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    update idletasks
}


Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "LabelFrame demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

set dm [Demo new]
wm geom . [wm geom .]
