#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}



namespace eval Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


proc Demo::createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


proc Demo::create { } {
    Demo::createMainwindow
    
    
    set frame    [${Demo::mainwin} getframe]
    
    
    #create the image (using images/info.gif)
    set bgimg [Bitmap::get info]
    
    
    #create the left side
    #set pw    [PanedWindow $frame.pw -side top]
    #set pane  [$pw add -weight 1]
    
    
    set img1  [label $frame.x -image ${bgimg} \
	    -foreground grey90 -background white]
    pack $img1 -side top -padx 50 -pady 50
    
    
    #create the right side
    #set pane [$pw add -weight 2]
    #set img2 [label $pane.bw -image ${bgimg} \
	#    -foreground grey90 -background white]
    #place $img2 -relx 1 -rely 1 -anchor se;  #place a bitmap over another one
    #pack $img2 -side top
    
    
    #pack the PanedWindow
    #pack $pw  -fill both  -expand yes
    
    
    #pack the mainframe
    pack ${Demo::mainwin} -fill both -expand yes
    update idletasks
}



proc Demo::main {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "Bitmap demo"

    Demo::create
    BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    wm deiconify .
    raise .
    focus -force .
}

Demo::main
wm geom . [wm geom .]
