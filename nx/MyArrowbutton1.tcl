#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide myarrowbutton1 1.0

package require nx
package require bwidget
package require arrowbutton



nx::Class create Demo {
    variable status
    variable prgindic
    variable var
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


Demo public method create { } {
    #Demo::createMainwindow
    
    #set frame    [${Demo::mainwin} getframe]
    
    set frame [frame .topf]
    pack $frame -pady 2 -fill x
    
    
    
    #create the ArrowButton (of type button)
    set arr1  [NXBwidget::ArrowButton new]
    $arr1 create $frame.arr1 -type button \
                   -width 25 -height 25 \
                   -repeatdelay 300 \
                   -command  "DemoGlob::arrowbutcmd 1" \
                   -helptext "This is an ArrowButton widget\nof type button"
    pack [$arr1 retrievePath]  -side left -padx 4
    $arr1 place . 70 50 right
    
    $arr1 place . 150 40 right
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    
    update idletasks
}


Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "ArrowButton demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

namespace eval DemoGlob {
    proc buttoncmd {args} {
        puts "Button pressed!"
    }

    proc arrowbutcmd {args} {
        puts "ArrowButton $args pressed!"
    }
}

set dm [Demo new]
wm geom . [wm geom .]
