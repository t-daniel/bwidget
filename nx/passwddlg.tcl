# -----------------------------------------------------------------------------
#  passwddlg.tcl
#  This file is part of Unifix BWidget Toolkit
#   by Stephane Lavirotte (Stephane.Lavirotte@sophia.inria.fr)
#  $Id: passwddlg.tcl,v 1.12 2009/06/11 15:42:51 oehhar Exp $
# -----------------------------------------------------------------------------
#  Index of commands:
#     - PasswdDlg::create
#     - PasswdDlg::configure
#     - PasswdDlg::cget
#     - PasswdDlg::_verifonlogin
#     - PasswdDlg::_verifonpasswd
#     - PasswdDlg::_max
#------------------------------------------------------------------------------
package provide passworddialog 1.0

package require nx
package require widget
package require labelentry
package require dialog



namespace eval NXBwidget {

nx::Class create PasswdDlg -superclass NXBwidget::Widget {
    
    #+++
    :public method initVars args {
	#initialize the classvariables of the superclasses + mixinclasses and the ones of self
	next
	
	set :_inst_Dialog [NXBwidget::Dialog new]
	set :_inst_LabelEntry [NXBwidget::LabelEntry new]
    }
    
    
    #+++
    :public method init {} {
    	:initVars;  #+++
	
	:define PasswdDlg passwddlg Dialog LabelEntry

	:bwinclude PasswdDlg Dialog :cmd \
	    remove     {-image -bitmap -side -default -cancel -separator} \
	    initialize {-modal local -anchor e}
	
	:bwinclude PasswdDlg LabelEntry .frame.lablog \
	    remove [list -command -justify -name -show -side	        \
		-state -takefocus -width -xscrollcommand -padx -pady	\
		-dragenabled -dragendcmd -dragevent -draginitcmd	\
		-dragtype -dropenabled -dropcmd -dropovercmd -droptypes	\
		] \
	    prefix [list login -editable -helptext -helpvar -label      \
		-text -textvariable -underline				\
		] \
	    initialize [list -relief sunken -borderwidth 2		\
		-labelanchor w -width 15 -loginlabel "Login"		\
		]
	
	:bwinclude PasswdDlg LabelEntry .frame.labpass		\
	    remove [list -command -width -show -side -takefocus		\
		-xscrollcommand -dragenabled -dragendcmd -dragevent	\
		-draginitcmd -dragtype -dropenabled -dropcmd		\
		-dropovercmd -droptypes -justify -padx -pady -name	\
		] \
	    prefix [list passwd -editable -helptext -helpvar -label	\
		-state -text -textvariable -underline			\
		] \
	    initialize [list -relief sunken -borderwidth 2		\
		-labelanchor w -width 15 -passwdlabel "Password"	\
		]
	
	:declare PasswdDlg {
	    {-type        Enum       ok           0 {ok okcancel}}
	    {-labelwidth  TkResource -1           0 {label -width}}
	    {-command     String     ""           0}
	}
    }

}

}




# -----------------------------------------------------------------------------
#  Command PasswdDlg::create
# -----------------------------------------------------------------------------
NXBwidget::PasswdDlg public method create { path args } {
    :storePath $path; #+++

    array set maps [list PasswdDlg {} :cmd {} .frame.lablog {} \
	    .frame.labpass {}]
    array set maps [:parseArgs PasswdDlg $args]; #+++

    :initFromODB PasswdDlg "$path#PasswdDlg" $maps(PasswdDlg); #+++

    # Extract the PasswdDlg megawidget options (those that don't map to a
    # subwidget)
    set type      [:cget "$path#PasswdDlg" -type]; #+++
    set cmd       [:cget "$path#PasswdDlg" -command]

    set defb -1
    set canb -1
    switch -- $type {
        ok        { set lbut {ok}; set defb 0 }
        okcancel  { set lbut {ok cancel} ; set defb 0; set canb 1 }
    }

    #+++
    set __bmp [NXBwidget::Bitmap new]
    $__bmp create ${:BWIDGET_LIBRARY}/images
    #+++
    
    #+++
    eval [list ${:_inst_Dialog} create $path] $maps(:cmd) \
        [list -class PasswdDlg -image [$__bmp get passwd] \
	     -side bottom -default $defb -cancel $canb]
    foreach but $lbut {
        if { $but == "ok" && $cmd != "" } {
            ${:_inst_Dialog} add $path -text $but -name $but -command $cmd
        } else {
            ${:_inst_Dialog} add $path -text $but -name $but
        }
    }

    set frame [${:_inst_Dialog} getframe $path]; #+++
    bind $path  <Return>  ""
    bind $frame <Destroy> [list [self] destroy $path\#PasswdDlg]; #+++
    
    #+++
    #${:_inst_LabelEntry}
    
    set __lablog [NXBwidget::LabelEntry new]
    set __labpass [NXBwidget::LabelEntry new]
    
    set lablog [eval [list $__lablog create $frame.lablog] \
		    $maps(.frame.lablog) \
		    [list -name login -dragenabled 0 -dropenabled 0 \
			 -command [list [self] _verifonpasswd \
				       $path $frame.labpass]]]
    
    puts "\n-->PasswdDlg::create\n  \
         lablog: $lablog\n  \
	 frame: $frame\n  \
	 maps: [array get maps]\n"

    #+++
    set labpass [eval [list $__labpass create $frame.labpass] \
		     $maps(.frame.labpass) \
		     [list -name password -show "*" \
			  -dragenabled 0 -dropenabled 0 \
			  -command [list [self] _verifonlogin \
					$path $frame.lablog]]]

    # compute label width
    if {[$lablog cget -labelwidth] == 0} {
        #set loglabel  [$lablog cget -label]
        #set passlabel [$labpass cget -label]

	set loglabel  [$__lablog cget $lablog -label]
        set passlabel [$__labpass cget $labpass -label]

        set labwidth  [:_max [string length $loglabel] [string length $passlabel]]

	set labwidth 7; #+++ added by me

        incr labwidth 1
	#+++
        #$lablog  configure -labelwidth $labwidth
        #$labpass configure -labelwidth $labwidth
	
	$__lablog configure $lablog "-labelwidth $labwidth"
	$__labpass configure $labpass "-labelwidth $labwidth"
	#+++
    }

    next "PasswdDlg $path 0"; #+++

    pack  $frame.lablog $frame.labpass -fill x -expand 1

    # added by bach@mwgdna.com
    #  give focus to loginlabel unless the state is disabled
    if {[$lablog cget -editable]} {
	focus $frame.lablog.e
    } else {
	focus $frame.labpass.e
    }
    set res [${:_inst_Dialog} draw $path]
    
    
    if { $res == 0 } {
	set res [list [$lablog.e cget -text] [$labpass.e cget -text]]
	
	#$lablog.e is the Entry that is part of the Labelframe widget. The above command
	#is equivalent with this:
	#set __entry [nsf::var::set ${:_inst_LabelEntry} _inst_Entry]
	#set res  [list  [$__entry cget $lablog.e -text]  [$__entry cget $labpass.e -text]]
    } else {
        set res [list]
    }
    
    puts "\n-->PasswdDlg::create - RES: $res\n"
    
    :destroy "$path#PasswdDlg"; #+++
    destroy $path

    return $res
}

# -----------------------------------------------------------------------------
#  Command PasswdDlg::configure
# -----------------------------------------------------------------------------
NXBwidget::PasswdDlg public method configure { path args } {
    set res [:configure "$path#PasswdDlg" $args]; #+++
}

# -----------------------------------------------------------------------------
#  Command PasswdDlg::cget
# -----------------------------------------------------------------------------

#+++ NXBwidget::PasswdDlg public method cget { path option } {
#    return [Widget::cget "$path#PasswdDlg" $option]
#}


# -----------------------------------------------------------------------------
#  Command PasswdDlg::_verifonlogin
# -----------------------------------------------------------------------------
NXBwidget::PasswdDlg public method _verifonlogin { path labpass } {
    ${:_inst_Dialog} enddialog $path 0; #+++
}

# -----------------------------------------------------------------------------
#  Command PasswdDlg::_verifonpasswd
# -----------------------------------------------------------------------------
NXBwidget::PasswdDlg public method _verifonpasswd { path labpass } {
    if {[string equal [$labpass cget -state] "disabled"]} {
        ${:_inst_Dialog} enddialog $path 0; #+++
    } else {
        focus $labpass
    }
}

# -----------------------------------------------------------------------------
#  Command PasswdDlg::_max
# -----------------------------------------------------------------------------
NXBwidget::PasswdDlg public method _max { val1 val2 } { 
    return [expr {($val1 > $val2) ? ($val1) : ($val2)}] 
}


#+++ (added by me)
NXBwidget::PasswdDlg public method storePath {path} {
    set :wpath $path
}


#+++ (added by me)
NXBwidget::PasswdDlg public method retrievePath {} {
    return ${:wpath}
}
