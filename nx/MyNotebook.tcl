#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide mynotebook 1.0

package require nx
package require widget
package require notebook



nx::Class create Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


Demo public method create { } {
    #variable notebook
    
    #Demo::createMainwindow
    #set frame    [${Demo::mainwin} getframe]
    
    
    set frame [frame .topf]
    pack $frame -pady 2 -fill x
    
    
    # NoteBook creation
    set notebook [NXBwidget::NoteBook new]
    $notebook create $frame.nb
    
    set __frame [$notebook insert [$notebook retrievePath] end demoBasic -text "My Notebook"]
    
    #pack the notebook
    $notebook compute_size [$notebook retrievePath]
    pack [$notebook retrievePath] -fill both -expand yes -padx 4 -pady 4
    $notebook raise [$notebook page [$notebook retrievePath] 0]
    
    $notebook place . 300 200 right
    
    #set the size of the notebook
    $notebook configure $frame.nb -width 290 -height 190
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    
    update idletasks
}


Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "Notebook demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}

set dm [Demo new]
wm geom . [wm geom .]
