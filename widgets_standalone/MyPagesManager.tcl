#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}



namespace eval Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


proc Demo::createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


proc Demo::create { } {
    Demo::createMainwindow
    
    
    set frame    [${Demo::mainwin} getframe]
    
    
    #create the PagesManager
    set pm [PagesManager $frame.pm]
    pack $pm -fill both -expand true
 
    set npages 5
    for { set i 0 } { $i < $npages } { incr i } {
       set p [$pm add $i]
       #pack [button .f.b$i -width 4 -text $i -command [list .pm raise $i]] -fill x
       grid [label $p.lbl0 -text "This is page $i"] -row 0 -column 0 -columnspan 2
       grid [label $p.lbl1 -text "Place some content here."] -row 1 -column 0 -columnspan 2
       grid [button $p.prev -text "Back" -command [list $pm raise [expr {$i-1}]]] -row 2 -column 0
       grid [button $p.next -text "Next" -command [list $pm raise [expr {$i+1}]]] -row 2 -column 1
       if { $i == 0 } { $p.prev configure -state disabled }
       if { $i >= ($npages-1) } { $p.next configure -state disabled }
    }
    $pm raise 0
    
    
    #pack the mainframe
    pack ${Demo::mainwin} -fill both -expand yes
    update idletasks
}



proc Demo::main {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "PagesManager demo"

    Demo::create
    BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    wm deiconify .
    raise .
    focus -force .
}

Demo::main
wm geom . [wm geom .]
