# ----------------------------------------------------------------------------
#  widget.tcl
#  This file is part of Unifix BWidget Toolkit
#  $Id: widget.tcl,v 1.35.2.1 2011/11/14 14:33:29 oehhar Exp $
# ----------------------------------------------------------------------------
#  Index of commands:
#     - Widget::tkinclude
#     - Widget::bwinclude
#     - Widget::declare
#     - Widget::addmap
#     - Widget::init
#     - Widget::destroy
#     - Widget::setoption
#     - Widget::configure
#     - Widget::cget
#     - Widget::subcget
#     - Widget::hasChanged
#     - Widget::options
#     - Widget::_get_tkwidget_options
#     - Widget::_test_tkresource
#     - Widget::_test_bwresource
#     - Widget::_test_synonym
#     - Widget::_test_string
#     - Widget::_test_flag
#     - Widget::_test_enum
#     - Widget::_test_int
#     - Widget::_test_boolean
# ----------------------------------------------------------------------------
# Each megawidget gets a namespace of the same name inside the Widget namespace
# Each of these has an array opt, which contains information about the 
# megawidget options.  It maps megawidget options to a list with this format:
#     {optionType defaultValue isReadonly {additionalOptionalInfo}}
# Option types and their additional optional info are:
#	TkResource	{genericTkWidget genericTkWidgetOptionName}
#	BwResource	{nothing}
#	Enum		{list of enumeration values}
#	Int		{Boundary information}
#	Boolean		{nothing}
#	String		{nothing}
#	Flag		{string of valid flag characters}
#	Synonym		{nothing}
#	Color		{nothing}
#
# Next, each namespace has an array map, which maps class options to their
# component widget options:
#	map(-foreground) => {.e -foreground .f -foreground}
#
# Each has an array ${path}:opt, which contains the value of each megawidget
# option for a particular instance $path of the megawidget, and an array
# ${path}:mod, which stores the "changed" status of configuration options.

# Steps for creating a bwidget megawidget:
# 1. parse args to extract subwidget spec
# 2. Create frame with appropriate class and command line options
# 3. Get initialization options from optionDB, using frame
# 4. create subwidgets

# Uses newer string operations

package provide widget 1.0

package require nx
package require bwidget
package require bwinit
package require Tcl 8.1.1



namespace eval NXBwidget {

nx::Class create Widget -mixin NXBwidget::BWidget {

#+++
:public method initVars args {
    set :BWIDGET_LIBRARY [pwd]; #+++ added by me (some other package, e.g.
    #dropsite.tcl, uses this. This is the folder, where images, and the language resources are stored.)
    
    #:variable _optiontype
    #:variable _class
    #:variable _tk_widget
    
    :bwidget_init; #+++
    
    #array set ::NXBwidget::_class {}; #must be declared global
    #array set :_tk_widget {}
    
    #+++
    if { [array exists ::NXBwidget::_class]==0 } {
    	array set ::NXBwidget::_class {}; #must be declared global
    }
    #+++
    
    # This controls whether we try to use themed widgets from Tile
    #:variable _theme 0
    set :_theme 0

    #:variable _aqua [expr {($::tcl_version >= 8.4) &&
    #		  [string equal [tk windowingsystem] "aqua"]}]
    set :_aqua [expr {($::tcl_version >= 8.4) &&
			  [string equal [tk windowingsystem] "aqua"]}]
    
    
    #+++
    #array set _optiontype {
    #    TkResource Widget::_test_tkresource
    #    BwResource Widget::_test_bwresource
    #    Enum       Widget::_test_enum
    #    Int        Widget::_test_int
    #    Boolean    Widget::_test_boolean
    #    String     Widget::_test_string
    #    Flag       Widget::_test_flag
    #    Synonym    Widget::_test_synonym
    #    Color      Widget::_test_color
    #    Padding    Widget::_test_padding
    #}
    

    #+++
    array set :_optiontype {
		TkResource :_test_tkresource
                BwResource :_test_bwresource
		Enum       :_test_enum
                Int        :_test_int
		Boolean    :_test_boolean
                String     :_test_string
		Flag       :_test_flag
                Synonym    :_test_synonym
		Color      :_test_color
                Padding    :_test_padding
    }

    puts "\n-->Widget::initVars:\n  BWIDGET_LIBRARY=${:BWIDGET_LIBRARY}\n  \
	  _optiontype(TkResource): ${:_optiontype(TkResource)}\n"; #+++
}

#proc use {} {}
#+++
:public method use {} {
  #empty line to avoid method deletion
}




# ----------------------------------------------------------------------------
#  Command Widget::tkinclude
#     Includes tk widget resources to BWidget widget.
#  class      class name of the BWidget
#  tkwidget   tk widget to include
#  subpath    subpath to configure
#  args       additionnal args for included options
# ----------------------------------------------------------------------------
:public method tkinclude { class tkwidget subpath args } {
    foreach {cmd lopt} $args {
        # cmd can be
        #   include      options to include            lopt = {opt ...}
        #   remove       options to remove             lopt = {opt ...}
        #   rename       options to rename             lopt = {opt newopt ...}
        #   prefix       options to prefix             lopt = {pref opt opt ..}
        #   initialize   set default value for options lopt = {opt value ...}
        #   readonly     set readonly flag for options lopt = {opt flag ...}
        switch -- $cmd {
            remove {
                foreach option $lopt {
                    set remove($option) 1
                }
            }
            include {
                foreach option $lopt {
                    set include($option) 1
                }
            }
            prefix {
                set prefix [lindex $lopt 0]
                foreach option [lrange $lopt 1 end] {
                    set rename($option) "-$prefix[string range $option 1 end]"
                }
            }
            rename     -
            readonly   -
            initialize {
                array set $cmd $lopt
            }
            default {
                return -code error "invalid argument \"$cmd\""
            }
        }
    }

    
    #+++
    #namespace eval $class {}
    #upvar 0 ${class}::opt classopt
    #upvar 0 ${class}::map classmap
    #upvar 0 ${class}::map$subpath submap
    #upvar 0 ${class}::optionExports exports
    
    
    #nx::Class create $class {}
    upvar 0 :${class}_opt classopt
    upvar 0 :${class}_map classmap
    upvar 0 :${class}_map$subpath submap
    upvar 0 :${class}_optionExports exports
    #+++

    
    set foo [$tkwidget ".ericFoo###"]
    # create resources informations from tk widget resources
    foreach optdesc [:_get_tkwidget_options $tkwidget] {
        set option [lindex $optdesc 0]
        if { (![info exists include] || [info exists include($option)]) &&
             ![info exists remove($option)] } {
            if { [llength $optdesc] == 3 } {
                # option is a synonym
                set syn [lindex $optdesc 1]
                if { ![info exists remove($syn)] } {
                    # original option is not removed
                    if { [info exists rename($syn)] } {
                        set classopt($option) [list Synonym $rename($syn)]
                    } else {
                        set classopt($option) [list Synonym $syn]
                    }
                }
            } else {
                if { [info exists rename($option)] } {
                    set realopt $option
                    set option  $rename($option)
                } else {
                    set realopt $option
                }
                if { [info exists initialize($option)] } {
                    set value $initialize($option)
                } else {
                    set value [lindex $optdesc 1]
                }
                if { [info exists readonly($option)] } {
                    set ro $readonly($option)
                } else {
                    set ro 0
                }
                set classopt($option) \
			[list TkResource $value $ro [list $tkwidget $realopt]]

		# Add an option database entry for this option
		set optionDbName ".[lindex [:_configure_option $realopt ""] 0]"
		if { ![string equal $subpath ":cmd"] } {
		    set optionDbName "$subpath$optionDbName"
		}
		option add *${class}$optionDbName $value widgetDefault
		lappend exports($option) "$optionDbName"

		# Store the forward and backward mappings for this
		# option <-> realoption pair
                lappend classmap($option) $subpath "" $realopt
		set submap($realopt) $option
            }
        }
    }
    puts "\n-->FOO - DESTROY\n"
    ::destroy $foo
}


# ----------------------------------------------------------------------------
#  Command Widget::bwinclude
#     Includes BWidget resources to BWidget widget.
#  class    class name of the BWidget
#  subclass BWidget class to include
#  subpath  subpath to configure
#  args     additionnal args for included options
# ----------------------------------------------------------------------------
:public method bwinclude { class subclass subpath args } {
    foreach {cmd lopt} $args {
        # cmd can be
        #   include      options to include            lopt = {opt ...}
        #   remove       options to remove             lopt = {opt ...}
        #   rename       options to rename             lopt = {opt newopt ...}
        #   prefix       options to prefix             lopt = {prefix opt opt ...}
        #   initialize   set default value for options lopt = {opt value ...}
        #   readonly     set readonly flag for options lopt = {opt flag ...}
        switch -- $cmd {
            remove {
                foreach option $lopt {
                    set remove($option) 1
                }
            }
            include {
                foreach option $lopt {
                    set include($option) 1
                }
            }
            prefix {
                set prefix [lindex $lopt 0]
                foreach option [lrange $lopt 1 end] {
                    set rename($option) "-$prefix[string range $option 1 end]"
                }
            }
            rename     -
            readonly   -
            initialize {
                array set $cmd $lopt
            }
            default {
                return -code error "invalid argument \"$cmd\""
            }
        }
    }
    
    
    #+++
    #namespace eval $class {}
    #upvar 0 ${class}::opt classopt
    #upvar 0 ${class}::map classmap
    #upvar 0 ${class}::map$subpath submap
    #upvar 0 ${class}::optionExports exports
    #upvar 0 ${subclass}::opt subclassopt
    #upvar 0 ${subclass}::optionExports subexports
    
    upvar 0 :${class}_opt classopt
    upvar 0 :${class}_map classmap
    upvar 0 :${class}_map$subpath submap
    upvar 0 :${class}_optionExports exports
    
    
    set ptr ":_inst_${subclass}";  set inst [set ${ptr}];
    #upvar 0 :${subclass}_opt subclassopt
    set copycmd "array get :${subclass}_opt"
    array set subclassopt  [$inst  eval  $copycmd]
    
    #upvar 0 :${subclass}_optionExports subexports
    set copycmd "array get :${subclass}_optionExports"
    array set subexports  [$inst  eval  $copycmd]
    
    puts "\n-->Widget::bwinclude:\n  \
	SUBclassopt(): [array get subclassopt]\n  \
	SUBexports(): [array get subexports]\n"; #+++
    #+++
    
    
    # create resources informations from BWidget resources
    foreach {option optdesc} [array get subclassopt] {
	set subOption $option
        if { (![info exists include] || [info exists include($option)]) &&
             ![info exists remove($option)] } {
            set type [lindex $optdesc 0]
            if { [string equal $type "Synonym"] } {
                # option is a synonym
                set syn [lindex $optdesc 1]
                if { ![info exists remove($syn)] } {
                    if { [info exists rename($syn)] } {
                        set classopt($option) [list Synonym $rename($syn)]
                    } else {
                        set classopt($option) [list Synonym $syn]
                    }
                }
            } else {
                if { [info exists rename($option)] } {
                    set realopt $option
                    set option  $rename($option)
                } else {
                    set realopt $option
                }
                if { [info exists initialize($option)] } {
                    set value $initialize($option)
                } else {
                    set value [lindex $optdesc 1]
                }
                if { [info exists readonly($option)] } {
                    set ro $readonly($option)
                } else {
                    set ro [lindex $optdesc 2]
                }
                set classopt($option) \
			[list $type $value $ro [lindex $optdesc 3]]

		# Add an option database entry for this option
		foreach optionDbName $subexports($subOption) {
		    if { ![string equal $subpath ":cmd"] } {
			set optionDbName "$subpath$optionDbName"
		    }
		    # Only add the option db entry if we are overriding the
		    # normal widget default
		    if { [info exists initialize($option)] } {
			option add *${class}$optionDbName $value \
				widgetDefault
		    }
		    lappend exports($option) "$optionDbName"
		}

		# Store the forward and backward mappings for this
		# option <-> realoption pair
                lappend classmap($option) $subpath $subclass $realopt
		set submap($realopt) $option
            }
        }
    }
    
    puts "\n-->Widget::bwinclude:\n  \
	classopt(): [array get classopt]\n"; #+++
}


# ----------------------------------------------------------------------------
#  Command Widget::declare
#    Declares new options to BWidget class.
# ----------------------------------------------------------------------------
:public method declare { class optlist } {
    #+++ variable _optiontype;  #this is a  classvariable
    upvar 0 :_optiontype _optiontype;  #bind the local variable _optiontype, to the classvariable :_optiontype


    #+++
    #namespace eval $class {}
    #upvar 0 ${class}::opt classopt
    #upvar 0 ${class}::optionExports exports
    #upvar 0 ${class}::optionClass optionClass
    
    upvar 0 :${class}_opt classopt
    upvar 0 :${class}_optionExports exports
    upvar 0 :${class}_optionClass optionClass
    #+++
    
    
    puts "\n-->Widget::declare - BEGIN\n  \
	classopt(): [array get classopt]\n"; #+++


    foreach optdesc $optlist {
        set option  [lindex $optdesc 0]
        set optdesc [lrange $optdesc 1 end]
        set type    [lindex $optdesc 0]

        #puts "\n  _theme: ${:_theme}";  #+++
        puts "  type: $type"
        #puts "  _optiontype($type): $_optiontype($type)]"
	puts "  IS-EXISTS _optiontype($type) => [info exists _optiontype($type)]"
	puts "  optdesc: {$optdesc}"
	
        if { ![info exists _optiontype($type)] } {
            # invalid resource type
            return -code error "invalid option type \"$type\""
        }

        if { [string equal $type "Synonym"] } {
            # test existence of synonym option
            set syn [lindex $optdesc 1]
            if { ![info exists classopt($syn)] } {
                return -code error "unknow option \"$syn\" for Synonym \"$option\""
            }
            set classopt($option) [list Synonym $syn]
            continue
        }

        # all other resource may have default value, readonly flag and
        # optional arg depending on type
        set value [lindex $optdesc 1]
        set ro    [lindex $optdesc 2]
        set arg   [lindex $optdesc 3]

        if { [string equal $type "BwResource"] } {
            # We don't keep BwResource. We simplify to type of sub BWidget
            set subclass    [lindex $arg 0]
            set realopt     [lindex $arg 1]
            if { ![string length $realopt] } {
                set realopt $option
            }
	    
	    
	    puts "\n  subclass: $subclass\n  realopt: $realopt\n"
	    
	    
	    #+++ upvar 0 ${subclass}::opt subclassopt
	    set ptr ":_inst_${subclass}";  set inst [set ${ptr}]; #+++???
	    set copycmd "array get :${subclass}_opt"; #+++
	    array set subclassopt [${inst}  eval  $copycmd]; #+++ copy array


            if { ![info exists subclassopt($realopt)] } {
                return -code error "unknow option \"$realopt\""
            }
            set suboptdesc $subclassopt($realopt)
            if { $value == "" } {
                # We initialize default value
                set value [lindex $suboptdesc 1]
            }
            set type [lindex $suboptdesc 0]
            set ro   [lindex $suboptdesc 2]
            set arg  [lindex $suboptdesc 3]
	    set optionDbName ".[lindex [:_configure_option $option ""] 0]"
	    option add *${class}${optionDbName} $value widgetDefault
	    set exports($option) $optionDbName
            set classopt($option) [list $type $value $ro $arg]
            continue
        }

        # retreive default value for TkResource
        if { [string equal $type "TkResource"] } {
            set tkwidget [lindex $arg 0]
	    set foo [$tkwidget ".ericFoo##"]
            set realopt  [lindex $arg 1]
            if { ![string length $realopt] } {
                set realopt $option
            }
            set tkoptions [:_get_tkwidget_options $tkwidget]
            if { ![string length $value] } {
                # We initialize default value
		set ind [lsearch $tkoptions [list $realopt *]]
                set value [lindex [lindex $tkoptions $ind] end]
            }
	    set optionDbName ".[lindex [:_configure_option $option ""] 0]"
	    option add *${class}${optionDbName} $value widgetDefault
	    set exports($option) $optionDbName
            set classopt($option) [list TkResource $value $ro \
		    [list $tkwidget $realopt]]
	    set optionClass($option) [lindex [$foo configure $realopt] 1]
	    ::destroy $foo
            continue
        }

	set optionDbName ".[lindex [:_configure_option $option ""] 0]"
	option add *${class}${optionDbName} $value widgetDefault
	set exports($option) $optionDbName
        # for any other resource type, we keep original optdesc
        set classopt($option) [list $type $value $ro $arg]
    }
    
    
    puts "\n-->Widget::declare - END\n";  #+++ added by me
}


:public method define { class filename args } {
    #+++ variable ::BWidget::use
    upvar 0 :use use;  #use@Bwidget

    set use($class)      $args
    set use($class,file) $filename
    lappend use(classes) $class
    
    puts "\nWidget::define:\n  ARGS: $args\n  CLASS: $class"

    if {[set x [lsearch -exact $args "-classonly"]] > -1} {
	set args [lreplace $args $x $x]
    } else {
	interp alias {} ::${class} {} ${class}::create
	#+++ proc ::${class}::use {} {}
	
	#+++
	#::NXBwidget::${class} public object method use {} {
	#    empty line...
	#}

	#+++ bind $class <Destroy> [list NXBwidget::Widget::destroy %W]
	bind [self] <Destroy> [list [self] destroy %W];  #+++
    }

    #+++ foreach class $args { NXBwidget::${class}::use }
}


:public method create { class path {rename 1} } {
    if {$rename} { rename $path ::$path:cmd }
    
    #This method connects a path to a namespace in the original version.
    #A command, like  [$path cget] is invoked in Widget::cget
    
    puts "\n-->widget::create:\n  class: $class\n  path: $path\n  SELF: [self]\n"

    #+++ proc ::$path { cmd args } \
    #	[subst {return \[eval \[linsert \$args 0 ${class}::\$cmd [list $path]\]\]}]
    
    
    #proc ::$path { cmd args } \
    #	[subst {return \[eval \[linsert \$args 0  NXBwidget::${class}::\$cmd [list $path]\]\]}]
    
    
    #Now it connects a path to a classinstance (to [self]).
    proc ::$path { cmd args } \
    	[subst {return \[eval \[linsert \$args 0  [self] \$cmd [list $path]\]\]}]
    #+++

    return $path
}


# ----------------------------------------------------------------------------
#  Command Widget::addmap
# ----------------------------------------------------------------------------
:public method addmap { class subclass subpath options } {
    #+++
    #upvar 0 ${class}::opt classopt
    #upvar 0 ${class}::optionExports exports
    #upvar 0 ${class}::optionClass optionClass
    #upvar 0 ${class}::map classmap
    #upvar 0 ${class}::map$subpath submap
    
    upvar 0 :${class}_opt classopt
    upvar 0 :${class}_optionExports exports
    upvar 0 :${class}_optionClass optionClass
    upvar 0 :${class}_map classmap
    upvar 0 :${class}_map$subpath submap
    #+++

    foreach {option realopt} $options {
        if { ![string length $realopt] } {
            set realopt $option
        }
	set val [lindex $classopt($option) 1]
	set optDb ".[lindex [:_configure_option $realopt ""] 0]"
	if { ![string equal $subpath ":cmd"] } {
	    set optDb "$subpath$optDb"
	}
	option add *${class}${optDb} $val widgetDefault
	lappend exports($option) $optDb
	# Store the forward and backward mappings for this
	# option <-> realoption pair
        lappend classmap($option) $subpath $subclass $realopt
	set submap($realopt) $option
    }
    
    puts "\n-->Widget::addmap - END\n";  #+++ added by me
}


# ----------------------------------------------------------------------------
#  Command Widget::syncoptions
# ----------------------------------------------------------------------------
:public method syncoptions { class subclass subpath options } {
    #+++ upvar 0 ${class}::sync classync
    upvar 0 :${class}_sync classync
    #+++

    foreach {option realopt} $options {
        if { ![string length $realopt] } {
            set realopt $option
        }
        set classync($option) [list $subpath $subclass $realopt]
    }
}


# ----------------------------------------------------------------------------
#  Command Widget::init
# ----------------------------------------------------------------------------
#+++ init becomes widget_init
:public method widget_init { class path options } {
    #+++ (variables used by Widget itself)
    #variable _inuse
    #variable _class
    #variable _optiontype
    
    puts "\n-->widget_init - BEGIN\n"
    
    upvar 0 :_inuse _inuse
    upvar 0 NXBwidget::_class _class
    upvar 0 :_optiontype _optiontype
    #+++
    
    
    #+++ (variables of ${class}; e.g class = ProgressBar)
    #upvar 0 ${class}::opt classopt
    #upvar 0 ${class}::$path:opt  pathopt
    #upvar 0 ${class}::$path:mod  pathmod
    #upvar 0 ${class}::map classmap
    #upvar 0 ${class}::$path:init pathinit
    
    upvar 0 :${class}_opt classopt
    upvar 0 ::NXBwidget::${class}_$path:opt  pathopt
    upvar 0 ::NXBwidget::${class}_$path:mod  pathmod
    upvar 0 :${class}_map classmap
    upvar 0 :${class}_$path:init pathinit
    #+++
    
    
    puts "\n-->widget_init:\n  \
	class: $class\n  \
	path: $path\n  \
	options: $options\n  \
	classopt: [array get classopt]\n"
    
    
    if { [info exists pathopt] } {
	unset pathopt
    }
    if { [info exists pathmod] } {
	unset pathmod
    }
    # We prefer to use the actual widget for option db queries, but if it
    # doesn't exist yet, do the next best thing:  create a widget of the
    # same class and use that.
    set fpath $path
    set rdbclass [string map [list :: ""] $class]
    if { ![winfo exists $path] } {
	set fpath ".#BWidget.#Class#$class"
	# encapsulation frame to not pollute '.' childspace
	if {![winfo exists ".#BWidget"]} { frame ".#BWidget" }
	if { ![winfo exists $fpath] } {
	    frame $fpath -class $rdbclass
	}
    }
    foreach {option optdesc} [array get classopt] {
        set pathmod($option) 0
	if { [info exists classmap($option)] } {
	    continue
	}
        set type [lindex $optdesc 0]
        if { [string equal $type "Synonym"] } {
	    continue
        }
        if { [string equal $type "TkResource"] } {
            set alt [lindex [lindex $optdesc 3] 1]
        } else {
            set alt ""
        }
        set optdb [lindex [:_configure_option $option $alt] 0]
        set def   [option get $fpath $optdb $rdbclass]
        if { [string length $def] } {
            set pathopt($option) $def
        } else {
            set pathopt($option) [lindex $optdesc 1]
        }
    }

    if {![info exists _inuse($class)]} { set _inuse($class) 0 }
    incr _inuse($class)

    set _class($path) $class
    foreach {option value} $options {
        if { ![info exists classopt($option)] } {
            unset pathopt
            unset pathmod
            return -code error "unknown option \"$option\""
        }
        set optdesc $classopt($option)
        set type    [lindex $optdesc 0]
        if { [string equal $type "Synonym"] } {
            set option  [lindex $optdesc 1]
            set optdesc $classopt($option)
            set type    [lindex $optdesc 0]
        }
        # this may fail if a wrong enum element was used
        if {[catch {
             $_optiontype($type) $option $value [lindex $optdesc 3]
        } msg]} {
            if {[info exists pathopt]} {
                unset pathopt
            }
            unset pathmod
            return -code error $msg
        }
        set pathopt($option) $msg
	set pathinit($option) $pathopt($option)
    }
    
    puts "\n-->widget_init - END\n";  #+++ added by me
}

# Bastien Chevreux (bach@mwgdna.com)
#
# copyinit performs basically the same job as init, but it uses a
#  existing template to initialize its values. So, first a perferct copy
#  from the template is made just to be altered by any existing options
#  afterwards.
# But this still saves time as the first initialization parsing block is
#  skipped.
# As additional bonus, items that differ in just a few options can be
#  initialized faster by leaving out the options that are equal.

# This function is currently used only by ListBox::multipleinsert, but other
#  calls should follow :)

# ----------------------------------------------------------------------------
#  Command Widget::copyinit
# ----------------------------------------------------------------------------
:public method copyinit { class templatepath path options } {
    #+++
    #variable _class
    #variable _optiontype
    
    upvar 0 NXBwidget::_class _class
    upvar 0 :_optiontype _optiontype
    #+++
    
    #+++
    #upvar 0 ${class}::opt classopt \
	#    ${class}::$path:opt	 pathopt \
	#    ${class}::$path:mod	 pathmod \
	#    ${class}::$path:init pathinit \
	#    ${class}::$templatepath:opt	  templatepathopt \
	#    ${class}::$templatepath:mod	  templatepathmod \
	#    ${class}::$templatepath:init  templatepathinit
    
    upvar 0 :${class}_opt classopt \
	    ::NXBwidget::${class}_$path:opt	 pathopt \
	    ::NXBwidget::${class}_$path:mod	 pathmod \
	    :${class}_$path:init pathinit \
	    :${class}_$templatepath:opt	  templatepathopt \
	    :${class}_$templatepath:mod	  templatepathmod \
	    :${class}_$templatepath:init  templatepathinit
    #+++

    if { [info exists pathopt] } {
	unset pathopt
    }
    if { [info exists pathmod] } {
	unset pathmod
    }

    # We use the template widget for option db copying, but it has to exist!
    array set pathmod  [array get templatepathmod]
    array set pathopt  [array get templatepathopt]
    array set pathinit [array get templatepathinit]

    set _class($path) $class
    foreach {option value} $options {
	if { ![info exists classopt($option)] } {
	    unset pathopt
	    unset pathmod
	    return -code error "unknown option \"$option\""
	}
	set optdesc $classopt($option)
	set type    [lindex $optdesc 0]
	if { [string equal $type "Synonym"] } {
	    set option	[lindex $optdesc 1]
	    set optdesc $classopt($option)
	    set type	[lindex $optdesc 0]
	}
	set pathopt($option) [$_optiontype($type) $option $value [lindex $optdesc 3]]
	set pathinit($option) $pathopt($option)
    }
}

# Widget::parseArgs --
#
#	Given a widget class and a command-line spec, cannonize and validate
#	the given options, and return a keyed list consisting of the 
#	component widget and its masked portion of the command-line spec, and
#	one extra entry consisting of the portion corresponding to the 
#	megawidget itself.
#
# Arguments:
#	class	widget class to parse for.
#	options	command-line spec
#
# Results:
#	result	keyed list of portions of the megawidget and that segment of
#		the command line in which that portion is interested.

:public method parseArgs {class options} {
    #+++ variable _optiontype
    upvar 0 :_optiontype _optiontype; #+++
    
    #+++
    #upvar 0 ${class}::opt classopt
    #upvar 0 ${class}::map classmap
    
    upvar 0 :${class}_opt classopt
    upvar 0 :${class}_map classmap
    #+++
    
    foreach {option val} $options {
	if { ![info exists classopt($option)] } {
	    error "unknown option \"$option\""
	}
        set optdesc $classopt($option)
        set type    [lindex $optdesc 0]
        if { [string equal $type "Synonym"] } {
            set option  [lindex $optdesc 1]
            set optdesc $classopt($option)
            set type    [lindex $optdesc 0]
        }
	if { [string equal $type "TkResource"] } {
	    # Make sure that the widget used for this TkResource exists
	    
	    #+++ Widget::_get_tkwidget_options [lindex [lindex $optdesc 3] 0]
	    :_get_tkwidget_options [lindex [lindex $optdesc 3] 0]; #+++
	}
	set val [$_optiontype($type) $option $val [lindex $optdesc 3]]
		
	if { [info exists classmap($option)] } {
	    foreach {subpath subclass realopt} $classmap($option) {
		lappend maps($subpath) $realopt $val
	    }
	} else {
	    lappend maps($class) $option $val
	}
    }
    return [array get maps]
}

# Widget::initFromODB --
#
#	Initialize a megawidgets options with information from the option
#	database and from the command-line arguments given.
#
# Arguments:
#	class	class of the widget.
#	path	path of the widget -- should already exist.
#	options	command-line arguments.
#
# Results:
#	None.

:public method initFromODB {class path options} {
    #+++
    #variable _inuse
    #variable _class
    
    upvar 0 :_inuse _inuse
    upvar 0 NXBwidget::_class _class
    #+++

    #+++
    #upvar 0 ${class}::$path:opt  pathopt
    #upvar 0 ${class}::$path:mod  pathmod
    #upvar 0 ${class}::map classmap
    
    upvar 0 ::NXBwidget::${class}_$path:opt  pathopt
    upvar 0 ::NXBwidget::${class}_$path:mod  pathmod
    upvar 0 :${class}_map classmap
    #+++

    if { [info exists pathopt] } {
	unset pathopt
    }
    if { [info exists pathmod] } {
	unset pathmod
    }
    # We prefer to use the actual widget for option db queries, but if it
    # doesn't exist yet, do the next best thing:  create a widget of the
    # same class and use that.
    set fpath [:_get_window $class $path]; #+++
    set rdbclass [string map [list :: ""] $class]
    if { ![winfo exists $path] } {
	set fpath ".#BWidget.#Class#$class"
	# encapsulation frame to not pollute '.' childspace
	if {![winfo exists ".#BWidget"]} { frame ".#BWidget" }
	if { ![winfo exists $fpath] } {
	    frame $fpath -class $rdbclass
	}
    }

    foreach {option optdesc} [array get :${class}_opt] {; #+++
        set pathmod($option) 0
	if { [info exists classmap($option)] } {
	    continue
	}
        set type [lindex $optdesc 0]
        if { [string equal $type "Synonym"] } {
	    continue
        }
	if { [string equal $type "TkResource"] } {
            set alt [lindex [lindex $optdesc 3] 1]
        } else {
            set alt ""
        }
        set optdb [lindex [:_configure_option $option $alt] 0]; #+++
        set def   [option get $fpath $optdb $rdbclass]
        if { [string length $def] } {
            set pathopt($option) $def
        } else {
            set pathopt($option) [lindex $optdesc 1]
        }
    }

    if {![info exists _inuse($class)]} { set _inuse($class) 0 }
    incr _inuse($class)

    set _class($path) $class
    array set pathopt $options
    
    puts "\n-->Widget::initFromODB\n  \
         class: [array get _class]\n"
}



# ----------------------------------------------------------------------------
#  Command Widget::destroy
# ----------------------------------------------------------------------------
:public method destroy args {
    #+++
    #variable _class
    #variable _inuse
    
    upvar 0 NXBwidget::_class _class
    upvar 0 :_inuse _inuse
    #+++
    
    set path $args
    if {$path == ""} {
	return
    }

    if {![info exists _class($path)]} { return }

    set class $_class($path)
    
    #+++
    #upvar 0 ${class}::$path:opt pathopt
    #upvar 0 ${class}::$path:mod pathmod
    #upvar 0 ${class}::$path:init pathinit
    
    upvar 0 ::NXBwidget::${class}_$path:opt pathopt
    upvar 0 ::NXBwidget::${class}_$path:mod pathmod
    upvar 0 :${class}_$path:init pathinit
    #+++

    if {[info exists _inuse($class)]} { incr _inuse($class) -1 }

    if {[info exists pathopt]} {
        unset pathopt
    }
    if {[info exists pathmod]} {
        unset pathmod
    }
    if {[info exists pathinit]} {
        unset pathinit
    }

    if {![string equal [info commands $path] ""]} { rename $path "" }

    ## Unset any variables used in this widget.
    #+++ foreach var [info vars ::${class}::$path:*] { unset $var }
    foreach var [info vars :${class}_$path:*] { unset $var }; #+++

    unset _class($path)
}



# ----------------------------------------------------------------------------
#  Command Widget::configure
# ----------------------------------------------------------------------------
:public method configure { path options {regex yes} } {
    #+++ added by me
    set orig_options $options
    if { $regex == "yes" } {
	regexp  {\{(.*)\}}  $options _ options
    }
    
    #catch unmatched brace exception
    set lenerr [catch  {set len [llength $options]}]
    if {$lenerr != 0} {
    	set options $orig_options
    	set len [llength $options]
    }
    #+++

    puts "\n-->Widget::configure\n  \
         path: $path\n  \
	 orig_options: $orig_options\n  \
         options: $options\n  \
	 length of options: $len"

    if { $len <= 1 } {
        return [:_get_configure $path $options]; #+++
    } elseif { $len % 2 == 1 } {
        return -code error "incorrect number of arguments"
    }

    #+++ variable _class
    #+++ variable _optiontype
    upvar 0 NXBwidget::_class _class; #+++
    upvar 0 :_optiontype _optiontype; #+++

    set class $_class($path)
    
    #+++
    #upvar 0 ${class}::opt  classopt
    #upvar 0 ${class}::map  classmap
    #upvar 0 ${class}::$path:opt pathopt
    #upvar 0 ${class}::$path:mod pathmod
    
    upvar 0 :${class}_opt  classopt
    upvar 0 :${class}_map  classmap
    upvar 0 ::NXBwidget::${class}_$path:opt pathopt
    upvar 0 ::NXBwidget::${class}_$path:mod pathmod
    #+++
    
    puts "\n  option: [array get classopt]"

    set window [:_get_window $class $path]; #+++
    foreach {option value} $options {
        if { ![info exists classopt($option)] } {
            return -code error "unknown option \"$option\""
        }
        set optdesc $classopt($option)
        set type    [lindex $optdesc 0]
        if { [string equal $type "Synonym"] } {
            set option  [lindex $optdesc 1]
            set optdesc $classopt($option)
            set type    [lindex $optdesc 0]
        }
        if { ![lindex $optdesc 2] } {
            set newval [$_optiontype($type) $option $value [lindex $optdesc 3]]
            if { [info exists classmap($option)] } {
		set window [:_get_window $class $window]; #+++
                foreach {subpath subclass realopt} $classmap($option) {
                    # Interpretation of special pointers:
                    # | subclass | subpath | widget           | path           | class   |
                    # +----------+---------+------------------+----------------+-context-+
                    # | :cmd     | :cmd    | herited widget   | window:cmd     |window   |
                    # | :cmd     | *       | subwidget        | window.subpath | window  |
                    # | ""       | :cmd    | herited widget   | window:cmd     | window  |
                    # | ""       | *       | own              | window         | window  |
                    # | *        | :cmd    | own              | window         | current |
                    # | *        | *       | subwidget        | window.subpath | current |
                    if { [string length $subclass] && ! [string equal $subclass ":cmd"] } {
                        if { [string equal $subpath ":cmd"] } {
                            set subpath ""
                        }
                        #+++ set curval [${subclass}::cget $window$subpath $realopt]
                        #+++ ${subclass}::configure $window$subpath $realopt $newval
			set ptr ":_inst_${subclass}";  set inst [set ${ptr}];
			set curval [${inst} cget $window$subpath $realopt]; #+++
			puts "\n  window_subpath: $window$subpath"
                        ${inst} configure $window$subpath [list $realopt $newval]; #+++
                    } else {
                        #+++ set curval [$window$subpath cget $realopt]
                        #+++ $window$subpath configure $realopt $newval
			set curval [$window$subpath cget $realopt]; #+++
                        $window$subpath configure $realopt $newval; #+++
                    }
                }
            } else {
		set curval $pathopt($option)
		set pathopt($option) $newval
	    }
	    set pathmod($option) [expr {![string equal $newval $curval]}]
        }
    }

    return {}
}


#+++ added by me
:public alias widget_configure [:info method handle configure]


# ----------------------------------------------------------------------------
#  Command Widget::cget
# ----------------------------------------------------------------------------
:public method cget { path option } {
    #+++ variable _class
    upvar 0 ::NXBwidget::_class _class; #+++
    
    if { ![info exists _class($path)] } {
	puts "\n-->Widget::cget - unknown widget  Path: $path\n  \
             _class: [array get _class]\n"
        return -code error "unknown widget $path"
    }

    set class $_class($path)

    puts "\n-->Widget::cget\n  \
	 path: $path\n\n  \
	 option: $option\n\n  \
         class_opt: [array get :${class}_opt]"
	 
    #+++
    if { ![info exists :${class}_opt($option)] } {
        return -code error "unknown option \"$option\""
    }

    set optdesc [set :${class}_opt($option)]; #+++
    set type    [lindex $optdesc 0]
    if {[string equal $type "Synonym"]} {
        set option [lindex $optdesc 1]
    }

    #+++
    if { [info exists :${class}_map($option)] } {
    	#+++
	foreach {subpath subclass realopt} [set :${class}_map($option)] {break}
	set path "[:_get_window $class $path]$subpath"; #+++
	
	puts "\n  new_path: $path\n  \
	    subpath: $subpath\n  \
	    subclass: $subclass\n  \
	    realopt: $realopt"
	
	#return [$path cget $realopt]; #original
	set ret [$path cget $realopt]
	
	puts "\n  RETVALUE: $ret"
	
	return $ret
    }
    upvar 0 ::NXBwidget::${class}_$path:opt pathopt; #+++
    
    puts "\n  pathopt: [array get pathopt]"
	 
    set otherret [set pathopt($option)]
    puts "\n  OTHER-RET_VALUE: $otherret"
    
    return $otherret
}


# ----------------------------------------------------------------------------
#  Command Widget::subcget
# ----------------------------------------------------------------------------
:public method subcget { path subwidget } {
    #+++ variable _class
    upvar 0 NXBwidget::_class _class; #+++
    
    set class $_class($path)
    
    #+++
    #upvar 0 ${class}::$path:opt pathopt
    #upvar 0 ${class}::map$subwidget submap
    #upvar 0 ${class}::$path:init pathinit
    
    upvar 0 ::NXBwidget::${class}_$path:opt pathopt
    upvar 0 :${class}_map$subwidget submap
    upvar 0 :${class}_$path:init pathinit
    #+++

    set result {}
    foreach realopt [array names submap] {
	if { [info exists pathinit($submap($realopt))] } {
	    lappend result $realopt $pathopt($submap($realopt))
	}
    }
    return $result
}


# ----------------------------------------------------------------------------
#  Command Widget::hasChanged
# ----------------------------------------------------------------------------
:public method hasChanged { path option pvalue } {
    #+++ variable _class
    upvar 0 NXBwidget::_class _class; #+++
    
    upvar $pvalue value
    set class $_class($path)
    #+++ upvar 0 ${class}::$path:mod pathmod
    upvar 0 ::NXBwidget::${class}_$path:mod pathmod; #+++

    puts "\n-->Widget::hasChanged\n  pathmod: [array get pathmod]\n"
    
    set value   [:cget $path $option]; #+++
    set result  $pathmod($option)
    set pathmod($option) 0

    return $result
}

:public method hasChangedX { path option args } {
    #+++ variable _class
    upvar 0 NXBwidget::_class _class; #+++
    
    set class $_class($path)
    #+++ upvar 0 ${class}::$path:mod pathmod
    upvar 0 ::NXBwidget::${class}_$path:mod pathmod; #+++

    set result  $pathmod($option)
    set pathmod($option) 0
    foreach option $args {
	lappend result $pathmod($option)
	set pathmod($option) 0
    }

    set result
}


# ----------------------------------------------------------------------------
#  Command Widget::setoption
# ----------------------------------------------------------------------------
:public method setoption { path option value } {
#    variable _class

#    set class $_class($path)
#    upvar 0 ${class}::$path:opt pathopt

#    set pathopt($option) $value

    #+++ Widget::configure $path [list $option $value]
    puts "\n-->Widget::setoption\n  \
         path: $path\n  \
	 option: $option\n  \
	 value: $value\n"
    
    :widget_configure $path [list $option $value]
    #+++
}


# ----------------------------------------------------------------------------
#  Command Widget::getoption
# ----------------------------------------------------------------------------
:public method getoption { path option } {
#    set class $::Widget::_class($path)
#    upvar 0 ${class}::$path:opt pathopt

#    return $pathopt($option)

    return [:cget $path $option]; #+++
}

# Widget::getMegawidgetOption --
#
#	Bypass the superfluous checks in cget and just directly peer at the
#	widget's data space.  This is much more fragile than cget, so it 
#	should only be used with great care, in places where speed is critical.
#
# Arguments:
#	path	widget to lookup options for.
#	option	option to retrieve.
#
# Results:
#	value	option value.

:public method getMegawidgetOption {path option} {
    #+++ variable _class
    upvar 0 NXBwidget::_class _class; #+++
    
    set class $_class($path)
    #+++ upvar 0 ${class}::${path}:opt pathopt
    upvar 0 ::NXBwidget::${class}_${path}:opt pathopt; #+++
    set pathopt($option)
}

# Widget::setMegawidgetOption --
#
#	Bypass the superfluous checks in cget and just directly poke at the
#	widget's data space.  This is much more fragile than configure, so it 
#	should only be used with great care, in places where speed is critical.
#
# Arguments:
#	path	widget to lookup options for.
#	option	option to retrieve.
#	value	option value.
#
# Results:
#	value	option value.

:public method setMegawidgetOption {path option value} {
    #+++ variable _class
    upvar 0 NXBwidget::_class _class; #+++
    
    set class $_class($path)
    #+++ upvar 0 ${class}::${path}:opt pathopt
    upvar 0 ::NXBwidget::${class}_${path}:opt pathopt; #+++
    set pathopt($option) $value
}

# ----------------------------------------------------------------------------
#  Command Widget::_get_window
#  returns the window corresponding to widget path
# ----------------------------------------------------------------------------
:public method _get_window { class path } {
    set idx [string last "#" $path]
    if { $idx != -1 && [string equal [string range $path [expr {$idx+1}] end] $class] } {
        return [string range $path 0 [expr {$idx-1}]]
    } else {
        return $path
    }
}


# ----------------------------------------------------------------------------
#  Command Widget::_get_configure
#  returns the configuration list of options
#  (as tk widget do - [$w configure ?option?])
# ----------------------------------------------------------------------------
:public method _get_configure { path options } {
    #+++ variable _class
    upvar 0 NXBwidget::_class _class; #+++

    set class $_class($path)
    #+++
    #upvar 0 ${class}::opt classopt
    #upvar 0 ${class}::map classmap
    #upvar 0 ${class}::$path:opt pathopt
    #upvar 0 ${class}::$path:mod pathmod
    
    upvar 0 :${class}_opt classopt
    upvar 0 :${class}_map classmap
    upvar 0 ::NXBwidget::${class}_$path:opt pathopt
    upvar 0 ::NXBwidget::${class}_$path:mod pathmod
    #+++

    set len [llength $options]
    if { !$len } {
        set result {}
        foreach option [lsort [array names classopt]] {
            set optdesc $classopt($option)
            set type    [lindex $optdesc 0]
            if { [string equal $type "Synonym"] } {
                set syn     $option
                set option  [lindex $optdesc 1]
                set optdesc $classopt($option)
                set type    [lindex $optdesc 0]
            } else {
                set syn ""
            }
            if { [string equal $type "TkResource"] } {
                set alt [lindex [lindex $optdesc 3] 1]
            } else {
                set alt ""
            }
            set res [:_configure_option $option $alt]; #+++
            if { $syn == "" } {
                lappend result [concat $option $res [list [lindex $optdesc 1]] [list [:cget $path $option]]]; #+++
            } else {
                lappend result [list $syn [lindex $res 0]]
            }
        }
        return $result
    } elseif { $len == 1 } {
        set option  [lindex $options 0]
        if { ![info exists classopt($option)] } {
            return -code error "unknown option \"$option\""
        }
        set optdesc $classopt($option)
        set type    [lindex $optdesc 0]
        if { [string equal $type "Synonym"] } {
            set option  [lindex $optdesc 1]
            set optdesc $classopt($option)
            set type    [lindex $optdesc 0]
        }
        if { [string equal $type "TkResource"] } {
            set alt [lindex [lindex $optdesc 3] 1]
        } else {
            set alt ""
        }
        set res [:_configure_option $option $alt]; #+++
        return [concat $option $res [list [lindex $optdesc 1]] [list [:cget $path $option]]]; #+++
    }
}


# ----------------------------------------------------------------------------
#  Command Widget::_configure_option
# ----------------------------------------------------------------------------
:public method _configure_option { option altopt } {
    #+++
    #variable _optiondb
    #variable _optionclass
    
    upvar 0 :_optiondb _optiondb
    upvar 0 :_optionclass _optionclass
    #+++

    if { [info exists _optiondb($option)] } {
        set optdb $_optiondb($option)
    } else {
        set optdb [string range $option 1 end]
    }
    if { [info exists _optionclass($option)] } {
        set optclass $_optionclass($option)
    } elseif { [string length $altopt] } {
        if { [info exists _optionclass($altopt)] } {
            set optclass $_optionclass($altopt)
        } else {
            set optclass [string range $altopt 1 end]
        }
    } else {
        set optclass [string range $option 1 end]
    }
    return [list $optdb $optclass]
}

# ----------------------------------------------------------------------------
#  Command Widget::_make_tk_widget_name
# ----------------------------------------------------------------------------
# Before, the widget meta name was build as: ".#BWidget.#$tkwidget"
# This does not work for ttk widgets, as they have an "::" in their name.
# Thus replace any "::" by "__" will do the job.
:public method _make_tk_widget_name { tkwidget } {
    set pos 0
    for {set pos 0} {0 <= [set pos [string first "::" $tkwidget $pos]]} {incr pos} {
	set tkwidget [string range $tkwidget 0 [expr {$pos-1}]]__[string range $tkwidget [expr {$pos+2}] end]
    }
    return ".#BWidget.#$tkwidget"
}

# ----------------------------------------------------------------------------
#  Command Widget::_get_tkwidget_options
# ----------------------------------------------------------------------------
:public method _get_tkwidget_options { tkwidget } {
    #+++
    #variable _tk_widget
    #variable _optiondb
    #variable _optionclass
    
    upvar 0 :_tk_widget _tk_widget
    upvar 0 :_optiondb _optiondb
    upvar 0 :_optionclass _optionclass
    #+++

    set widget [:_make_tk_widget_name $tkwidget]; #+++
    # encapsulation frame to not pollute '.' childspace
    
    puts "\n_get_tkwidget_options():\n  tkwidget=${tkwidget}\n  widget=${widget}\n"; #+++
    
    if {![winfo exists ".#BWidget"]} { frame ".#BWidget" }
    if { ![winfo exists $widget] || ![info exists _tk_widget($tkwidget)] } {
	
	puts "\n-->DESTROYING ${widget}\n"; #+++
	::destroy $widget; #+++ (This is not the destroy()@Widget. E.G. $widget = .#BWidget.#frame)
	#+++ See (http://icanprogram.com/09tk/lesson2/lesson2.html)
	#+++     (http://www.ks.uiuc.edu/Research/vmd/mailing_list/vmd-l/17704.html)
	
	set widget [$tkwidget $widget]
	
	# JDC: Withdraw toplevels, otherwise visible
	if {[string equal $tkwidget "toplevel"]} {
	    wm withdraw $widget
	}
	set config [$widget configure]
	foreach optlist $config {
	    set opt [lindex $optlist 0]
	    if { [llength $optlist] == 2 } {
		set refsyn [lindex $optlist 1]
		# search for class
		set idx [lsearch $config [list * $refsyn *]]
		if { $idx == -1 } {
		    if { [string index $refsyn 0] == "-" } {
			# search for option (tk8.1b1 bug)
			set idx [lsearch $config [list $refsyn * *]]
		    } else {
			# last resort
			set idx [lsearch $config [list -[string tolower $refsyn] * *]]
		    }
		    if { $idx == -1 } {
			# fed up with "can't read classopt()"
			return -code error "can't find option of synonym $opt"
		    }
		}
		set syn [lindex [lindex $config $idx] 0]
		# JDC: used 4 (was 3) to get def from optiondb
		set def [lindex [lindex $config $idx] 4]
		lappend _tk_widget($tkwidget) [list $opt $syn $def]
	    } else {
		# JDC: used 4 (was 3) to get def from optiondb
		set def [lindex $optlist 4]
		lappend _tk_widget($tkwidget) [list $opt $def]
		set _optiondb($opt)    [lindex $optlist 1]
		set _optionclass($opt) [lindex $optlist 2]
	    }
	}
    }
    return $_tk_widget($tkwidget)
}


# ----------------------------------------------------------------------------
#  Command Widget::_test_tkresource
# ----------------------------------------------------------------------------
Widget  public method _test_tkresource { option value arg } {
#    set tkwidget [lindex $arg 0]
#    set realopt  [lindex $arg 1]
    foreach {tkwidget realopt} $arg break
    set path     [:_make_tk_widget_name $tkwidget]; #+++
    set old      [$path cget $realopt]
    $path configure $realopt $value
    set res      [$path cget $realopt]
    $path configure $realopt $old

    return $res
}


# ----------------------------------------------------------------------------
#  Command Widget::_test_bwresource
# ----------------------------------------------------------------------------
:public method _test_bwresource { option value arg } {
    return -code error "bad option type BwResource in widget"
}


# ----------------------------------------------------------------------------
#  Command Widget::_test_synonym
# ----------------------------------------------------------------------------
:public method _test_synonym { option value arg } {
    return -code error "bad option type Synonym in widget"
}

# ----------------------------------------------------------------------------
#  Command Widget::_test_color
# ----------------------------------------------------------------------------
:public method _test_color { option value arg } {
    if {[catch {winfo rgb . $value} color]} {
        return -code error "bad $option value \"$value\": must be a colorname \
		or #RRGGBB triplet"
    }

    return $value
}


# ----------------------------------------------------------------------------
#  Command Widget::_test_string
# ----------------------------------------------------------------------------
:public method _test_string { option value arg } {
    set value
}


# ----------------------------------------------------------------------------
#  Command Widget::_test_flag
# ----------------------------------------------------------------------------
:public method _test_flag { option value arg } {
    set len [string length $value]
    set res ""
    for {set i 0} {$i < $len} {incr i} {
        set c [string index $value $i]
        if { [string first $c $arg] == -1 } {
            return -code error "bad [string range $option 1 end] value \"$value\": characters must be in \"$arg\""
        }
        if { [string first $c $res] == -1 } {
            append res $c
        }
    }
    return $res
}


# -----------------------------------------------------------------------------
#  Command Widget::_test_enum
# -----------------------------------------------------------------------------
:public method _test_enum { option value arg } {
    if { [lsearch $arg $value] == -1 } {
        set last [lindex   $arg end]
        set sub  [lreplace $arg end end]
        if { [llength $sub] } {
            set str "[join $sub ", "] or $last"
        } else {
            set str $last
        }
        return -code error "bad [string range $option 1 end] value \"$value\": must be $str"
    }
    return $value
}


# -----------------------------------------------------------------------------
#  Command Widget::_test_int
# -----------------------------------------------------------------------------
:public method _test_int { option value arg } {
    if { ![string is int -strict $value] || \
	    ([string length $arg] && \
	    ![expr [string map [list %d $value] $arg]]) } {
		    return -code error "bad $option value\
			    \"$value\": must be integer ($arg)"
    }
    return $value
}


# -----------------------------------------------------------------------------
#  Command Widget::_test_boolean
# -----------------------------------------------------------------------------
:public method _test_boolean { option value arg } {
    if { ![string is boolean -strict $value] } {
        return -code error "bad $option value \"$value\": must be boolean"
    }

    # Get the canonical form of the boolean value (1 for true, 0 for false)
    return [string is true $value]
}


# -----------------------------------------------------------------------------
#  Command Widget::_test_padding
# -----------------------------------------------------------------------------
:public method _test_padding { option values arg } {
    set len [llength $values]
    if {$len < 1 || $len > 2} {
        return -code error "bad pad value \"$values\":\
                        must be positive screen distance"
    }

    foreach value $values {
        if { ![string is int -strict $value] || \
            ([string length $arg] && \
            ![expr [string map [list %d $value] $arg]]) } {
                return -code error "bad pad value \"$value\":\
                                must be positive screen distance ($arg)"
        }
    }
    return $values
}


# Widget::_get_padding --
#
#       Return the requesting padding value for a padding option.
#
# Arguments:
#	path		Widget to get the options for.
#       option          The name of the padding option.
#	index		The index of the padding.  If the index is empty,
#                       the first padding value is returned.
#
# Results:
#	Return a numeric value that can be used for padding.
:public method _get_padding { path option {index 0} } {
    set pad [:cget $path $option]; #+++
    set val [lindex $pad $index]
    if {$val == ""} { set val [lindex $pad 0] }
    return $val
}


# -----------------------------------------------------------------------------
#  Command Widget::focusNext
#  Same as tk_focusNext, but call Widget::focusOK
# -----------------------------------------------------------------------------
:public method focusNext { w } {
    set cur $w
    while 1 {

	# Descend to just before the first child of the current widget.

	set parent $cur
	set children [winfo children $cur]
	set i -1

	# Look for the next sibling that isn't a top-level.

	while 1 {
	    incr i
	    if {$i < [llength $children]} {
		set cur [lindex $children $i]
		if {[string equal [winfo toplevel $cur] $cur]} {
		    continue
		} else {
		    break
		}
	    }

	    # No more siblings, so go to the current widget's parent.
	    # If it's a top-level, break out of the loop, otherwise
	    # look for its next sibling.

	    set cur $parent
	    if {[string equal [winfo toplevel $cur] $cur]} {
		break
	    }
	    set parent [winfo parent $parent]
	    set children [winfo children $parent]
	    set i [lsearch -exact $children $cur]
	}
	if {[string equal $cur $w] || [focusOK $cur]} {
	    return $cur
	}
    }
}


# -----------------------------------------------------------------------------
#  Command Widget::focusPrev
#  Same as tk_focusPrev, except:
#	+ Don't traverse from a child to a direct ancestor
#	+ Call Widget::focusOK instead of tk::focusOK
# -----------------------------------------------------------------------------
:public method focusPrev { w } {
    set cur $w
    set origParent [winfo parent $w]
    while 1 {

	# Collect information about the current window's position
	# among its siblings.  Also, if the window is a top-level,
	# then reposition to just after the last child of the window.

	if {[string equal [winfo toplevel $cur] $cur]}  {
	    set parent $cur
	    set children [winfo children $cur]
	    set i [llength $children]
	} else {
	    set parent [winfo parent $cur]
	    set children [winfo children $parent]
	    set i [lsearch -exact $children $cur]
	}

	# Go to the previous sibling, then descend to its last descendant
	# (highest in stacking order.  While doing this, ignore top-levels
	# and their descendants.  When we run out of descendants, go up
	# one level to the parent.

	while {$i > 0} {
	    incr i -1
	    set cur [lindex $children $i]
	    if {[string equal [winfo toplevel $cur] $cur]} {
		continue
	    }
	    set parent $cur
	    set children [winfo children $parent]
	    set i [llength $children]
	}
	set cur $parent
	if {[string equal $cur $w]} {
	    return $cur
	}
	# If we are just at the original parent of $w, skip it as a
	# potential focus accepter.  Extra safety in this is to see if
	# that parent is also a proc (not a C command), which is what
	# BWidgets makes for any megawidget.  Could possibly also check
	# for '[info commands ::${origParent}:cmd] != ""'.  [Bug 765667]
	if {[string equal $cur $origParent]
	    && [info procs ::$origParent] != ""} {
	    continue
	}
	if {[focusOK $cur]} {
	    return $cur
	}
    }
}


# ----------------------------------------------------------------------------
#  Command Widget::focusOK
#  Same as tk_focusOK, but handles -editable option and whole tags list.
# ----------------------------------------------------------------------------
:public method focusOK { w } {
    set code [catch {$w cget -takefocus} value]
    if { $code == 1 } {
        return 0
    }
    if {($code == 0) && ($value != "")} {
	if {$value == 0} {
	    return 0
	} elseif {$value == 1} {
	    return [winfo viewable $w]
	} else {
	    set value [uplevel \#0 [list $value $w]]
            if {$value != ""} {
		return $value
	    }
        }
    }
    if {![winfo viewable $w]} {
	return 0
    }
    set code [catch {$w cget -state} value]
    if {($code == 0) && ($value == "disabled")} {
	return 0
    }
    set code [catch {$w cget -editable} value]
    if {($code == 0) && ($value == 0)} {
        return 0
    }

    set top [winfo toplevel $w]
    foreach tags [bindtags $w] {
        if { ![string equal $tags $top]  &&
             ![string equal $tags "all"] &&
             [regexp Key [bind $tags]] } {
            return 1
        }
    }
    return 0
}


:public method traverseTo { w } {
    set focus [focus]
    if {![string equal $focus ""]} {
	event generate $focus <<TraverseOut>>
    }
    focus $w

    event generate $w <<TraverseIn>>
}


# Widget::varForOption --
#
#	Retrieve a fully qualified variable name for the option specified.
#	If the option is not one for which a variable exists, throw an error 
#	(ie, those options that map directly to widget options).
#
# Arguments:
#	path	megawidget to get an option var for.
#	option	option to get a var for.
#
# Results:
#	varname	name of the variable, fully qualified, suitable for tracing.

:public method varForOption {path option} {
    #+++
    #variable _class
    #variable _optiontype
    
    upvar 0 NXBwidget::_class _class
    upvar 0 :_optiontype _optiontype
    #+++

    set class $_class($path)
    #+++ upvar 0 ${class}::$path:opt pathopt
    upvar 0 ::NXBwidget::${class}_$path:opt pathopt; #+++

    if { ![info exists pathopt($option)] } {
	error "unable to find variable for option \"$option\""
    }
    #+++ set varname "::Widget::${class}::$path:opt($option)"
    set varname ":${class}_$path:opt($option)"; #+++
    return $varname
}

# Widget::getVariable --
#
#       Get a variable from within the namespace of the widget.
#
# Arguments:
#	path		Megawidget to get the variable for.
#	varName		The variable name to retrieve.
#       newVarName	The variable name to refer to in the calling proc.
#
# Results:
#	Creates a reference to newVarName in the calling proc.
:public method getVariable { path varName {newVarName ""} } {
    #+++ variable _class
    upvar 0 NXBwidget::_class _class; #+++
    
    set class $_class($path)
    if {![string length $newVarName]} { set newVarName $varName }
    #+++ uplevel 1 [list upvar \#0 ${class}::$path:$varName $newVarName]
    uplevel 1 [list upvar \#0 :${class}_$path:$varName $newVarName]; #+++???
}

# Widget::options --
#
#       Return a key-value list of options for a widget.  This can
#       be used to serialize the options of a widget and pass them
#       on to a new widget with the same options.
#
# Arguments:
#	path		Widget to get the options for.
#	args		A list of options.  If empty, all options are returned.
#
# Results:
#	Returns list of options as: -option value -option value ...
:public method options { path args } {
    if {[llength $args]} {
        foreach option $args {
            lappend options [:_get_configure $path $option]; #+++
        }
    } else {
        set options [:_get_configure $path {}]; #+++
    }

    set result [list]
    foreach list $options {
        if {[llength $list] < 5} { continue }
        lappend result [lindex $list 0] [lindex $list end]
    }
    return $result
}


# Widget::getOption --
#
#	Given a list of widgets, determine which option value to use.
#	The widgets are given to the command in order of highest to
#	lowest.  Starting with the lowest widget, whichever one does
#	not match the default option value is returned as the value.
#	If all the widgets are default, we return the highest widget's
#	value.
#
# Arguments:
#	option		The option to check.
#	default		The default value.  If any widget in the list
#			does not match this default, its value is used.
#	args		A list of widgets.
#
# Results:
#	Returns the value of the given option to use.
#
:public method getOption { option default args } {
    for {set i [expr [llength $args] -1]} {$i >= 0} {incr i -1} {
	set widget [lindex $args $i]
	#+++ set value  [Widget::cget $widget $option]
	set value  [:cget $widget $option]; #+++
	if {[string equal $value $default]} { continue }
	return $value
    }
    return $value
}


:public method nextIndex { path node } {
    :getVariable $path autoIndex; #+++
    if {![info exists autoIndex]} { set autoIndex -1 }
    return [string map [list #auto [incr autoIndex]] $node]
}


:public method exists { path } {
    #+++ variable _class
    upvar 0 NXBwidget::_class _class; #+++
    return [info exists _class($path)]
}

:public method theme {{bool {}}} {
    # Private, *experimental* API that may change at any time - JH
    #+++ variable _theme
    upvar 0 :_theme _theme; #+++
    if {[llength [info level 0]] == 2} {
	# set theme-ability
	if {[catch {package require Tk 8.5a6}]
	    && [catch {package require tile 0.6}]
	    && [catch {package require tile 1}]} {
	    return -code error "BWidget's theming requires tile 0.6+"
	} else {
	    catch {style default BWSlim.Toolbutton -padding 0}
	}
	set _theme [string is true -strict $bool]
    }
    return $_theme
}

}; #END - class

}; #END - namespace
