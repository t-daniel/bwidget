#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}



namespace eval Demo {
    variable status
    set prgindic 1
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


proc Demo::createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


proc Demo::create { } {
    Demo::createMainwindow
    set frame   [${Demo::mainwin} getframe]
    
    
    #create the ProgressBar
    set prg   [ProgressBar $frame.prg -width 100 -height 10 -background white \
	    -variable Demo::prgindic -maximum 10]
    pack $prg -side top -pady 20m
    
    
    #pack the mainframe
    pack ${Demo::mainwin} -fill both -expand yes
    update idletasks
    
    
    #update the PorgressBar
    Demo::_update_progbar
}


proc Demo::_update_progbar { } {
    if {${Demo::prgindic} < 11} {
        incr Demo::prgindic
        puts "a: ${Demo::prgindic}"
        update
        after 1000 Demo::_update_progbar
    }
}


proc Demo::BAD_createProgressbar {path args} {
    array set maps [list ProgressDlg {} :cmd {} .frame.msg {} .frame.pb {}]
    array set maps [Widget::parseArgs ProgressDlg $args]
    
    set frame   [${Demo::mainwin} getframe]
    $frame configure -cursor watch
    
    eval [list label $frame.msg] $maps(.frame.msg) \
	-relief flat -borderwidth 0 \
	    -highlightthickness 0 -anchor w -justify left
    pack $frame.msg -side top -pady 3m -anchor nw -fill x -expand yes
    
    eval [list ProgressBar::create] $frame.pb $maps(.frame.pb) -width 100
    pack $frame.pb -side bottom -anchor w -fill x -expand yes
}



proc Demo::main {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "ProgressBar demo"

    Demo::create
    BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    wm deiconify .
    raise .
    focus -force .
}

Demo::main
wm geom . [wm geom .]
