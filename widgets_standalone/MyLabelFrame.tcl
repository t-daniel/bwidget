#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}



namespace eval Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


proc Demo::createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


proc Demo::create { } {
    Demo::createMainwindow
    
    
    set frame    [${Demo::mainwin} getframe]
    
    
    #create two LabelFrames
    set labf1 [LabelFrame $frame.labf1 -text "First frame" -side top \
                   -anchor w -relief sunken -borderwidth 1]
                   
    set labf2 [LabelFrame $frame.labf2 -text "Second frame" -side top \
                   -anchor w -relief sunken -borderwidth 1]
                   
    pack $labf1 $labf2 -side left -padx 4 -anchor n
    
    
    #fill the two LabelFrames
    set subf  [$labf1 getframe]
    set but1   [Button $subf.but1 -text "Button_1" \
                   -repeatdelay 300 \
                   -command  {puts "Button_1 pressed."} \
                   -helptext "This is a Button widget"]
    pack $but1  -side left -padx 4 -pady 4
                   
    set subf  [$labf2 getframe]
    set but2   [Button $subf.but2 -text "Button_2" \
                   -repeatdelay 300 \
                   -command  {puts "Button_2 pressed."} \
                   -helptext "This is a Button widget"]
    pack $but2  -side left -padx 4 -pady 4
    
    
    #pack the mainframe
    pack ${Demo::mainwin} -fill both -expand yes
    update idletasks
}



proc Demo::main {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "LabelFrame demo"

    Demo::create
    BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    wm deiconify .
    raise .
    focus -force .
}

Demo::main
wm geom . [wm geom .]
