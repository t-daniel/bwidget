#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide myseparator 1.0

package require nx
package require bwidget
package require separator



nx::Class create Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


Demo public method create { } {
    #Demo::createMainwindow
    
    #set frame    [${Demo::mainwin} getframe]
    
    
    #set sf [ScrollableFrame $frame.sf -width 60 -height 20]
    #pack $sf
    
    #create a Label and right from it a separator
    set topf  [frame .topf]
    pack $topf -pady 2 -fill x
    
    set lab [label $topf.lab1 -text "The vertical line is the Separator"]
    pack $lab -side left -padx 4
    
    set sep1  [NXBwidget::Separator new]
    $sep1 create $topf.sep1 -orient vertical
    pack [$sep1 retrievePath] -side left -padx 4 -fill y
    
    #set lab [label $sf.lab2 -text "Label_2"]
    #pack $lab -side left -padx 4
    
    
    #pack the mainframe
    #pack ${Demo::mainwin} -fill both -expand yes
    
    update idletasks
}



Demo public method init args {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm geometry . 300x200
    wm title . "Separator demo"

    :create

    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}


set dm [Demo new]
wm geom . [wm geom .]

