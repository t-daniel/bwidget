#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide myscrollview 1.0

package require nx
package require bwidget
package require scrollview
package require scrolledwindow
package require scrollframe
package require panedwindow
package require button
package require mainframe



nx::Class create Demo {
    #variable status
    #variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set :mainwin [NXBwidget::MainFrame new]
    ${:mainwin} create .mainframe \
                       -menu         $descmenu \
                       -textvariable DemoGlob::status \
                       -progressvar  DemoGlob::prgindic
}


Demo public method create { } {
    :createMainwindow
    set frame    [${:mainwin} getframe .mainframe]
    
    
    #set frame [frame .topf]
    #pack $frame -pady 2 -fill x
    
    
    
    #create a Label
    set lab [label $frame.lab1 -text "Drag the white rectangle \n to scroll the list on the right side"]
    pack $lab -pady 2 -fill x
    
    
    
    #create the PanedWindow
    set pw    [NXBwidget::PanedWindow  new]
    $pw create $frame.panedwin -side top
    
    
    #create the left side
    set pleft  [$pw add $frame.panedwin -weight 1]
    
    
    #create the right side
    set pright [$pw add $frame.panedwin -weight 2]
    #Create the ScrolledWindow. ScrollableFrame should be used, to make e.g. a scrollable list.
    #ScrollView will interact with ScrollableFrame.
    
    set sw [NXBwidget::ScrolledWindow new]
    $sw create $pright.scrwin -relief sunken -borderwidth 2
    
    set sf [NXBwidget::ScrollableFrame new]
    $sf create [$sw retrievePath].scrframe
    
    $sw setwidget [$sw retrievePath] $sf
    
    set subf [$sf getframe [$sf retrievePath]]
    
    for {set i 0} {$i <= 15} {incr i} {
      #creates Buttons as list items
      set but   [NXBwidget::Button new]
      $but create $subf.but_${i} -text "Button ${i}" \
                   -repeatdelay 300 \
                   -command  "DemoGlob::buttoncmd $i" \
                   -helptext "This is a Button widget"
      pack [$but retrievePath]  -side top -padx 4 -pady 2
    }
    pack [$sw retrievePath]  -side top  -expand yes -fill both
    
    pack [$pw retrievePath]  -fill both  -expand yes
    
    
    
    #create the ScrollView
    :createScrollView $pleft $sf
    
    
    puts "\n-->MyScrollView:\n  NXBwidget::_class(): [namespace eval NXBwidget {array get _class}]\n"
    
    
    
    #pack the mainframe
    pack [${:mainwin} retrievePath] -fill both -expand yes
    
    update idletasks
}


Demo public method createScrollView {container  wnd} {
    set scv [NXBwidget::ScrollView new]
    $scv create $container.scrview -window $wnd -fill white -relief sunken -bd 1 \
	    -width 300 -height 300
    pack [$scv retrievePath] -fill both -expand yes
}



Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "ScrollView demo"

    :create
    
    #BWidget::place . 300 300 center;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    wm geometry . 300x200
    raise .
    focus -force .
}

namespace eval DemoGlob {
    variable status
    variable prgindic
    
    proc buttoncmd {args} {
	puts "Button ${args} pressed!"
    }
}

set dm [Demo new]
wm geom . [wm geom .]
