### What is this repository for? ###

This is a ported version of BWidget (http://sourceforge.net/projects/tcllib/files/BWidget/1.9.7/) and (https://core.tcl.tk/bwidget/home).
It uses the language NX/Tcl (https://next-scripting.org/xowiki/Next_Scripting_Framework). BWidget now supports object-oriented programming.

### Contribution guidelines ###

Feel free to download or contribute.
