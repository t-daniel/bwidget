#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}

package provide myselectcolor 1.0

package require nx
package require widget
package require mainframe
package require selectcolor



nx::Class create Demo {
    #variable status
    #variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


Demo public method createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set :mainwin [NXBwidget::MainFrame new]
    ${:mainwin} create  .mainframe \
                       -menu         $descmenu \
                       -textvariable ::status \
                       -progressvar  ::prgindic
}


Demo public method create { } {
    :createMainwindow
    
    set frame    [${:mainwin} getframe [${:mainwin} retrievePath]]
    
    
    #create the SelectColor Widget
    set but0  [button $frame.but0 \
                   -text "SelectColor Widget" \
                   -command "::_show_color $frame.but0"]
    pack $but0 -side top -padx 5 -anchor w
    
    
    #pack the mainframe
    pack [${:mainwin} retrievePath] -fill both -expand yes
    update idletasks
}



Demo public method init {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "SelectColor demo"

    :create
    
    #BWidget::place . 300 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    
    wm deiconify .
    raise .
    focus -force .
}


proc _show_color {w} {
    #set color [SelectColor::menu $w.color [list below $w] \
    #                   -color [$w cget -background]]
    
    set __color [NXBwidget::SelectColor new]
    set color [$__color menu $w.color [list below $w] \
                       -color [$w cget -background]]
    
    if {[string length $color]} {
        #$w configure -background $color
        puts "The selected color: $color"
    }
}


set dm [Demo new]
wm geom . [wm geom .]
