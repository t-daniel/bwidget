#!/bin/sh
# The next line is executed by /bin/sh, but not tcl \
exec wish "$0" ${1+"$@"}



namespace eval Demo {
    variable status
    variable prgindic
    variable mainwin
    
    set pwd [pwd]
    cd [file dirname [info script]]
    variable DEMODIR [pwd]
}


proc Demo::createMainwindow {} {
    # Menu description (required by mainframe); currently File
    set descmenu {
        "&File" all file 0 {
            {command "E&xit" {} "Exit Programm" {} -command exit}
        }
    }
    
    #create the main frame (the main window)
    set Demo::mainwin [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable Demo::status \
                       -progressvar  Demo::prgindic]
}


proc Demo::create { } {
    Demo::createMainwindow
    
    
    set frame    [${Demo::mainwin} getframe]
    
    
    #create the Drag-source & Drop-target (using options: -dragenabled,  -dropenabled)
    set titf1 [TitleFrame $frame.titf1 -text "Drag sources"]
    set subf  [$titf1 getframe]

    set ent1  [LabelEntry $subf.e1 -label "DragEntry" -labelwidth 14 -dragenabled 1 -dragevent 3]
    set labf1 [LabelFrame $subf.f1 -text "Label (text)" -width 14]
    set f     [$labf1 getframe]
    set lab   [Label $f.l -text "Drag this text or the text in the inputfiled DragEntry \n (with the 3rd mouse button and \n move over the inputfield DropEntry)" -dragenabled 1 -dragevent 3]
    pack $lab

    set labf2 [LabelFrame $subf.f2 -text "Label (bitmap)" -width 14]
    set f     [$labf2 getframe]
    set lab   [Label $f.l -bitmap info -dragenabled 1 -dragevent 3]
    pack $lab
    pack $ent1 $labf1 $labf2 -side top -fill x -pady 4
    
    
    set titf2 [TitleFrame $frame.titf2 -text "Drop targets"]
    set subf  [$titf2 getframe]

    set ent1  [LabelEntry $subf.e1 -label "DropEntry" -labelwidth 14 -dropenabled 1]
    set labf1 [LabelFrame $subf.f1 -text "Label" -width 14]
    set f     [$labf1 getframe]
    set lab   [Label $f.l -dropenabled 1 -highlightthickness 1]
    pack $lab -fill x
    pack $ent1 $labf1 -side top -fill x -pady 4

    pack $titf1 $titf2 -pady 4
    
    
    #pack the mainframe
    pack ${Demo::mainwin} -fill both -expand yes
    update idletasks
}



proc Demo::main {} {
    variable DEMODIR

    lappend ::auto_path [file dirname $DEMODIR]; #DEMODIR is required for the line below !
    package require BWidget

    #option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . "Drag & Drop demo"

    Demo::create
    BWidget::place . 450 300 right;  #center  changed to  right (the position of the demo application); width=300  height=300
    wm deiconify .
    raise .
    focus -force .
}

Demo::main
wm geom . [wm geom .]
